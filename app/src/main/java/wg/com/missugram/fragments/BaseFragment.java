package wg.com.missugram.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import wg.com.missugram.activities.BaseActivity;
import wg.com.missugram.activities.LoginActivity;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.network.ApiClient;
import wg.com.missugram.network.ApiInterface;

public class BaseFragment extends Fragment {


    public final static int REMOVE_USER = 1;
    ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
    private AlertDialog.Builder builder;

    public <V> V findView(View view, int id) {
        return (V) view.findViewById(id);
    }

    public void showProgressBar() {
        Activity activity = getActivity();
        if (activity != null) {
            if (activity instanceof LoginActivity) {
                ((LoginActivity) getActivity()).showProgressBar();
            } else if (activity instanceof MainActivity) {
                ((MainActivity) getActivity()).showProgressBar();
            }
        }

    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
    }

    public void hideProgressBar() {
        Activity activity = getActivity();
        if (activity != null) {
            if (activity instanceof LoginActivity) {
                ((LoginActivity) getActivity()).hideProgressBar();
            } else if (activity instanceof MainActivity) {
                ((MainActivity) getActivity()).hideProgressBar();
            }
        }
    }

    public void showYesNoDialog1(final String message, final int dialogAction, final BaseActivity.onUserPick listener) {
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        if (listener != null)
                            listener.userPick(true);

                    }
                }, 5);
            }
        })
                .setNegativeButton("No", (dialogInterface, i) -> new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        dialogInterface.dismiss();
                    }
                }, 5)).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
    }
}
