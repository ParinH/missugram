package wg.com.missugram.fragments;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.adapter.FriendListAdapter;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.ContactResponseVo;
import wg.com.missugram.model.InviteRequestVo;
import wg.com.missugram.model.InviteUserVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;
import wg.com.missugram.utils.PrefUtils;
import wg.com.missugram.utils.VerticalDividerItemDecoration;


public class FriendsAndFamilyFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener {

    private static final String KEY_FROM_WHERE = "fromWhere";
    private static final String KEY_MEMORIAL_VO = "memorialVo";
    protected int pastVisibleItems, visibleItemCount, totalItemCountPrivate;
    private ArrayList<UserModel> commonVoArrayList;
    private RecyclerView mRvFriend;
    private LinearLayoutManager linearLayoutManager;
    private int fromWhere;
    private int currentPage = 1;
    private boolean isPullingMoreResult;
    private boolean isInviteClicked;
    private FriendListAdapter friendListAdapter;
    private MemorialVo mMemoriamVo;
    private EditText mEtSearch;
    private ImageView mIvClear;
    private TextView mTvNoRecords;
    UserModel user;
    int user_id;
    public static FriendsAndFamilyFragment newInstance(int fromWhere, MemorialVo memorialVo) {
        FriendsAndFamilyFragment friendsAndFamilyFragment = new FriendsAndFamilyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        bundle.putParcelable(KEY_MEMORIAL_VO, memorialVo);
        friendsAndFamilyFragment.setArguments(bundle);
        return friendsAndFamilyFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.firend_list_fragment_layout, container, false);
        user = PrefUtils.getUser(getContext(), "user");
        if (user != null) {
            user_id = user.getId();

        }
        initViews(root);
        addScrollListener();
        extractArguments();
        initUI();
        addListener();
        return root;
    }

    private void addListener()

    {
        mIvClear.setOnClickListener(this);
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = mEtSearch.getText().toString().toLowerCase(Locale.getDefault());

                if (friendListAdapter != null) {
                    friendListAdapter.filter(text);

                    if (friendListAdapter != null) {
                        if (friendListAdapter.getItemCount() == 0) {
                            mTvNoRecords.setVisibility(View.VISIBLE);
                        } else {
                            mTvNoRecords.setVisibility(View.GONE);
                        }
                    }
                }


            }
        });
    }

    private void callWs() {

        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        Call<ContactResponseVo> contactResponseVoCall = apiInterface.getMatchedContact(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Constants.PAGE_COUNT, currentPage, user_id);
        contactResponseVoCall.enqueue(new Callback<ContactResponseVo>() {
            @Override
            public void onResponse(Call<ContactResponseVo> call, Response<ContactResponseVo> response) {
                if (response != null && response.body() != null) {
                    CommonUtills.dismissProgressDialog();
                    ContactResponseVo contactResponseVo = response.body();
                    if (contactResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        if (contactResponseVo.getFriends_list() != null && contactResponseVo.getFriends_list().size() < Constants.PAGE_COUNT) {
                            isPullingMoreResult = true;
                        } else {
                            isPullingMoreResult = false;
                        }
                        setData(contactResponseVo.getFriends_list());
                    } else {
                        CommonUtills.showAlert(getActivity(), contactResponseVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactResponseVo> call, Throwable t) {

            }
        });
    }

    private void setData(ArrayList<UserModel> userModelArrayList) {
        commonVoArrayList = userModelArrayList;

        if (friendListAdapter == null) {
            friendListAdapter = new FriendListAdapter(getActivity(), commonVoArrayList, fromWhere);
            mRvFriend.setAdapter(friendListAdapter);
        } else {
            commonVoArrayList.addAll(userModelArrayList);
            friendListAdapter.notifyDataSetChanged();
            friendListAdapter.updateFriendsVo(userModelArrayList);
        }

    }

    private void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();
    }

    private void initViews(View root) {
        mRvFriend = (RecyclerView) root.findViewById(R.id.recycler_view_friend);
        mRvFriend.addItemDecoration(new VerticalDividerItemDecoration((int) getActivity().getResources().getDimension(R.dimen.divider_space), false));
        mEtSearch = (EditText) root.findViewById(R.id.mfl_etSearch);
        mIvClear = (ImageView) root.findViewById(R.id.mfl_ivClear);
        mTvNoRecords = (TextView) root.findViewById(R.id.mfl_tv_no_records);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRvFriend.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.mfl_ivClear:
                mEtSearch.setText("");
                try {
                    CommonUtility.hideKeyboard(getActivity(), mEtSearch);
                } catch (Throwable throwable) {
                    CustomLogHandler.printErrorlog(throwable);
                }
                break;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        CommonUtills.dismissProgressDialog();
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {

        }
        return false;
    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS == bundle.getInt(KEY_FROM_WHERE, Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS)) {
                fromWhere = bundle.getInt(KEY_FROM_WHERE, Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS);
                ((TestMainActivity) getActivity()).setIsFromGroupChat(Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS);
            } else {

                fromWhere = bundle.getInt(KEY_FROM_WHERE, Constants.FROM_MEMORIAL_LISTING);
                mMemoriamVo = bundle.getParcelable(KEY_MEMORIAL_VO);
            }
        }
        callWs();
    }


    private void addScrollListener() {

        mRvFriend.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCountPrivate = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isPullingMoreResult) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCountPrivate) {
                            isPullingMoreResult = true;
                            currentPage++;
                            callWs();
                        }
                    }
                }
            }
        });


    }

    public void collectAndOpenCreateGroup() {

        if (friendListAdapter==null)
            return;
        ArrayList<Integer> integers = friendListAdapter.getCheckedUserIDForChat();
        if (friendListAdapter != null && friendListAdapter.getCheckedUserIDForChat().size() > 0) {
            ((TestMainActivity) getActivity()).setIsFromGroupChat(-1);
            FragmentController.addCreateGroupFragment(getActivity(), integers);
        } else {
            CommonUtills.showAlert(getActivity(), getString(R.string.str_validation_groupcontact_Selection), Constants.NO_ACTION);
        }
    }

    public void senInvitation() {

        //if (!isInviteClicked) {

        //    isInviteClicked = true;
        InviteRequestVo inviteRequestVo = new InviteRequestVo();
        inviteRequestVo.setSender_user_id(MissUGram.getApp(getActivity()).getUserModel().getId());
        inviteRequestVo.setMemorial_id(mMemoriamVo == null ? 0 : mMemoriamVo.getId());
        ArrayList<InviteUserVo> arrayList = new ArrayList<>();
        for (UserModel userModel : friendListAdapter.getCheckedContacts()) {
            InviteUserVo inviteUserVo = new InviteUserVo();
            inviteUserVo.setId(userModel.getId());
            if (userModel.isChecked())
                arrayList.add(inviteUserVo);
        }
        inviteRequestVo.setReceiver_uses(arrayList);

        if (arrayList.size() <= 0) {
            CommonUtills.showAlert(getActivity(), getString(R.string.str_selectcontact), Constants.NO_ACTION);
        } else {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
            final Call<CommonVo> contactResponse = apiInterface.sendInvitation(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), inviteRequestVo);
            contactResponse.enqueue(new Callback<CommonVo>() {
                @Override
                public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                    CommonUtills.dismissProgressDialog();
                    //   isInviteClicked = false;

                    if (response != null && response.body() != null) {
                        if (response.body().getsStatus() == Constants.STATUS_SUCCESS) {
                            CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.ACTION_FRAGMENT_FINISH);
                        } else {
                            CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                        }
                    } else {
                        CommonUtills.showAlert(getActivity(), getString(R.string.str_something_went_wrong), Constants.NO_ACTION);
                    }

                }

                @Override
                public void onFailure(Call<CommonVo> call, Throwable t) {

                }
            });
        }

    }
}
