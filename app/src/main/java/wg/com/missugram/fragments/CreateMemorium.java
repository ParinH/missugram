package wg.com.missugram.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.ImageCropperActivity;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.MemorialEditResponseVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.PrefUtils;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static androidx.core.content.PermissionChecker.checkSelfPermission;


public class CreateMemorium extends BaseFragment implements View.OnClickListener, View.OnTouchListener {
    public static final String TIME_STAMP_FORMAT = "yyyyMMdd_HHmmss";
    public static final String IMAGE_DIRECTORY_NAME = "Image";
    private static final String KEY_FROM_WHERE = "fromDescription";
    private static final String KEY_MEMORIAL_VO_FOREDIT = "memorialVoForEdit";
    public static String MEDIA_DIRECTORY_PATH = null;
    final int DRAWABLE_TOP = 1;
    private final int MEDIA_TYPE_IMAGE = 1;
    String[] categoryItems = new String[]{"Select Option", "Public", "Private"};
    File photoFile = null;
    Uri photoURI = null;
    private EditText msCEtTitle;
    private Spinner msSpCmCategory;
    private EditText mCmEtCmDesc;
    private ImageView mCmIvCmClose;
    private ImageView mCmIvCmSelectedImage;
    private TextView mCmTvUpload;
    private int REQUEST_CAMERA = 1;
    private int REQUEST_CROP_CAMERA = 3;
    private int SELECT_FILE = 2;
    private int SELECT_CROP_FILE = 4;
    private File file;
    private MemorialVo mMemoriamVo;
    private int fromWhere;
    private StringBuffer m_buffer = new StringBuffer();
    private Uri mImageUri;
    int requestCode = 1;
    String imagePath = "";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;

    Context context;

    public static CreateMemorium newInstance(MemorialVo memorialVo, int fromWhere) {
        CreateMemorium createMemoriumFragment = new CreateMemorium();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        bundle.putParcelable(KEY_MEMORIAL_VO_FOREDIT, memorialVo);
        createMemoriumFragment.setArguments(bundle);
        return createMemoriumFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    private boolean isStoragePermissionGranted() {
        String TAG = "Storage Permission";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        String result = uri + "";
        // DocumentProvider
        if (isKitKat && (result.contains("media.documents"))) {
            String[] ary = result.split("/");
            int length = ary.length;
            String imgary = ary[length - 1];
            final String[] dat = imgary.split("%3A");
            final String docId = dat[1];
            final String type = dat[0];
            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(type)) {
            } else if ("audio".equals(type)) {
            }
            final String selection = "_id=?";
            final String[] selectionArgs = new String[]{
                    dat[1]
            };
            return getDataColumn(context, contentUri, selection, selectionArgs);
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.creatememoriam_layout_fragment, container, false);
        isStoragePermissionGranted();

        Log.e("user_id", " " + MissUGram.getApp(getActivity()).getUserModel().getId());

        Thread t = new Thread() {
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initViews(root);

                        initUI();
                        extractArguments();


                    }
                });
                // makeWsCall();

            }
        };
        t.start();


        return root;
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        Thread t = new Thread() {
//            public void run() {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        initUI();
//                        extractArguments();
//
//
//                    }
//                });
//                // makeWsCall();
//
//            }
//        };
//        t.start();
//    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromWhere = bundle.getInt(KEY_FROM_WHERE, 0);
            mMemoriamVo = bundle.getParcelable(KEY_MEMORIAL_VO_FOREDIT);

            if (fromWhere == Constants.FROM_MYMEMORIAL_EDIT) {


                ((TestMainActivity) getActivity()).setToolbarTitle(getString(R.string.str_editMemorium));
                fillMemoriumDataForEdit();
            }
            ((TestMainActivity) getActivity()).setCurrentMemoriamVal(fromWhere);

        }
    }

    private void fillMemoriumDataForEdit() {
        msCEtTitle.setText(mMemoriamVo.getTitle());
        mCmEtCmDesc.setText(mMemoriamVo.getDescription());
        ImageLoader.getInstance().displayImage(mMemoriamVo.getImage(), mCmIvCmSelectedImage);
        //  msSpCmCategory.setSelection(mMemoriamVo.getCategory_id().equalsIgnoreCase(Constants.MEMORIUM_TYPE_PUBLIC) ? 1 : 2);
        if (!TextUtils.isEmpty(mMemoriamVo.getCategory_id())) {
            msSpCmCategory.setSelection(mMemoriamVo.getCategory_id().equalsIgnoreCase(Constants.MEMORIUM_TYPE_PUBLIC) ? 1 : 2);
        } else {
            msSpCmCategory.setSelection(Constants.myCate_id);

        }
        mCmTvUpload.setVisibility(View.GONE);
        mCmIvCmSelectedImage.setVisibility(View.VISIBLE);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void initUI() {

        ((TestMainActivity) getActivity()).updateToolbar();

    }

    private void initViews(View root) {
        msCEtTitle = (EditText) root.findViewById(R.id.mcm_title);
        msSpCmCategory = (Spinner) root.findViewById(R.id.mcm_category);
        mCmEtCmDesc = (EditText) root.findViewById(R.id.mcm_desc);
        mCmIvCmClose = (ImageView) root.findViewById(R.id.mcm_close);
        mCmIvCmSelectedImage = (ImageView) root.findViewById(R.id.mcm_iv_selectedimage);
        mCmTvUpload = (TextView) root.findViewById(R.id.mcm_tv_upload);

        mCmIvCmClose.setOnClickListener(this);
        mCmIvCmSelectedImage.setOnClickListener(this);
        mCmTvUpload.setOnTouchListener(this);
        addSpinnerData();
    }

    private void addSpinnerData() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.view_spinner_item,
                categoryItems
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        msSpCmCategory.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.mcm_close:
                //mCmTvUpload.setVisibility(View.VISIBLE);
                mCmIvCmSelectedImage.setVisibility(View.GONE);
                mCmTvUpload.setVisibility(View.VISIBLE);
                mCmIvCmClose.setVisibility(View.GONE);
                photoFile = null;
                break;
            case R.id.mcm_iv_selectedimage:

                if (checkAndRequestPermissions(getActivity())) {
                    chooseImage(context);
                }
               /* CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getContext(), this);*/
                break;

        }
    }


    private String getPath1(Uri uri) {
        String result;
        Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


     /*   if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                mCmIvCmSelectedImage.setImageURI(resultUri);
                imagePath = getPath1(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }*/


        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK && data != null) {
                    Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                    mCmIvCmSelectedImage.setImageBitmap(selectedImage);

                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    mCmIvCmSelectedImage.setImageBitmap(photo);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getActivity(), photo);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));

                    imagePath = getRealPathFromURI(tempUri);
                }
                break;
            case 1:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    if (selectedImage != null) {
                        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);
                            mCmIvCmSelectedImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                            cursor.close();
                            imagePath = picturePath;
                        }
                    }
                }
                break;
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void closeKeyboard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), mCmEtCmDesc);
            CommonUtility.hideKeyboard(getActivity(), msCEtTitle);

        } catch (Throwable throwable) {

        }
    }

    private void validateUserData() {


        /*if (mCmIvCmSelectedImage.getVisibility() == View.GONE) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_image), Constants.NO_ACTION);
        }*/
        if (CommonUtills.checkLength(msCEtTitle.getText().toString().trim()) <= 0 || msCEtTitle.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_title), Constants.NO_ACTION);
        } else if (msSpCmCategory.getSelectedItemPosition() == 0) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_category), Constants.NO_ACTION);
        } else if (CommonUtills.checkLength(mCmEtCmDesc.getText().toString().trim()) <= 0 || mCmEtCmDesc.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_desc), Constants.NO_ACTION);
        } else if (mCmIvCmSelectedImage.getDrawable() == null) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_image), Constants.NO_ACTION);
        }
//        else if (mCmIvCmSelectedImage.getDrawable()==null){
//            CommonUtills.showAlert(getActivity(), getString(R.string.cm_empty_image), Constants.NO_ACTION);
//        }

//
        else {
            if (CommonUtills.isNetworkAvailable(getActivity())) {

                if (fromWhere == Constants.FROM_MYMEMORIAL_EDIT) {
                    callEditMemoriumWS();
                } else {
                    callCreateMemoriumWS();
                }
            } else {
                CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
            }
        }
    }

    public void doProcessCreateMemorium() {
        closeKeyboard();
        validateUserData();
    }

    private void callCreateMemoriumWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        MemorialVo memorialVo = new MemorialVo();
        memorialVo.setTitle(msCEtTitle.getText().toString().trim());
        memorialVo.setCategory_id(msSpCmCategory.getSelectedItem().toString());
        memorialVo.setDescription(mCmEtCmDesc.getText().toString().trim());
        memorialVo.setUser_id(MissUGram.getApp(getActivity()).getUserModel().getId());
        Gson gson = new Gson();
        String convertedJson = gson.toJson(memorialVo);
        RequestBody name = RequestBody.create(MediaType.parse("application/json"), convertedJson);

        if (fromWhere == Constants.FROM_MYMEMORIAL_EDIT && file == null) {

        } else {
            //with image selection in both create and edit memorium.
        }

        MultipartBody.Part imageMB = null;

        if (!TextUtils.isEmpty(imagePath)) {
            RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(imagePath));
            imageMB = MultipartBody.Part.createFormData("image", new File(imagePath).getName(), imageBody);
        }


        // final Call<CommonVo> memoriumResponse = apiInterface.createMemorium(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), body, name);
        final Call<CommonVo> memoriumResponse = apiInterface.createMemorium(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), imageMB, name);

        memoriumResponse.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                CommonUtills.dismissProgressDialog();

                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        if (getActivity() != null && !getActivity().isFinishing()) {

                            CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.ACTION_FRAGMENT_FINISH);

                            clearAllText();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });
    }

    private void callEditMemoriumWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        final MemorialVo memorialVo = new MemorialVo();
        memorialVo.setTitle(msCEtTitle.getText().toString().trim());
        memorialVo.setCategory_id(msSpCmCategory.getSelectedItem().toString());
        memorialVo.setDescription(mCmEtCmDesc.getText().toString().trim());
        memorialVo.setId(mMemoriamVo.getId());
        memorialVo.setUser_id(MissUGram.getApp(getActivity()).getUserModel().getId());

        Gson gson = new Gson();
        String convertedJson = gson.toJson(memorialVo);
        RequestBody name = RequestBody.create(MediaType.parse("application/json"), convertedJson);

       /* MultipartBody.Part body = null;

        if (file != null) {
            RequestBody imageBody = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), imageBody);
        }
*/

        MultipartBody.Part imageMB = null;

        if (!TextUtils.isEmpty(imagePath)) {
            RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(imagePath));
            imageMB = MultipartBody.Part.createFormData("image", new File(imagePath).getName(), imageBody);
        }

        //body will go null if no image ios provide.
        final Call<MemorialEditResponseVo> memorialEditResponseVoCall = apiInterface.updateMemorium(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), imageMB, name);

        memorialEditResponseVoCall.enqueue(new Callback<MemorialEditResponseVo>() {
            @Override
            public void onResponse(Call<MemorialEditResponseVo> call, Response<MemorialEditResponseVo> response) {
                CommonUtills.dismissProgressDialog();

                if (response != null && response.body() != null) {
                    MemorialEditResponseVo memorialResponseVo = response.body();
                    if (memorialResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        if (getActivity() != null && !getActivity().isFinishing()) {

                            CommonUtills.showAlert(getActivity(), memorialResponseVo.getsMessage(), Constants.ACTION_FRAGMENT_DESCRIPTION, memorialResponseVo.getMemorialinfo());

                            clearAllText();
                        }
                    } else {
                        CommonUtills.showAlert(getActivity(), memorialResponseVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<MemorialEditResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });
    }


    private void clearAllText() {
        mCmEtCmDesc.setText("");
        msCEtTitle.setText("");
        msSpCmCategory.setSelection(0);
        //mCmTvUpload.setVisibility(View.VISIBLE);
        //mCmIvCmSelectedImage.setVisibility(View.GONE);

    }

    public MemorialVo getmMemoriamVo() {
        return mMemoriamVo;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.mcm_tv_upload:
                if (mCmTvUpload.getVisibility() == View.VISIBLE) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        // Getting  if drawable top is clicked. If clicked we will open the dialog for image selection.
                        if (event.getRawX() >= mCmTvUpload.getTop() - mCmTvUpload.getCompoundDrawables()[DRAWABLE_TOP].getBounds().width()) {
                            if (checkAndRequestPermissions(getActivity())) {
                                chooseImage(context);
                            }
                            /*CropImage.activity()
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(getContext(), this);*/
                        }
                    }
                }
                break;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(),
                            "FlagUp Requires Access to Camara.", Toast.LENGTH_SHORT)
                            .show();
                } else if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(),
                            "FlagUp Requires Access to Your Storage.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    chooseImage(context);
                }
                break;
        }
    }

    public static boolean checkAndRequestPermissions(final Activity context) {
        int WExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (WExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void chooseImage(Context context) {
        final CharSequence[] optionsMenu = {"Take Photo", "Choose from Gallery", "Exit"}; // create a menuOption Array
        // create a dialog for showing the optionsMenu
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // set the items in builder
        builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (optionsMenu[i].equals("Take Photo")) {
                    // Open the camera and get the photo
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else if (optionsMenu[i].equals("Choose from Gallery")) {
                    // choose from  external storage
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);
                } else if (optionsMenu[i].equals("Exit")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

}
