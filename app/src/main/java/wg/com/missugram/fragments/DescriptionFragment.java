package wg.com.missugram.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wg.framework.log.CustomLogHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import wg.com.missugram.BuildConfig;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FileUtil;
import wg.com.missugram.utils.FragmentController;
import wg.com.missugram.utils.ScreenshotUtil;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;
import static androidx.core.content.ContextCompat.checkSelfPermission;


public class DescriptionFragment extends BaseFragment implements View.OnClickListener {


    private static final String KEY_FROM_WHERE = "fromWhere";
    private static final String KEY_MEMORIAL_VO = "memorialVo";
    private int fromWhere;

    private static final int REQUEST_CODE = 1;

    private MemorialVo mMemoriamVo;
    private ImageView mDflIvImage;
    private TextView mDflTvTitle;
    private View mDflView;
    private TextView mDflTvDescription;
    private LinearLayout mDflLlComments;
    private LinearLayout mDflLlShare;
    private LinearLayout mDflLlInvite;
    private LinearLayout layoutShare;
    private Bitmap bitmap;
    private File imagePath;
    View mRootView;

    Context context;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    public static DescriptionFragment newInstance(MemorialVo memorialVo, int fromWhere) {
        DescriptionFragment descriptionFragment = new DescriptionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        bundle.putParcelable(KEY_MEMORIAL_VO, memorialVo);
        descriptionFragment.setArguments(bundle);
        return descriptionFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.description_fragment_layout, container, false);
        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);

        initViews(root);

        extractArguments();
        initUI();
        setData();
        setClickListener();


        return root;
    }

    public void setData() {
        if (mMemoriamVo != null) {
            mDflTvTitle.setText(mMemoriamVo.getTitle());
            mDflTvDescription.setText(mMemoriamVo.getDescription());

           // Constants.myCate_id = Integer.parseInt(!mMemoriamVo.get.equalsIgnoreCase(Constants.MEMORIUM_TYPE_INVITE) ? memoriamType : "");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ImageLoader.getInstance().displayImage(mMemoriamVo.getImage(), mDflIvImage, CommonUtills.getRoundImageOptionsForMemorial());

                }
            }, 500);
        }
    }

    private void setClickListener() {
        mDflLlComments.setOnClickListener(this);
        mDflLlInvite.setOnClickListener(this);
        mDflLlShare.setOnClickListener(this);
    }

    public void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();
        ((TestMainActivity) getActivity()).setToolbarTitle(mMemoriamVo != null ? mMemoriamVo.getTitle() : "");
        mDflLlInvite.setVisibility(fromWhere == Constants.FROM_MY_MEMORIAL ? View.VISIBLE : View.GONE);
    }

    private void initViews(View root) {
        mDflIvImage = (ImageView) root.findViewById(R.id.dfl_iv_image);
        mDflTvTitle = (TextView) root.findViewById(R.id.dfl_tv_title);
        mDflView = root.findViewById(R.id.dfl_view);
        mDflTvDescription = (TextView) root.findViewById(R.id.dfl_tv_description);
        mDflLlComments = (LinearLayout) root.findViewById(R.id.dfl_ll_comments);
        mDflLlShare = (LinearLayout) root.findViewById(R.id.dfl_ll_share);
        mDflLlInvite = (LinearLayout) root.findViewById(R.id.dfl_ll_invite);
        layoutShare = (LinearLayout) root.findViewById(R.id.layoutShare);
        mRootView = root.findViewById(R.id.content_main).getRootView();
    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromWhere = bundle.getInt(KEY_FROM_WHERE, Constants.FROM_MEMORIAL_LISTING);
            mMemoriamVo = bundle.getParcelable(KEY_MEMORIAL_VO);
            ((TestMainActivity) getActivity()).setCurrentMemoriamVal(fromWhere);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dfl_ll_comments:
                FragmentController.addCommentsListingFragment(getActivity(), mMemoriamVo.getId(), fromWhere);
                break;
            case R.id.dfl_ll_share:
                Bitmap mBitmap = takeScreenshot();
                saveBitmap(mBitmap);
                shareIt();
                break;
            case R.id.dfl_ll_invite:
                ((TestMainActivity) getActivity()).setCurrentFriendListVal(Constants.FRIEND_LIST_FROM_FROM_INVITE);
                inviteUser();
                break;
        }
    }

    private void inviteUser() {
        FragmentController.addFriendListFragment(getActivity(), Constants.FRIEND_LIST_FROM_FROM_INVITE, mMemoriamVo);
    }

    public void openCreateMemoriumForEdit() {

        FragmentController.addCreateMemoriumFragment(getActivity(), mMemoriamVo, Constants.FROM_MYMEMORIAL_EDIT);
    }

    public void setmMemoriamVo(MemorialVo mMemoriamVo) {
        this.mMemoriamVo = mMemoriamVo;
        ((TestMainActivity) getActivity()).updateToolbar();
        ((TestMainActivity) getActivity()).setToolbarTitle(mMemoriamVo != null ? mMemoriamVo.getTitle() : "");
    }

    public MemorialVo getmMemoriamVo() {
        return mMemoriamVo;
    }

    /*
     * Developer:parin
     * Date:10/12
     * */
    private void shareIt() {
        // Uri uri = Uri.fromFile(imagePath);
       /* Uri uri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider",imagePath);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);

        startActivity(Intent.createChooser(sharingIntent, "Share via"));

*/

        bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(mRootView); // Take ScreenshotUtil for any view

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestPermissionAndSave();
            }
        }, 200);
    }

    private void requestPermissionAndSave() {

        Dexter.withContext(getContext())
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        if (bitmap != null) {
                            String path = Environment.getExternalStorageDirectory().toString() + "/test.png";
                            FileUtil.getInstance().storeBitmap(bitmap, path);
                            Toast.makeText(context, "Success" + " " + path, Toast.LENGTH_LONG).show();
                            shareImage(bitmap);

                        } else {
                            Toast.makeText(context, "Fail", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        if (permissionDeniedResponse.isPermanentlyDenied()) {
                            Toast.makeText(context, "Require Storage Permission", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), this.getClass().getSimpleName());
                            intent.setData(uri);
                            startActivityForResult(intent, 1);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();


    }

    private void shareImage(Bitmap inImage) {

        String tittle = mDflTvTitle.getText().toString();
        String desciption = mDflTvDescription.getText().toString();

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        // sharingIntent.setType("*/*");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
        /* sharingIntent.putExtra(Intent.EXTRA_TEXT, tittle + "\n " + desciption);*/
        sharingIntent.putExtra(Intent.EXTRA_STREAM, getImageUri(inImage));
        startActivity(Intent.createChooser(sharingIntent, "Share Image Using"));
    }


    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);

        if (path != null) {
            return Uri.parse(path);
        } else {
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    //======================
    public Bitmap takeScreenshot() {
        View rootView = mRootView.getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(Environment.getExternalStorageDirectory() + File.separator + "screenshot.png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            CustomLogHandler.printDebuglog(DescriptionFragment.class.getSimpleName(), e.getMessage());
        } catch (IOException e) {
            CustomLogHandler.printDebuglog(DescriptionFragment.class.getSimpleName(), e.getMessage());
        }
    }


}