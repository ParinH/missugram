package wg.com.missugram.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.adapter.MemorialAdapter;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.MemorialResponseVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

public class TestMemorialFragment extends BaseFragment {
    Context context;
    Call<MemorialResponseVo> call;
    private String memorialUrl;
    protected boolean isPullingMoreResultsPrivate, isPullingMoreResultsPublic;
    private int currentPage = 1;
    private String memoriamType = Constants.MEMORIUM_TYPE_PUBLIC;//private
    int user_id;
    private MemorialAdapter memorialAdapterPrivate;
    private MemorialAdapter memorialAdapterPublic;
    private int fromWhere;


    @BindView(R.id.mfl_tv_public)
    TextView mfl_tv_public;
    @BindView(R.id.pb)
    ProgressBar pb;
    @BindView(R.id.mfl_tv_invite)
    TextView mfl_tv_invite;
    @BindView(R.id.mfl_tv_create_memoriam)
    TextView mfl_tv_create_memoriam;
    @BindView(R.id.mfl_rl_bottom)
    RelativeLayout mfl_rl_bottom;
    @BindView(R.id.mfl_etSearch)
    EditText mfl_etSearch;
    @BindView(R.id.llSearch)
    LinearLayout llSearch;
    @BindView(R.id.mfl_ivClear)
    ImageView mfl_ivClear;
    @BindView(R.id.mfl_tv_no_records)
    TextView mfl_tv_no_records;
    @BindView(R.id.recycler_view_public)
    RecyclerView recycler_view_public;
    @BindView(R.id.recycler_view_private)
    RecyclerView recycler_view_private;
    private static final String KEY_FROM_WHERE = "fromWhere";

    MemorialAdapter memorialAdapter;

    public static TestMemorialFragment newInstance(int fromWhere) {
        TestMemorialFragment settingsFragment = new TestMemorialFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        settingsFragment.setArguments(bundle);
        return settingsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        super.onCreate(savedInstanceState);

        context = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_memorial, container, false);
        ButterKnife.bind(this, view);


        user_id = MissUGram.getApp(getActivity()).getUserModel().getId();


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recycler_view_public.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recycler_view_private.setLayoutManager(layoutManager1);

        recycler_view_private.setVisibility(View.GONE);
        recycler_view_public.setVisibility(View.VISIBLE);

        initUI();
        // getList();
        extractArguments();

        mfl_etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = mfl_etSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (memoriamType.equals(Constants.MEMORIUM_TYPE_PRIVATE) || memoriamType.equals(Constants.MEMORIUM_TYPE_INVITE)) {

                    if (memorialAdapter != null) {
                        memorialAdapter.filter(text);


                        if (memorialAdapter.getItemCount() == 0) {
                            mfl_tv_no_records.setVisibility(View.VISIBLE);
                        } else {
                            mfl_tv_no_records.setVisibility(View.GONE);
                        }
                        memorialAdapter.notifyDataSetChanged();
                    }


                } else {

                    if (memorialAdapter != null) {
                        memorialAdapter.filter(text);
                        if (memorialAdapter.getItemCount() == 0) {
                            mfl_tv_no_records.setVisibility(View.VISIBLE);
                        } else {
                            mfl_tv_no_records.setVisibility(View.GONE);
                        }
                        memorialAdapter.notifyDataSetChanged();
                    }

                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


            }
        });

        return view;
    }

    private void initUI() {
        ((TestMainActivity) getActivity()).updateToolbarr();
        memorialUrl = Constants.URL_MEMORIAM_PAGE;

    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromWhere = bundle.getInt(KEY_FROM_WHERE, Constants.FROM_MEMORIAL_LISTING);
            ((TestMainActivity) getActivity()).setCurrentFriendListVal(fromWhere);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        user_id = MissUGram.getApp(getActivity()).getUserModel().getId();
        getList();
    }


    private void getList() {

        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        // call.cancel();
        call = apiInterface.getMemorials(memorialUrl, Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Constants.PAGE_COUNT, currentPage, memoriamType, user_id);

        call.enqueue(new Callback<MemorialResponseVo>() {
            @Override
            public void onResponse(Call<MemorialResponseVo> call, Response<MemorialResponseVo> response) {
                if (response != null && response.body() != null) {
                    CommonUtills.dismissProgressDialog();


                    memorialAdapter = new MemorialAdapter(context, response.body().getMemorialinfo());
                    recycler_view_public.setAdapter(memorialAdapter);

                    MemorialResponseVo memorialResponseVo = response.body();
                    if (memorialResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {

                        if (memorialResponseVo.getMemorialinfo().size() == 0) {
                            mfl_tv_no_records.setVisibility(View.VISIBLE);
                            //  pb.setVisibility(View.GONE);

                        } else {
                            mfl_tv_no_records.setVisibility(View.GONE);
                            //  pb.setVisibility(View.GONE);
                        }
                        if (memorialResponseVo.getMemorialinfo().size() < Constants.PAGE_COUNT) {
                            isPullingMoreResultsPublic = true;
                            isPullingMoreResultsPrivate = true;
                        } else {
                            isPullingMoreResultsPublic = false;
                            isPullingMoreResultsPrivate = false;
                        }

                        memorialAdapter.setOnRecyclerItemListener(new MemorialAdapter.OnRecyclerItemClickListener() {
                            @Override
                            public void onViewItemClicked(MemorialVo memorialVo) {
                                //since from the web e are not getting the categoryId so adding statically based on the memorial requested provate or public. That will be set in edit memoriuam as a value.

                                memorialVo.setCategory_id(!memoriamType.equalsIgnoreCase(Constants.MEMORIUM_TYPE_INVITE) ? memoriamType : "");

                                FragmentController.addDescriptionFragment(getActivity(), memorialVo, fromWhere);
                            }
                        });

                        //setListData(memorialResponseVo.getMemorialinfo());

                    } else {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<MemorialResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();

            }
        });

    }

    @OnClick({R.id.mfl_tv_public, R.id.mfl_tv_invite, R.id.mfl_ivClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mfl_tv_public:
                mfl_etSearch.setText("");
                mfl_tv_public.setBackgroundResource(R.drawable.right_selected_view);
                mfl_tv_public.setTextColor(Color.WHITE);
                mfl_tv_invite.setTextColor(getResources().getColor(R.color.colorPrimary));
                mfl_tv_invite.setBackgroundResource(0);
                memoriamType = Constants.MEMORIUM_TYPE_PUBLIC;
                recycler_view_private.setVisibility(View.GONE);
                recycler_view_public.setVisibility(View.VISIBLE);
                getList();
                break;
            case R.id.mfl_tv_invite:
                mfl_etSearch.setText("");
                mfl_tv_invite.setBackgroundResource(R.drawable.right_selected_view);
                mfl_tv_invite.setTextColor(Color.WHITE);
                mfl_tv_public.setTextColor(getResources().getColor(R.color.colorPrimary));
                mfl_tv_public.setBackgroundResource(0);
                memoriamType = Constants.MEMORIUM_TYPE_INVITE;
                recycler_view_private.setVisibility(View.VISIBLE);
                recycler_view_public.setVisibility(View.GONE);
                getList();
                break;

            case R.id.mfl_ivClear:
                mfl_etSearch.setText("");
                llSearch.setVisibility(View.GONE);
                try {
                    CommonUtility.hideKeyboard(getActivity(), mfl_etSearch);
                } catch (Throwable throwable) {
                    CustomLogHandler.printErrorlog(throwable);
                }
                break;


        }
    }

//    @Override
//    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
//        inflater.inflate(R.menu.menu, menu);
//
//        super.onCreateOptionsMenu(menu, inflater);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
//
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            llSearch.setVisibility(View.VISIBLE);
//        }
//
//        return super.onOptionsItemSelected(item);
//
//    }


    public void showSearchBar() {
        llSearch.setVisibility(View.VISIBLE);

    }


}