package wg.com.missugram.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;


public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {


    private ImageView fpIvLogo;
    private EditText fpEtEmail;
    private Button fpBtDone;
    private Button fpBtCancel;

    @Override
    public void onClick(View v) {
        if (v == fpBtDone) {
            // Handle clicks for fpBtDone
            closeKeyBoard();
            validateUserData();
        } else if (v == fpBtCancel) {
            FragmentController.removeThisFragment(getActivity(), FragmentController.TAG_FORGOT_PASSWORD_FRAGMENT);
            FragmentController.addLoginFragment(getActivity());

        }
    }

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.forgot_password_fragment_layout, container, false);
        initViews(root);
        return root;

    }

    private void initViews(View root) {
        fpIvLogo = (ImageView) root.findViewById(R.id.fp_iv_logo);
        fpEtEmail = (EditText) root.findViewById(R.id.fp_et_email);
        fpBtDone = (Button) root.findViewById(R.id.fp_bt_done);
        fpBtCancel = (Button) root.findViewById(R.id.fp_bt_cancel);

        fpBtDone.setOnClickListener(this);
        fpBtCancel.setOnClickListener(this);

    }

    private void closeKeyBoard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), fpEtEmail);

        } catch (Throwable t) {
        }
    }

    private void validateUserData() {
        if (CommonUtills.checkLength(fpEtEmail.getText().toString().trim()) <= 0 || fpEtEmail.getText().toString().trim().isEmpty()) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_email), Constants.NO_ACTION);

        } else if (!CommonUtills.isValidEmail(fpEtEmail.getText().toString().trim())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_invalidemail), Constants.NO_ACTION);
        } else {
            closeKeyBoard();

            callRegisterUserWS();
        }
    }

    private void callRegisterUserWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        UserModel userRegisterRequest = new UserModel();
        userRegisterRequest.setEmail(fpEtEmail.getText().toString().trim());

        final Call<CommonVo> commonvo = apiInterface.forgotPassword(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), userRegisterRequest);
        commonvo.enqueue(new Callback<CommonVo>() {
                             @Override
                             public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                                 CommonUtills.dismissProgressDialog();
                                 if (response != null && response.body() != null) {
                                     CommonVo commonVo = response.body();
                                     if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                                         if (getActivity() != null && !getActivity().isFinishing()) {
                                             CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.ACTION_SIGN_IN);
                                         }
                                     } else {
                                         if (getActivity() != null && !getActivity().isFinishing()) {
                                             CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.NO_ACTION);
                                         }
                                     }
                                 }
                             }

                             @Override
                             public void onFailure(Call<CommonVo> call, Throwable t) {
                                 CommonUtills.dismissProgressDialog();
                                 CustomLogHandler.printErrorlog(t);
                             }
                         }
        );
    }
}
