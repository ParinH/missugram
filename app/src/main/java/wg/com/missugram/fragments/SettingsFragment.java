package wg.com.missugram.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wg.framework.log.CustomLogHandler;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.BaseActivity;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.NotificationRequestVo;
import wg.com.missugram.model.SlugVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;


public class SettingsFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener {
    private static final String SELECTED_TAB = "selectedTab";
    final int DRAWABLE_RIGHT = 2;
    private TextView mTvMyAccount;
    private TextView mTvNotifications;
    private TextView mTvAboutApp;
    private TextView mTvTermsUse;
    private TextView mTvChanePassword;
    private TextView mTvViewEditProfile;
    private TextView mTvDeleteAccount;

    public static SettingsFragment newInstance(String str) {
        SettingsFragment settingsFragment = new SettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SELECTED_TAB, str);
        settingsFragment.setArguments(bundle);
        return settingsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.settings_layout_fragment, container, false);
        initViews(root);
        initUI();
        setNotificationIcon();
        return root;
    }

    private void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();
    }

    private void setNotificationIcon() {
        Drawable[] drawables = mTvNotifications.getCompoundDrawables();
        Drawable drawableToSet = ContextCompat.getDrawable(getActivity(), CommonUtills.getPrefIsNotificationEnabled(getActivity()) ? R.drawable.enable : R.drawable.disable);
        mTvNotifications.setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawableToSet, drawables[3]);
    }

    private void initViews(View root) {
        mTvMyAccount = (TextView) root.findViewById(R.id.ms_et_myaccount);
        mTvNotifications = (TextView) root.findViewById(R.id.ms_et_notifications);
        mTvAboutApp = (TextView) root.findViewById(R.id.ms_et_aboutapplication);
        mTvTermsUse = (TextView) root.findViewById(R.id.ms_et_terms);

        mTvChanePassword = (TextView) root.findViewById(R.id.ms_tv_changepassword);
        mTvViewEditProfile = (TextView) root.findViewById(R.id.ms_tv_edit_viewprofile);
        mTvDeleteAccount = (TextView) root.findViewById(R.id.ms_tv_deleteaccount);

        mTvMyAccount.setOnTouchListener(this);
        mTvAboutApp.setOnClickListener(this);
        mTvTermsUse.setOnClickListener(this);
        mTvNotifications.setOnTouchListener(this);

        mTvChanePassword.setOnClickListener(this);
        mTvDeleteAccount.setOnClickListener(this);
        mTvViewEditProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ms_et_aboutapplication:

                if (CommonUtills.isNetworkAvailable(getActivity())) {
                    callCallTermsAndAboutApplication(Constants.SLUG_ABOUTUS);

                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
                }

                break;
            case R.id.ms_et_terms:

                if (CommonUtills.isNetworkAvailable(getActivity())) {
                    callCallTermsAndAboutApplication(Constants.SLUG_TERMSNCONDITIONS);

                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
                }


                break;
            case R.id.ms_tv_changepassword:
                FragmentController.addChangePasswordFragment(getActivity());
                break;
            case R.id.ms_tv_deleteaccount:
                ((TestMainActivity) getActivity()).showYesNoDialog1(getString(R.string.str_deleteaccount), BaseActivity.DELETE_DLG, new BaseActivity.onUserPick() {
                    @Override
                    public void userPick(Boolean yes) {
                        if (yes) {
                            if (CommonUtills.isNetworkAvailable(getActivity())) {
                                callDeleteUserProfile();

                            } else {
                                CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
                            }
                        }
                    }
                });

                break;
            case R.id.ms_tv_edit_viewprofile:
                FragmentController.addSignUpFragment(getActivity(), Constants.FROM_SETTINGS);
                break;

        }
    }



    private void callCallTermsAndAboutApplication(String p_SlugName) {

        if (p_SlugName != null) {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
            final Call<SlugVo> slugVoCall = apiInterface.callSlug(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), p_SlugName);
            slugVoCall.enqueue(new Callback<SlugVo>() {
                @Override
                public void onResponse(Call<SlugVo> call, Response<SlugVo> response) {
                    CommonUtills.dismissProgressDialog();

                    if (response != null && response.body() != null) {
                        SlugVo mSlugVo = response.body();

                        if (mSlugVo.getsStatus() == Constants.STATUS_SUCCESS) {
                            ((TestMainActivity) getActivity()).setIsFromSlug(Constants.FROM_SLUG);
                            FragmentController.addInformationFragment(getActivity(), mSlugVo.getCmsInfoVo(), "");

                        } else {
                            CommonUtills.showAlert(getActivity(), mSlugVo.getsMessage(), Constants.NO_ACTION);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SlugVo> call, Throwable t) {
                    CommonUtills.dismissProgressDialog();
                }
            });

        }

    }

    private void callDeleteUserProfile() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

        UserModel model = new UserModel();
        if (MissUGram.getApp(getActivity()) == null) {
            return;
        }
        model.setId(MissUGram.getApp(getActivity()).getUserModel().getId());


        //final Call<CommonVo> callDelete = apiInterface.deleteProfile(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), model);

        Map<String, String> params = new HashMap<String, String>();
        params.put("userid", String.valueOf(MissUGram.getApp(getActivity()).getUserModel().getId()));
        final Call<CommonVo> callDelete = apiInterface.deleteProfile(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), params);

        callDelete.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        getActivity().getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().clear().apply();
                        Constants.BY_PASS_SPLASH = true;
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.ACTION_LOGIN_FROM_SETTINGS);
                    } else {
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.NO_ACTION);
                    }
                }

            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.ms_et_myaccount:

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // Getting  if drawable right is clicked. If clicked we will change the image according for both my account and notifications.
                    if (motionEvent.getRawX() >= mTvMyAccount.getRight() - mTvMyAccount.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {


                    }
                }
                break;
            case R.id.ms_et_notifications:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (motionEvent.getRawX() >= mTvNotifications.getRight() - mTvNotifications.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                        doProcessDrawables();
                    }
                }
                break;
        }
        return false;
    }

    private void doProcessDrawables() {
        Drawable[] drawables = mTvNotifications.getCompoundDrawables();
        Drawable drawable = drawables[DRAWABLE_RIGHT];
        if (drawable.getConstantState().equals(ContextCompat.getDrawable(getActivity(), R.drawable.enable).getConstantState())) {
            Drawable drawableToSet = ContextCompat.getDrawable(getActivity(), R.drawable.disable);
            mTvNotifications.setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawableToSet, drawables[3]);

            if (CommonUtills.isNetworkAvailable(getActivity())) {
                callUpdateNotification("0");
            } else {
                CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
            }

        } else if (drawable.getConstantState().equals(ContextCompat.getDrawable(getActivity(), R.drawable.disable).getConstantState())) {
            Drawable drawableToSet = ContextCompat.getDrawable(getActivity(), R.drawable.enable);
            mTvNotifications.setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawableToSet, drawables[3]);

            if (CommonUtills.isNetworkAvailable(getActivity())) {
                callUpdateNotification("1");
            } else {
                CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
            }
        }
    }

    public void callUpdateNotification(final String p_val) {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

        NotificationRequestVo notificationRequestVo = new NotificationRequestVo();
        notificationRequestVo.setUserid(MissUGram.getApp(getActivity()) == null ? "0" : String.valueOf(MissUGram.getApp(getActivity()).getUserModel().getId()));
        notificationRequestVo.setPush_flag(p_val);

        final Call<CommonVo> callUpdateNotification = apiInterface.updateNotification(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), notificationRequestVo);
        callUpdateNotification.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.NO_ACTION);
                        if (p_val.equals("1")) {
                            CommonUtills.setPrefIsNotificationEnabled(getActivity(), true);
                        } else {
                            CommonUtills.setPrefIsNotificationEnabled(getActivity(), false);
                        }
                    } else {
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });
    }
}
