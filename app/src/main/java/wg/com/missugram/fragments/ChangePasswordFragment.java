package wg.com.missugram.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.wg.framework.util.CommonUtility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;


public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener {
    private EditText cpEtOldPassword;
    private EditText cpEtNewPassword;
    private EditText cpEtConfirmNewPassword;
    private LinearLayout cpLlSave;
    private LinearLayout cpLlCancel;


    public static ChangePasswordFragment newInstance() {
        return new ChangePasswordFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.changepassword_fragment_layout, container, false);
        initViews(root);
        initUI();
        return root;
    }

    private void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();

    }

    private void initViews(View root) {
        cpEtOldPassword = (EditText) root.findViewById(R.id.cp_et_oldpassword);
        cpEtNewPassword = (EditText) root.findViewById(R.id.cp_et_newpassword);
        cpEtConfirmNewPassword = (EditText) root.findViewById(R.id.cp_et_confirmnewpassword);
        cpLlSave = (LinearLayout) root.findViewById(R.id.cp_ll_save);
        cpLlCancel = (LinearLayout) root.findViewById(R.id.cp_ll_cancel);

        cpLlCancel.setOnClickListener(this);
        cpLlSave.setOnClickListener(this);

    }


    private void closeKeyBoard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), cpEtOldPassword);
            CommonUtility.hideKeyboard(getActivity(), cpEtNewPassword);
            CommonUtility.hideKeyboard(getActivity(), cpEtConfirmNewPassword);

        } catch (Throwable throwable) {

        }
    }

    private void validateUserData() {
        if (CommonUtills.checkLength(cpEtOldPassword.getText().toString().trim()) <= 0 || cpEtOldPassword.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_old_password), Constants.NO_ACTION);
        } else if (CommonUtills.checkLength(cpEtNewPassword.getText().toString().trim()) <= 0 || cpEtNewPassword.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_new_password), Constants.NO_ACTION);
        } else if (CommonUtills.checkLength(cpEtConfirmNewPassword.getText().toString().trim()) <= 0 || cpEtConfirmNewPassword.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_confirm_password), Constants.NO_ACTION);
        } else if (!CommonUtills.checkPassWordAndConfirmPassword(cpEtNewPassword.getText().toString().trim(), cpEtConfirmNewPassword.getText().toString().trim())) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_password_confirmspass_mismatch), Constants.NO_ACTION);
        } else {
            callChangePasswordWS();
        }
    }

    private void callChangePasswordWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        UserModel changepasswprdRequest = new UserModel();
        changepasswprdRequest.setEmail(MissUGram.getApp(getActivity()).getUserModel().getEmail());
        changepasswprdRequest.setPassword(cpEtOldPassword.getText().toString().trim());
        changepasswprdRequest.setNew_password(cpEtNewPassword.getText().toString().trim());
        changepasswprdRequest.setConfirm_password(cpEtConfirmNewPassword.getText().toString().trim());

        final Call<CommonVo> commonvo = apiInterface.changeUserPassword(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), changepasswprdRequest);
        commonvo.enqueue(new Callback<CommonVo>() {

            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                CommonUtills.dismissProgressDialog();


                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();

                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.ACTION_SETTINGS);
                    } else {
                        //handle some error message
                        CommonUtills.showAlert(getActivity(), commonVo.getsMessage(), Constants.NO_ACTION);
                    }
                }


            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cp_ll_save:
                closeKeyBoard();
                validateUserData();
                break;
            case R.id.cp_ll_cancel:
                FragmentController.addSettingsFragment(getActivity(), "");
                break;
        }
    }
}
