package wg.com.missugram.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestUpdateBuilder;
import com.wg.framework.util.CommonUtility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

public class CreateGroup extends BaseFragment implements OnClickListener {
    private static final String KEY_SELECTED_USERID_FOR_GROUP = "selected_userid_forgroup";
    final int DRAWABLE_TOP = 1;
    private EditText mcg_group_title;
    private ImageView mCgIvCmClose;
    private ImageView mCgIvCmSelectedImage;
    private TextView mCgTvUpload;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private File file = null;
    private ArrayList<Integer> Idlist;

    public static CreateGroup newInstance(ArrayList<Integer> Idlist) {
        CreateGroup mCreateGroup = new CreateGroup();
        Bundle bundle = new Bundle();
        bundle.putIntegerArrayList(KEY_SELECTED_USERID_FOR_GROUP, Idlist);
        mCreateGroup.setArguments(bundle);
        return mCreateGroup;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_create_group, container, false);
        initViews(root);
        initUI();
        extractArguments();
        addListener();
        return root;
    }

    private void addListener() {
        mCgIvCmClose.setOnClickListener(this);
        mCgTvUpload.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // Getting  if drawable top is clicked. If clicked we will open the dialog for image selection.
                    if (motionEvent.getRawX() >= mCgTvUpload.getTop() - mCgTvUpload.getCompoundDrawables()[DRAWABLE_TOP].getBounds().width()) {
                        try {
                            CommonUtility.hideKeyboard(getContext(), mcg_group_title);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                        selectImage();

                    }
                }
                return false;
            }
        });
    }

    private void initViews(View root) {
        mcg_group_title = (EditText) root.findViewById(R.id.mcg_title);
        mCgIvCmClose = (ImageView) root.findViewById(R.id.mcg_close);
        mCgIvCmSelectedImage = (ImageView) root.findViewById(R.id.mcg_iv_selectedimage);
        mCgTvUpload = (TextView) root.findViewById(R.id.mcg_tv_upload);
    }

    private void initUI() {

        ((TestMainActivity) getActivity()).updateToolbar();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    public void validateData() {

        if (CommonUtills.checkLength(mcg_group_title.getText().toString().trim()) <= 0 || mcg_group_title.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.str_empry_group), Constants.NO_ACTION);
        }  else {
            // call create group method here.
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), true);
            //leave occupant id list as blank if you want to join anyone in the group. or create pre loaded.
            QBChatDialog dialog = DialogUtils.buildDialog(mcg_group_title.getText().toString().trim(), QBDialogType.GROUP, Idlist);

            QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(final QBChatDialog result, Bundle params) {

                    MissUGram.getApp(getActivity()).getListOfGroupDialogIds().add(result.getDialogId());

                    if (file == null) {
                        CommonUtills.dismissProgressDialog();
                        FragmentController.removeFragment(getActivity(), FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                        ChatProvider.getInstance().sendSystemMessageAboutCreatingDialog(((TestMainActivity) getActivity()).getSystemMessagesManager(), result);
                        FragmentController.removeFragment(getActivity(), FragmentController.TAG_CREATE_GROUP_FRAGMENT);

                        FragmentController.addGrievanceChatFragment(getActivity());

                    } else {

                        ChatProvider.getInstance().uploadDialogPhoto(file, new QBEntityCallback<QBFile>() {
                            @Override
                            public void onSuccess(QBFile qbFile, Bundle bundle) {

                                result.setPhoto(qbFile.getId().toString());
                                QBRequestUpdateBuilder updateBuilder = new QBRequestUpdateBuilder();
                                QBRestChatService.updateGroupChatDialog(result, updateBuilder).performAsync(new QBEntityCallback<QBChatDialog>() {
                                    @Override
                                    public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                                        CommonUtills.dismissProgressDialog();
                                        FragmentController.removeFragment(getActivity(), FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);

                                        ChatProvider.getInstance().sendSystemMessageAboutCreatingDialog(((TestMainActivity) getActivity()).getSystemMessagesManager(), qbChatDialog);
                                        FragmentController.removeFragment(getActivity(), FragmentController.TAG_CREATE_GROUP_FRAGMENT);

                                        FragmentController.addGrievanceChatFragment(getActivity());

                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        CommonUtills.dismissProgressDialog();
                                    }
                                });

                            }

                            @Override
                            public void onError(QBResponseException e) {
                                CommonUtills.dismissProgressDialog();
                            }
                        });


                    }


                }

                @Override
                public void onError(QBResponseException responseException) {
                    CommonUtills.dismissProgressDialog();
                }
            });

        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            mCgTvUpload.setVisibility(View.GONE);
            mCgIvCmSelectedImage.setVisibility(View.VISIBLE);

            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                file = new File(CommonUtills.getRealPathFromURI(getActivity(), CommonUtills.getImageUri(getActivity(), bm)));
                ImageLoader.getInstance().displayImage(data.getData().toString(), mCgIvCmSelectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mCgTvUpload.setVisibility(View.VISIBLE);
            mCgIvCmSelectedImage.setVisibility(View.GONE);
        }
    }

    private void onCaptureImageResult(Intent data) {

        mCgIvCmSelectedImage.setVisibility(View.VISIBLE);
        mCgTvUpload.setVisibility(View.GONE);

        File sd = Environment.getExternalStorageDirectory();
        File dest = new File(sd, String.valueOf(System.currentTimeMillis()) + ".jpg");
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        try {
            FileOutputStream out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageLoader.getInstance().displayImage("file://" + dest.getAbsolutePath(), mCgIvCmSelectedImage);
        file = new File(dest.getAbsolutePath());
    }

    private void extractArguments() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            Idlist = new ArrayList<>();
            Idlist = bundle.getIntegerArrayList(KEY_SELECTED_USERID_FOR_GROUP);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.mcg_close:
                mCgTvUpload.setVisibility(View.VISIBLE);
                mCgIvCmSelectedImage.setVisibility(View.GONE);
                break;

        }
    }

    private void selectImage() {
        final CharSequence[] items = {getActivity().getResources().getString(R.string.str_takephoto), getActivity().getResources().getString(R.string.str_choosefromlibrary),
                getActivity().getResources().getString(R.string.dialog_btn_cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.str_add_photo));
        builder.setItems(items, (dialog, item) -> {

            if (items[item].equals(getActivity().getResources().getString(R.string.str_takephoto))) {
                cameraIntent();
            } else if (items[item].equals(getActivity().getResources().getString(R.string.str_choosefromlibrary))) {

                galleryIntent();
            } else if (items[item].equals(getActivity().getResources().getString(R.string.dialog_btn_cancel))) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }
}
