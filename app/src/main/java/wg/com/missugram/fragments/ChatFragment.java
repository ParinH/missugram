package wg.com.missugram.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBChatDialogMessageSentListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.request.QBDialogRequestBuilder;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.wg.framework.log.CustomLogHandler;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.adapter.ChatAdapter;
import wg.com.missugram.adapter.StickerImageAdapter;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatModel;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

public class ChatFragment extends BaseFragment implements View.OnClickListener {
    private static final String KEY_QUICKBLOXID = "ID";
    private static final String KEY_FROM_WHERE = "fromWhere";
    private static final String KEY_GROUP_DIALOG_ID = "GroupdialogID";
    private static final String KEY_NAME = "chatusername";
    private static final String KEY_IMAGEURL = "userimageurl";
    int recepinetQuickbloxid = -1;
    String groupDialogid = "";
    QBChatDialog qbChatDialogFoPrivaterChat, qbChatDialogFoGroupChat;
    QBIncomingMessagesManager incomingMessagesManager;
    ChatAdapter mChatAdapter;
    ArrayList<ChatModel> messageList;
    QBUser Currentuser;
    int fromWhere = -1;
    String otherUserimageUrl;
    private RecyclerView mrvChat;
    private EditText mchat_et_comment;
    private RecyclerView mchat_rv_stickers;
    private ImageView mchat_iv_addimages, mchat_iv_sendchat;
    private ArrayList<String> alStickerImagePath;
    private LinearLayoutManager mLinearLayoutManager;
    MyThreadListener listener = new MyThreadListener() {

        @Override
        public void threadFinished(final QBChatDialog qbChatDialog, String msg) {

            if (fromWhere == Constants.FOR_PRIVATE_CHAT) {

                getActivity().runOnUiThread(() -> ChatProvider.getInstance().loadChatHistory(qbChatDialog, new QBEntityCallback<ArrayList<QBChatMessage>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {

                        processPrivateChatResponse(qbChatMessages);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        CommonUtills.dismissProgressDialog();

                    }
                }));
            } else if (fromWhere == Constants.FOR_GROUP_CHAT) {

                getActivity().runOnUiThread(() -> ChatProvider.getInstance().loadGroupChatWithUsers(qbChatDialog, Currentuser.getId()));
            }

        }
    };
    private String deletedGroupId;
    private GridLayoutManager mGridLayoutManager;
    private StickerImageAdapter mStickerImageAdapter;
    private Context mContext;

    public static ChatFragment newInstance(int quickbloxId, int fromWhere, String groupDialogId, String name, String imageUrl) {
        ChatFragment chatFragment = new ChatFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_QUICKBLOXID, quickbloxId);
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        bundle.putString(KEY_GROUP_DIALOG_ID, groupDialogId);
        bundle.putString(KEY_NAME, name);
        bundle.putString(KEY_IMAGEURL, imageUrl);

        chatFragment.setArguments(bundle);
        return chatFragment;
    }

    private void processPrivateChatResponse(ArrayList<QBChatMessage> qbChatMessages) {
        if (qbChatMessages != null && qbChatMessages.size() > 0) {
            ChatModel model;
            for (QBChatMessage message : qbChatMessages) {

                if (!message.getBody().equalsIgnoreCase("null") && message.getBody() != null && !message.getBody().isEmpty()) {
                    model = new ChatModel();
                    model.setQbChatMessage(message);
                    messageList.add(model);
                }
            }


            setReceiverPhotoForPrivateChat(Integer.valueOf(otherUserimageUrl));

            loadChatMessageList();
        } else {
            CommonUtills.dismissProgressDialog();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.chatlisting_layout_fragment, container, false);
        initViews(root);

        extractArguments();
        initUI();
        listAllImages();
        setListeners();
        return root;
    }

    private void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();

    }

    private void extractArguments() {
        Bundle bundle = getArguments();

        if (bundle != null) {

            if (bundle.getInt(KEY_FROM_WHERE) == Constants.FOR_PRIVATE_CHAT) {
                recepinetQuickbloxid = bundle.getInt(KEY_QUICKBLOXID, -1);
                fromWhere = Constants.FOR_PRIVATE_CHAT;
                doRequisitesForPrivateChat();

            } else if (bundle.getInt(KEY_FROM_WHERE) == Constants.FOR_GROUP_CHAT) {
                groupDialogid = bundle.getString(KEY_GROUP_DIALOG_ID, "");
                fromWhere = Constants.FOR_GROUP_CHAT;
                doRequisitesForGroupChat();
            }

            new Handler().postDelayed(() -> {
                otherUserimageUrl = bundle.getString(KEY_IMAGEURL, "-1");

                ((TestMainActivity) getActivity()).initChatUI(bundle.getString(KEY_NAME, ""), bundle.getString(KEY_IMAGEURL, "-1"), fromWhere);
            }, 250);


        }
    }

    private void setReceiverPhotoForPrivateChat(int fileId) {
        for (int i = 0; i < messageList.size(); i++) {
            if (messageList.get(i).getQbChatMessage().getSenderId() != Currentuser.getId()) {
                messageList.get(i).setFileId(fileId);
            }
        }
    }

    private void loadChatMessageList() {

        if (mChatAdapter == null) {

            ArrayList<ChatModel> modelsTemp = new ArrayList<>();

            if (messageList != null) {
                modelsTemp.addAll(messageList);
                messageList.clear();
                for (int i = modelsTemp.size() - 1; i >= 0; i--) {

                    messageList.add(modelsTemp.get(i));
                }
                mrvChat.setLayoutManager(mLinearLayoutManager);
                mChatAdapter = new ChatAdapter(getActivity(), messageList, fromWhere);
                mrvChat.setAdapter(mChatAdapter);
            }
        }
        CommonUtills.dismissProgressDialog();

        if (messageList != null) {
            callScrollToPosition(messageList);
        }

    }

    private void processNewPaginationData(ArrayList<ChatModel> pChatModels) {
        if (mChatAdapter != null) {
            //Create a temp array list. Since new array list contain the items that needs to be added from last item added to top in array list.
            ArrayList<ChatModel> pChatModelsTemp = new ArrayList<>();

            pChatModelsTemp.addAll(pChatModels);
            pChatModels.clear();

            // So we are reversing the the list and adding the items again in list after clearing it and assigning into temp list.
            for (int i = pChatModelsTemp.size() - 1; i >= 0; i--) {

                pChatModels.add(pChatModelsTemp.get(i));
            }
            pChatModelsTemp.clear();

            //now adding list to the 0 th position.
            mChatAdapter.setChatMessage(pChatModels);
        }

    }

    private void reverseTheList() {

        if (mChatAdapter == null) {

            ArrayList<ChatModel> modelsTemp = new ArrayList<>();

            if (messageList != null) {
                modelsTemp.addAll(messageList);
                messageList.clear();
                for (int i = modelsTemp.size() - 1; i >= 0; i--) {

                    messageList.add(modelsTemp.get(i));
                }
                mrvChat.setLayoutManager(mLinearLayoutManager);
                mChatAdapter = new ChatAdapter(getActivity(), messageList, fromWhere);
                mrvChat.setAdapter(mChatAdapter);
            }
        }

        if (messageList != null) {
            callScrollToPosition(messageList);
        }
    }

    private void setListeners() {
        mchat_iv_addimages.setOnClickListener(this);
        mchat_iv_sendchat.setOnClickListener(this);
    }

    private void initViews(View root) {
        mrvChat = (RecyclerView) root.findViewById(R.id.mrvChat);
        mrvChat.setHasFixedSize(true);
        mrvChat.setItemViewCacheSize(20);
        mrvChat.setDrawingCacheEnabled(true);
        mchat_rv_stickers = (RecyclerView) root.findViewById(R.id.mchat_rv_stickers);

        mchat_et_comment = (EditText) root.findViewById(R.id.mchat_et_comment);
        mchat_iv_addimages = (ImageView) root.findViewById(R.id.mchat_iv_addimages);
        mchat_iv_sendchat = (ImageView) root.findViewById(R.id.iv_sendchat);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        messageList = new ArrayList<>();

        Currentuser = new QBUser(CommonUtills.getPrefCurrentChatUname(getActivity()), CommonUtills.getPrefCurrentChatUname(getActivity()));
        Currentuser.setId(CommonUtills.getPrefChatId(getActivity()));
        Currentuser.setPassword(CommonUtills.getPrefCurrentChatUname(getActivity()));
        Currentuser.setEmail(CommonUtills.getPrefCurrentChatUname(getActivity()));


        ChatProvider.getInstance().setmOnUserWithMessageResponse(chatModels -> {
            if (chatModels != null && chatModels.size() > 0) {
                messageList = chatModels;
                if (mChatAdapter == null) {
                    loadChatMessageList();
                } else {
                    CommonUtills.dismissProgressDialog();
                    processNewPaginationData(messageList);
                }
            } else {
                CommonUtills.dismissProgressDialog();
            }

        });
    }

    public void removeUser()

    {
        showYesNoDialog1(getString(R.string.str_leave_group), -1, yes -> {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), true);
            QBDialogRequestBuilder requestBuilder = new QBDialogRequestBuilder();
            requestBuilder.removeUsers(Currentuser.getId()); // remove any users

            deletedGroupId = qbChatDialogFoGroupChat.getDialogId();
            QBRestChatService.updateGroupChatDialog(qbChatDialogFoGroupChat, requestBuilder).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                    CommonUtills.dismissProgressDialog();
                    FragmentController.removeThisFragment(getActivity(), FragmentController.TAG_CHAT_FRAGMENT);
                    new Handler().postDelayed(new Runnable() {
                                                  @Override
                                                  public void run() {
                                                      if (FragmentController.getFrontFragmentTag(TestMainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                                                          GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(TestMainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                                                          grievanceChatFragment.setDeletedGroupId(deletedGroupId, qbChatDialogFoGroupChat.getName());
                                                      }

                                                  }
                                              }
                            , 250);

                }

                @Override
                public void onError(QBResponseException e) {
                    CommonUtills.dismissProgressDialog();
                }
            });
        });


    }

    public void listAllImages() {

        try {
            alStickerImagePath = new ArrayList<>();
            alStickerImagePath = new ArrayList<String>(Arrays.asList(getActivity().getAssets().list(Constants.ASSEST_IMAGE_FOLDERNAME)));
            String[] images = getActivity().getAssets().list(Constants.ASSEST_IMAGE_FOLDERNAME);
            Arrays.sort(images, new AlphanumericSorting());
            alStickerImagePath = new ArrayList<String>(Arrays.asList(images));

            prepareStickerImageAdapter();

        } catch (IOException e) {
            CustomLogHandler.printErrorlog(e);
        }
    }

    private void prepareStickerImageAdapter() {
        mGridLayoutManager = new GridLayoutManager(getActivity(), 10);
        mchat_rv_stickers.setItemAnimator(new DefaultItemAnimator());
        mchat_rv_stickers.setLayoutManager(mGridLayoutManager);
        mStickerImageAdapter = new StickerImageAdapter(alStickerImagePath, getActivity());
        mchat_rv_stickers.setAdapter(mStickerImageAdapter);
        mStickerImageAdapter.setOnRecyclerItemListener(tag -> {
            mchat_rv_stickers.setVisibility(View.GONE);
            if (tag.indexOf(".") > 0)
                tag = tag.substring(0, tag.lastIndexOf("."));
            if (fromWhere == Constants.FOR_PRIVATE_CHAT) {

                // If you pass blank to a message in quickblox message will process but lister for message sent  listener will not be invoked. so passing unique text instead of blank.
                ChatProvider.getInstance().sendMessage(Constants.KEY_MESSAGE_DEFAULT, qbChatDialogFoPrivaterChat, recepinetQuickbloxid, null, Currentuser.getId(), tag);

            } else {
                ChatProvider.getInstance().sendGroupMessage(qbChatDialogFoGroupChat, Constants.KEY_MESSAGE_DEFAULT, Currentuser.getId(), null, tag);
            }

        });
    }

    private void callScrollToPosition(final ArrayList<ChatModel> pChatModels) {
        new Handler().postDelayed(() -> mrvChat.scrollToPosition(mrvChat.getAdapter().getItemCount() - 1), 500);
    }

    private void doRequisitesForGroupChat() {
        //can come here after group creation or from the list of groups.

        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), true);

        QBRestChatService.getChatDialogById(groupDialogid).performAsync(new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                if (TextUtils.isEmpty(qbChatDialog.getRoomJid())) {
                    ChatProvider.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {

                        @Override
                        public void onSuccess(Void aVoid, Bundle bundle) {

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!QBChatService.getInstance().isLoggedIn()) {
                                        try {
                                            QBChatService.getInstance().login(Currentuser);
                                        } catch (XMPPException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (SmackException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }).start();
                            qbChatDialogFoGroupChat = qbChatDialog;
                            registerConnection();
                            listener.threadFinished(qbChatDialogFoGroupChat, null);

                        }

                        @Override
                        public void onError(QBResponseException e) {

                            CommonUtills.dismissProgressDialog();
                        }
                    });
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (!QBChatService.getInstance().isLoggedIn()) {
                                try {
                                    QBChatService.getInstance().login(Currentuser);
                                    qbChatDialogFoGroupChat = qbChatDialog;
                                    registerConnection();
                                    listener.threadFinished(qbChatDialogFoGroupChat, null);
                                } catch (XMPPException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (SmackException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ChatProvider.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid, Bundle bundle) {
                                        qbChatDialogFoGroupChat = qbChatDialog;
                                        registerConnection();
                                        listener.threadFinished(qbChatDialogFoGroupChat, null);
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        CommonUtills.dismissProgressDialog();
                                    }
                                });

                            }
                        }
                    }).start();

                }

            }

            @Override
            public void onError(QBResponseException e) {
                CommonUtills.dismissProgressDialog();
            }
        });

    }

    private void doRequisitesForPrivateChat()

    {

        QBChatDialog dialog1 = DialogUtils.buildPrivateDialog(recepinetQuickbloxid);
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), true);
        QBRestChatService.createChatDialog(dialog1).performAsync(new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(final QBChatDialog qbChatDialog, Bundle bundle) {

                new Thread(() -> {
                    try {
                        CommonUtills.setPrefPrivateDialogId(getActivity(), qbChatDialog.getDialogId());
                        qbChatDialogFoPrivaterChat = qbChatDialog;
                        if (!QBChatService.getInstance().isLoggedIn()) {

                            QBChatService.getInstance().login(Currentuser);
                        }
                        qbChatDialogFoPrivaterChat.initForChat(QBChatService.getInstance());


                        registerConnection();
                        listener.threadFinished(qbChatDialog, null);
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                        try {
                            QBChatService.getInstance().logout();
                        } catch (SmackException.NotConnectedException e1) {
                            e1.printStackTrace();
                        }
                        CommonUtills.dismissProgressDialog();
                    } catch (XMPPException e) {
                        CommonUtills.dismissProgressDialog();
                        try {
                            QBChatService.getInstance().logout();
                        } catch (SmackException.NotConnectedException e1) {
                            e1.printStackTrace();
                        }
                        e.printStackTrace();
                    } catch (IOException e) {
                        CommonUtills.dismissProgressDialog();
                        try {
                            QBChatService.getInstance().logout();
                        } catch (SmackException.NotConnectedException e1) {
                            e1.printStackTrace();
                        }
                        e.printStackTrace();
                    } catch (SmackException e) {
                        CommonUtills.dismissProgressDialog();
                        try {
                            QBChatService.getInstance().logout();
                        } catch (SmackException.NotConnectedException e1) {
                            e1.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                }).start();
            }

            @Override
            public void onError(QBResponseException e) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mchat_iv_addimages:
                if (mchat_rv_stickers.getVisibility() == View.VISIBLE) {
                    mchat_rv_stickers.setVisibility(View.GONE);
                } else {
                    mchat_rv_stickers.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.iv_sendchat:
                String message = mchat_et_comment.getText().toString().trim();

                if (TextUtils.isEmpty(message)) {
                    CommonUtills.showAlert(getActivity(), getString(R.string.validation_chat_enter_message), Constants.NO_ACTION);

                } else {
                    if (fromWhere == Constants.FOR_PRIVATE_CHAT) {
                        // sending a text message so passing blank as a sticker image.

                        ChatProvider.getInstance().sendMessage(message, qbChatDialogFoPrivaterChat, recepinetQuickbloxid, null, Currentuser.getId(), null);

                    } else {

                        ChatProvider.getInstance().sendGroupMessage(qbChatDialogFoGroupChat, message, Currentuser.getId(), null, null);

                    }
                }

                mchat_et_comment.getText().clear();
                break;
        }
    }

    private void registerConnection() {
        registerMessageListener();
    }

    public QBChatDialog getQbChatDialogFoGroupChat() {
        return qbChatDialogFoGroupChat;
    }

    public QBChatDialog getQbChatDialogFoPrivaterChat() {
        return qbChatDialogFoPrivaterChat;
    }

    private void registerMessageListener() {

        if (incomingMessagesManager == null) {
            incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        }

        incomingMessagesManager.addDialogMessageListener(new QBChatDialogMessageListener() {
            @Override
            public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {

                if (integer != Currentuser.getId()) {

                    ChatModel model = new ChatModel();
                    if (qbChatMessage.getAttachments() != null && qbChatMessage.getAttachments().size() > 0) {
                        List<QBAttachment> qbAttachments = new ArrayList<QBAttachment>();
                        qbAttachments.addAll(qbChatMessage.getAttachments());
                        model.setFileId(Integer.valueOf(qbAttachments.get(0).getId()));
                    } else {

                        if (fromWhere == Constants.FOR_PRIVATE_CHAT) {
                            model.setFileId(Integer.valueOf(otherUserimageUrl));
                        }
                    }

                    model.setQbChatMessage(qbChatMessage);

                    if (mChatAdapter == null) {
                        // reverseTheList();
                        loadChatMessageList();
                    } else {
                        mChatAdapter.updateChatList(model);
                        callScrollToPosition(messageList);
                    }
                }

            }

            @Override
            public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {

            }
        });

        if (fromWhere == Constants.FOR_GROUP_CHAT) {

            qbChatDialogFoGroupChat.addMessageSentListener(new QBChatDialogMessageSentListener() {
                @Override
                public void processMessageSent(String s, QBChatMessage qbChatMessage) {
                    if (qbChatMessage.getRecipientId() != null && qbChatMessage.getRecipientId() != Currentuser.getId()) {
                        ChatModel model = new ChatModel();
                        model.setQbChatMessage(qbChatMessage);
                        // messageList.add(model);
                        mChatAdapter.updateChatList(model);
                        CommonUtills.dismissProgressDialog();

                        callScrollToPosition(messageList);
                    }
                }

                @Override
                public void processMessageFailed(String s, QBChatMessage qbChatMessage) {

                }
            });

        } else {
            qbChatDialogFoPrivaterChat.addMessageSentListener(new QBChatDialogMessageSentListener() {
                @Override
                public void processMessageSent(String s, QBChatMessage qbChatMessage) {
                    if (qbChatMessage.getRecipientId() != Currentuser.getId()) {
                        ChatModel model = new ChatModel();
                        model.setQbChatMessage(qbChatMessage);


                        mChatAdapter.updateChatList(model);
                        CommonUtills.dismissProgressDialog();

                        callScrollToPosition(messageList);
                    }
                }

                @Override
                public void processMessageFailed(String s, QBChatMessage qbChatMessage) {

                }
            });
        }
    }


    public interface MyThreadListener {
        public void threadFinished(QBChatDialog qbChatDialog, String msg);
    }

    public class AlphanumericSorting implements Comparator {
        /**
         * The compare method that compares the alphanumeric strings
         */
        public int compare(Object firstObjToCompare, Object secondObjToCompare) {
            String firstString = firstObjToCompare.toString();
            String secondString = secondObjToCompare.toString();

            if (secondString == null || firstString == null) {
                return 0;
            }

            int lengthFirstStr = firstString.length();
            int lengthSecondStr = secondString.length();

            int index1 = 0;
            int index2 = 0;

            while (index1 < lengthFirstStr && index2 < lengthSecondStr) {
                char ch1 = firstString.charAt(index1);
                char ch2 = secondString.charAt(index2);

                char[] space1 = new char[lengthFirstStr];
                char[] space2 = new char[lengthSecondStr];

                int loc1 = 0;
                int loc2 = 0;

                do {
                    space1[loc1++] = ch1;
                    index1++;

                    if (index1 < lengthFirstStr) {
                        ch1 = firstString.charAt(index1);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch1) == Character.isDigit(space1[0]));

                do {
                    space2[loc2++] = ch2;
                    index2++;

                    if (index2 < lengthSecondStr) {
                        ch2 = secondString.charAt(index2);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch2) == Character.isDigit(space2[0]));

                String str1 = new String(space1);
                String str2 = new String(space2);

                int result;

                if (Character.isDigit(space1[0]) && Character.isDigit(space2[0])) {
                    Integer firstNumberToCompare = new Integer(Integer
                            .parseInt(str1.trim()));
                    Integer secondNumberToCompare = new Integer(Integer
                            .parseInt(str2.trim()));
                    result = firstNumberToCompare.compareTo(secondNumberToCompare);
                } else {
                    result = str1.compareTo(str2);
                }

                if (result != 0) {
                    return result;
                }
            }
            return lengthFirstStr - lengthSecondStr;
        }
    }
}
