package wg.com.missugram.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.iid.FirebaseInstanceId;
import com.quickblox.users.model.QBUser;
import com.wg.framework.log.CustomLogHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.LoginResponseVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;


public class SplashFragment extends BaseFragment {


    final QBUser user = new QBUser();

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.splash_layout, container, false);
        user.setEmail(CommonUtills.getPrefCurrentChatUname(getActivity()));
        user.setPassword(CommonUtills.getPrefCurrentChatPwd(getActivity()));
        user.setId(CommonUtills.getPrefChatId(getActivity()));

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        new Handler().postDelayed(() -> {
            if (CommonUtills.getPrefUserEmail(getActivity()).isEmpty()) {
                FragmentController.removeFragment(getActivity(), FragmentController.TAG_SPLASH_FRAGMENT);
                FragmentController.addLoginFragment(getActivity());
            } else {

                if (CommonUtills.isNetworkAvailable(getActivity())) {
                    callLoginUserWS();
                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.ACTION_FINISH);
                }
            }
        }, 3000);

    }

    private void callLoginUserWS() {
        showProgressBar();
        final UserModel userloginRequest = new UserModel();
        userloginRequest.setEmail(CommonUtills.getPrefUserEmail(getActivity()));
        userloginRequest.setPassword(CommonUtills.getPrefUserPassword(getActivity()));
        userloginRequest.setDevice_type("2");
        checkTokenStored();
        userloginRequest.setDevice_token(CommonUtills.getPrefFirebaseDeviceToken(getActivity()));

        final Call<LoginResponseVo> loginresponse = apiInterface.loginUser(userloginRequest);
        loginresponse.enqueue(new Callback<LoginResponseVo>() {
            @Override
            public void onResponse(Call<LoginResponseVo> call, Response<LoginResponseVo> response) {
                hideProgressBar();
                if (response != null && response.body() != null) {
                    LoginResponseVo userModel = response.body();
                    if (userModel.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

                        MissUGram.getApp(getActivity()).setUserModel(userModel.getUserInfo());
                        TokenKeeper.getInstance().storeTokens(getActivity(), userModel.getUserInfo().getToken(), 3600);
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            CommonUtills.setPrefFirstName(getActivity(), userModel.getUserInfo().getFirst_name());
                            CommonUtills.setPrefLastName(getActivity(), userModel.getUserInfo().getLast_name());
                            CommonUtills.setPrefUserAvatar(getActivity(), userModel.getUserInfo().getAvatar());
                            CommonUtills.setPrefIsNotificationEnabled(getActivity(), userModel.getUserInfo().getPush_flag().equals("1") ? true : false);
                            CommonUtills.setPrefId(getActivity(), userModel.getUserInfo().getId());
                            FragmentController.removeFragment(getActivity(), FragmentController.TAG_SPLASH_FRAGMENT);

                            getActivity().startActivity(new Intent(getActivity(), TestMainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                        }
                    } else {
                        CommonUtills.showAlert(getActivity(), userModel.getsMessage(), Constants.ACTION_MAIN);
                    }
                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.str_something_went_wrong), Constants.NO_ACTION);
                }
            }

            @Override
            public void onFailure(Call<LoginResponseVo> call, Throwable t) {
                hideProgressBar();
                if (getActivity() != null && !getActivity().isFinishing()) {
                    CommonUtills.showAlert(getActivity(), getString(R.string.str_something_went_wrong), Constants.ACTION_FINISH);
                }
            }
        });

    }

    private void checkTokenStored() {

        if (CommonUtills.getPrefFirebaseDeviceToken(getActivity()) == null || CommonUtills.getPrefFirebaseDeviceToken(getActivity()).equals("test")) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            CommonUtills.setPrefFirebaseDeviceToken(getActivity(), refreshedToken);
            CustomLogHandler.printDebuglog(SplashFragment.this.getClass().getSimpleName(), "token for fcm in splash fragment" + refreshedToken);
        }

    }
}