package wg.com.missugram.fragments;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBRoster;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.adapter.ChatUserListingAdapter;
import wg.com.missugram.adapter.FriendListAdapter;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatUserHistoryModel;
import wg.com.missugram.model.ContactResponseVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;
import wg.com.missugram.utils.VerticalDividerItemDecoration;


public class GrievanceChatFragment extends BaseFragment implements View.OnClickListener {
    protected int pastVisibleItems, visibleItemCount, totalItemCountPrivate;
    QBRoster qbRoster = null;
    boolean isContactsClicked = false;
    boolean isGroupClicked = false;
    ArrayList<ChatUserHistoryModel> mUserHistoryGroup;
    ChatUserListingAdapter.OnRecyclerItemClickListener onRecyclerItemClickListener = (id, type, dialogId, name, url) -> {

        FragmentController.addChatFragment(getActivity(), id, type == QBDialogType.PRIVATE ? Constants.FOR_PRIVATE_CHAT : Constants.FOR_GROUP_CHAT, String.valueOf(dialogId), name, url);
    };
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<UserModel> commonVoArrayList;
    private ArrayList<ChatUserHistoryModel> mSingleChatUserHistory;
    private FriendListAdapter friendListAdapter;
    private ChatUserListingAdapter chatUserListingAdapter;
    private EditText mEtSearch;
    private ImageView mIvClear;
    private RecyclerView mrvChatHistoryUser;
    private RecyclerView mrvFriendlistchat;
    private View mvw_chat, mvw_groupchat, mvw_contacts;
    private LinearLayout mlnr_contacts, mlnr_group_chat, mlnr_chat;
    private TextView mTvNoRecords_chat;
    private int currentPageContacts = 1;
    private boolean isPullingMoreResult;
    private int currentPage = 1;
    private String deletedGroupId;

    public static GrievanceChatFragment newInstance() {
        return new GrievanceChatFragment();
    }

    public void setDeletedGroupId(String deletedGroupId,String dilaogname) {
        this.deletedGroupId = deletedGroupId;
        //remove the record based on the dialogid from a list;

        if (mUserHistoryGroup != null && mUserHistoryGroup.size() > 0) {

            for (int i = 0; i < mUserHistoryGroup.size(); i++) {
                if (mUserHistoryGroup.get(i).getChatDialog() != null && mUserHistoryGroup.get(i).getChatDialog().getName().equalsIgnoreCase(dilaogname))
                    mUserHistoryGroup.remove(i);
                chatUserListingAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.layout_grievancechat_fragment, container, false);
        initViews(root);
        addListener();
        addScrollListener();
        initUI();
        setListeners();

        getDialogForChatHistory(QBDialogType.PRIVATE);
        return root;
    }


    private void addScrollListener() {

        mrvFriendlistchat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCountPrivate = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isPullingMoreResult) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCountPrivate) {
                            isPullingMoreResult = true;
                            currentPage++;
                            callContactsWS();
                        }
                    }
                }
            }
        });

    }


    public void callAndUpdate(ChatUserHistoryModel historyModel) {
        chatUserListingAdapter.updateHistoryChat(historyModel);
    }

    public void checkIfNewMessageArrivedForGroup(QBChatDialog chatDialog) {

            QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
            requestBuilder.lt("updated_at", String.valueOf(System.currentTimeMillis()));
            if (chatDialog != null&&chatDialog.getUnreadMessageCount()!=null&&chatDialog.getUnreadMessageCount()>0) {
                CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

                QBRestChatService.getChatDialogById(chatDialog.getDialogId()).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < mUserHistoryGroup.size(); i++) {

                                    String historyDialogId = mUserHistoryGroup.get(i).getChatDialog().getDialogId();
                                    String iterativeDialogId = qbChatDialog.getDialogId();
                                    if (iterativeDialogId != null && historyDialogId != null && historyDialogId.equalsIgnoreCase(iterativeDialogId)) {
                                        if (qbChatDialog.getLastMessage() != null) {

                                            ChatUserHistoryModel model = mUserHistoryGroup.get(i);
                                            model.setChatuser_status(qbChatDialog.getLastMessage());
                                            model.setUnread_messagecount(qbChatDialog.getUnreadMessageCount());

                                            mUserHistoryGroup.remove(i);
                                            mUserHistoryGroup.add(model);

                                            getActivity().runOnUiThread(() ->{CommonUtills.dismissProgressDialog();
                                                callAndUpdate(model);
                                            });
                                            break;
                                        }
                                    }
                                }
                            }
                        }).start();


                    }

                    @Override
                    public void onError(QBResponseException e) {
                        CommonUtills.dismissProgressDialog();
                    }
                });
            }

    }

    public void checkIfNewMessageArrived(QBChatDialog dialog) {

            if (dialog != null) {
                if (dialog.getUnreadMessageCount()!=null&&dialog.getUnreadMessageCount() > 0) {
                    CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
                    QBRestChatService.getChatDialogById(dialog.getDialogId()).performAsync(new QBEntityCallback<QBChatDialog>() {
                        @Override
                        public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    for (int i = 0; i < mSingleChatUserHistory.size(); i++)

                                    {

                                        String name = mSingleChatUserHistory.get(i).getChatuser_name();
                                        if (qbChatDialog.getName().equalsIgnoreCase(name)) {


                                            if (qbChatDialog.getLastMessage() != null) {
                                                ChatUserHistoryModel model = mSingleChatUserHistory.get(i);
                                                model.setChatuser_status(qbChatDialog.getLastMessage());
                                                model.setUnread_messagecount(qbChatDialog.getUnreadMessageCount());

                                                mSingleChatUserHistory.remove(i);
                                                mSingleChatUserHistory.add(model);

                                                getActivity().runOnUiThread(() -> {
                                                    CommonUtills.dismissProgressDialog();
                                                    callAndUpdate(model);
                                                });
                                                break;
                                            }

                                        }

                                    }
                                }
                            }).start();


                        }

                        @Override
                        public void onError(QBResponseException e) {
                            CommonUtills.dismissProgressDialog();
                        }
                    });
                }
            }
    }

    public void getDialogForChatHistory(QBDialogType qbDialogType) {

        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.sortAsc("last_message_date_sent");

        if (qbDialogType == QBDialogType.PRIVATE) {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
            QBRestChatService.getChatDialogs(qbDialogType, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                @Override
                public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {

                    if (qbChatDialogs != null && qbChatDialogs.size() > 0) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Collection<Integer> mCollectionOfUserIds = null;
                                ArrayList<List<Integer>> mListOfChatUserIds = null;

                                if (qbChatDialogs.size() > 0) {

                                    mCollectionOfUserIds = new ArrayList<>();
                                    mListOfChatUserIds = new ArrayList<>();

                                    for (QBChatDialog qbChatDialog : qbChatDialogs) {

                                        mListOfChatUserIds.add(qbChatDialog.getOccupants());
                                    }
                                }


                                for (int i = 0; i < (mListOfChatUserIds != null ? mListOfChatUserIds.size() : 0); i++) {

                                    List<Integer> mIntegers = mListOfChatUserIds.get(i);
                                    int size = mIntegers.size();
                                    for (int j = 0; j < size; j++) {

                                        if (mIntegers.get(j) != CommonUtills.getPrefChatId(getActivity())) {
                                            mCollectionOfUserIds.add(mIntegers.get(j));
                                        }
                                    }

                                }

                                QBPagedRequestBuilder mQbRequestGetBuilder = new QBPagedRequestBuilder();
                                mQbRequestGetBuilder.setPage(1);
                                mQbRequestGetBuilder.setPerPage(100);

                                Collection<Integer> finalMCollectionOfUserIds = mCollectionOfUserIds;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        QBUsers.getUsersByIDs(finalMCollectionOfUserIds, mQbRequestGetBuilder).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
                                            @Override
                                            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {

                                                commonVoArrayList = new ArrayList<>();
                                                mSingleChatUserHistory = new ArrayList<>();

                                                new Thread(() -> {
                                                    if (qbUsers.size() > 0) {
                                                        for (QBUser qbUser : qbUsers) {


                                                            ChatUserHistoryModel model = new ChatUserHistoryModel();

                                                            int id = qbUser.getId() == null ? -1 : qbUser.getId();

                                                            model.setChatuser_name(qbUser.getFullName() == null ? qbUser.getLogin() : qbUser.getFullName());
                                                            model.setChatuser_id(id);
                                                            model.setChatuser_fileid(qbUser.getFileId() == null ? -1 : qbUser.getFileId());


                                                            model.setType(QBDialogType.PRIVATE);

                                                            for (QBChatDialog qbChatDialog : qbChatDialogs) {
                                                                if (qbChatDialog.getName().trim().equalsIgnoreCase(model.getChatuser_name().trim())) {


                                                                    model.setUnread_messagecount(qbChatDialog.getUnreadMessageCount());
                                                                    model.setChatuser_status(qbChatDialog.getLastMessage());
                                                                    model.setLastMessageDateSentPrivateChat(qbChatDialog.getLastMessageDateSent());
                                                                }
                                                            }

                                                            mSingleChatUserHistory.add(model);
                                                        }


                                                        //calling it for adding a groups also in a recent chat listing.

                                                        // getRecentGroup();
                                                        getActivity().runOnUiThread(() -> {
                                                            CommonUtills.dismissProgressDialog();
                                                            setList(mSingleChatUserHistory);
                                                            MissUGram.getApp(getActivity()).setmChatUserHistoryModelsForRecentChatList(mSingleChatUserHistory);
                                                        });

                                                    }
                                                }).start();

                                            }

                                            @Override
                                            public void onError(QBResponseException e) {
                                                CommonUtills.dismissProgressDialog();
                                            }
                                        });

                                    }
                                });

                            }
                        }).start();


                    } else {
                        CommonUtills.dismissProgressDialog();
                        CommonUtills.showAlert(getActivity(), getString(R.string.str_nogroup), Constants.NO_ACTION);

                        mrvChatHistoryUser.setVisibility(View.GONE);

                    }
                }

                @Override
                public void onError(QBResponseException e) {
                    CommonUtills.dismissProgressDialog();
                }
            });
        } else {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_groupload), false);

            QBRestChatService.getChatDialogs(qbDialogType, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                @Override
                public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {

                    CommonUtills.dismissProgressDialog();


                    if (qbChatDialogs != null && qbChatDialogs.size() > 0) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ChatUserHistoryModel model;
                                for (QBChatDialog qbChatDialog : qbChatDialogs) {

                                    // join chat dialog to receive the incoming messages.
                                    ChatProvider.getInstance().join(qbChatDialog, new QBEntityCallback<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid, Bundle bundle) {
                                        }

                                        @Override
                                        public void onError(QBResponseException e) {

                                        }
                                    });

                                    model = new ChatUserHistoryModel();


                                    model.setType(QBDialogType.GROUP);
                                    model.setChatDialog(qbChatDialog);
                                    model.setUnread_messagecount(qbChatDialog.getUnreadMessageCount());
                                    model.setChatuser_status(qbChatDialog.getLastMessage());
                                    model.setLastMessageDateSentPrivateChat(qbChatDialog.getLastMessageDateSent());
                                    mUserHistoryGroup.add(model);
                                    MissUGram.getApp(getActivity()).getListOfGroupDialogIds().add(qbChatDialog.getDialogId());

                                }
                                getActivity().runOnUiThread(() -> {
                                    setList(mUserHistoryGroup);
                                    MissUGram.getApp(getActivity()).setmChatUserHistoryModelsForGroup(mUserHistoryGroup);

                                });
                            }
                        }).start();


                    } else {
                        CommonUtills.showAlert(getActivity(), getString(R.string.str_messgae_nogroup), Constants.NO_ACTION);
                        mrvChatHistoryUser.setVisibility(View.GONE);

                    }


                }

                @Override
                public void onError(QBResponseException e) {
                    CommonUtills.dismissProgressDialog();
                }
            });


        }
    }

    private void setList(ArrayList<ChatUserHistoryModel> historyModels) {
        chatUserListingAdapter = new ChatUserListingAdapter(getActivity(), historyModels);
        mrvChatHistoryUser.setAdapter(chatUserListingAdapter);
        chatUserListingAdapter.setOnRecyclerItemListener(onRecyclerItemClickListener);
    }

    public void getRecentGroup() {
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.sortAsc("last_message_date_sent");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                QBRestChatService.getChatDialogs(QBDialogType.GROUP, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {


                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ChatUserHistoryModel model;

                                for (QBChatDialog qbChatDialog : qbChatDialogs) {
                                    model = new ChatUserHistoryModel();


                                    model.setType(QBDialogType.GROUP);
                                    model.setChatDialog(qbChatDialog);
                                    mSingleChatUserHistory.add(model);
                                }
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        CommonUtills.dismissProgressDialog();
                                        setList(mSingleChatUserHistory);
                                    }
                                });

                            }
                        }).start();


                    }

                    @Override
                    public void onError(QBResponseException e) {
                        CommonUtills.dismissProgressDialog();
                    }
                });
            }
        });
    }

    private void callContactsWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        Call<ContactResponseVo> contactResponseVoCall = apiInterface.getMatchedContact(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Constants.PAGE_COUNT, currentPageContacts, MissUGram.getApp(getActivity()).getUserModel().getId());
        contactResponseVoCall.enqueue(new Callback<ContactResponseVo>() {
            @Override
            public void onResponse(Call<ContactResponseVo> call, Response<ContactResponseVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {

                    ContactResponseVo contactResponseVo = response.body();
                    if (contactResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        if (contactResponseVo.getFriends_list() != null && contactResponseVo.getFriends_list().size() < Constants.PAGE_COUNT) {
                            isPullingMoreResult = true;
                        } else {
                            isPullingMoreResult = false;
                        }
                        setData(contactResponseVo.getFriends_list());
                    } else {
                        CommonUtills.showAlert(getActivity(), contactResponseVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    private void setData(ArrayList<UserModel> userModelArrayList) {
        commonVoArrayList = userModelArrayList;

        if (friendListAdapter == null) {
            friendListAdapter = new FriendListAdapter(getActivity(), commonVoArrayList, Constants.CHAT);
            mrvFriendlistchat.setAdapter(friendListAdapter);
            friendListAdapter.setOnRecyclerItemListenerForId(id -> {
                if (id != 0 || id != -1) {
                    CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), true);
                    QBUsers.getUser(id).performAsync(new QBEntityCallback<QBUser>() {
                        @Override
                        public void onSuccess(QBUser user, Bundle bundle) {
                            CommonUtills.dismissProgressDialog();
                            FragmentController.addChatFragment(getActivity(), id, Constants.FOR_PRIVATE_CHAT, "", user.getFullName() == null ? user.getLogin() : user.getFullName(), user.getFileId() == null ? String.valueOf("-1") : String.valueOf(user.getFileId()));
                        }

                        @Override
                        public void onError(QBResponseException e) {
                            CommonUtills.dismissProgressDialog();
                        }
                    });

                }
            });
        } else {
            commonVoArrayList.addAll(userModelArrayList);
            friendListAdapter.notifyDataSetChanged();
            friendListAdapter.updateFriendsVo(userModelArrayList);
        }

    }


    private void addListener()

    {
        mIvClear.setOnClickListener(this);
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String textToSearch = mEtSearch.getText().toString().toLowerCase(Locale.getDefault());

                if (textToSearch != null) {
                    if (mrvFriendlistchat.getVisibility() == View.VISIBLE) {
                        if (friendListAdapter != null) {
                            friendListAdapter.filter(textToSearch);

                            if (friendListAdapter != null) {
                                if (friendListAdapter.getItemCount() == 0) {
                                    mTvNoRecords_chat.setVisibility(View.VISIBLE);
                                } else {
                                    mTvNoRecords_chat.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else if (mrvChatHistoryUser.getVisibility() == View.VISIBLE) {
                        if (chatUserListingAdapter != null) {
                            chatUserListingAdapter.filter(textToSearch, mvw_chat.getVisibility() == View.VISIBLE ? QBDialogType.PRIVATE : QBDialogType.GROUP);

                            if (chatUserListingAdapter.getItemCount() == 0) {
                                mTvNoRecords_chat.setVisibility(View.VISIBLE);
                            } else {
                                mTvNoRecords_chat.setVisibility(View.GONE);
                            }
                        }
                    }


                }

            }
        });
    }


    private void setListeners() {
        mlnr_chat.setOnClickListener(this);
        mlnr_group_chat.setOnClickListener(this);
        mlnr_contacts.setOnClickListener(this);

    }


    private void initViews(View root) {
        mlnr_chat = (LinearLayout) root.findViewById(R.id.lnr_chat);
        mlnr_group_chat = (LinearLayout) root.findViewById(R.id.lnr_group_chat);
        mlnr_contacts = (LinearLayout) root.findViewById(R.id.lnr_contacts);

        mvw_chat = (View) root.findViewById(R.id.vw_chat);
        mvw_groupchat = (View) root.findViewById(R.id.vw_groupchat);
        mvw_contacts = (View) root.findViewById(R.id.vw_contacts);

        mEtSearch = (EditText) root.findViewById(R.id.mfl_etSearch);
        mIvClear = (ImageView) root.findViewById(R.id.mfl_ivClear);

        mTvNoRecords_chat = (TextView) root.findViewById(R.id.mfl_tv_no_records_chat);
        mrvChatHistoryUser = (RecyclerView) root.findViewById(R.id.recycler_view_chat_contacts);
        mrvChatHistoryUser.addItemDecoration(new VerticalDividerItemDecoration((int) getActivity().getResources().getDimension(R.dimen.divider_space), false));
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mrvChatHistoryUser.setLayoutManager(linearLayoutManager);

        mrvFriendlistchat = (RecyclerView) root.findViewById(R.id.recycler_view_friendlist_chat);
        mrvFriendlistchat.addItemDecoration(new VerticalDividerItemDecoration((int) getActivity().getResources().getDimension(R.dimen.divider_space), false));
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mrvFriendlistchat.setLayoutManager(linearLayoutManager);

        mrvFriendlistchat.setVisibility(View.GONE);
        mrvChatHistoryUser.setVisibility(View.VISIBLE);

        mUserHistoryGroup = new ArrayList<ChatUserHistoryModel>();

        if (!QBChatService.getInstance().isLoggedIn()) {
            QBUser user = new QBUser(CommonUtills.getPrefCurrentChatUname(getActivity()), CommonUtills.getPrefCurrentChatUname(getActivity()));
            user.setId(CommonUtills.getPrefChatId(getActivity()));
            user.setPassword(CommonUtills.getPrefCurrentChatUname(getActivity()));
            new Thread(() -> {
                try {
                    QBChatService.getInstance().login(user);
                    qbRoster = QBChatService.getInstance().getRoster();
                } catch (XMPPException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SmackException e) {
                    e.printStackTrace();
                }
            }).start();

        }


    }

    public void initUI() {
        ((TestMainActivity) getActivity()).updateToolbar();
    }

    private void closeKeyboard() {
        try {
            CommonUtility.hideKeyboard(getContext(), mEtSearch);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lnr_chat:
                //FIRST TAB CHAT
                closeKeyboard();
                ((TestMainActivity) getActivity()).setIsFromGroupChat(-1);
                mEtSearch.getText().clear();
                mTvNoRecords_chat.setVisibility(View.GONE);

                mvw_chat.setVisibility(View.VISIBLE);
                mvw_groupchat.setVisibility(View.GONE);
                mvw_contacts.setVisibility(View.GONE);

                mrvFriendlistchat.setVisibility(View.GONE);
                mrvChatHistoryUser.setVisibility(View.VISIBLE);

                mEtSearch.getText().clear();
                initUI();

                mrvChatHistoryUser.setVisibility(View.VISIBLE);

                if (mSingleChatUserHistory != null && mSingleChatUserHistory.size() > 0) {

                    setList(mSingleChatUserHistory);
                } else {
                    // if do not have data loading with empty list. bea
                    ArrayList<ChatUserHistoryModel> mTempuserHistoryModels = new ArrayList<>();
                    setList(mTempuserHistoryModels);
                }


                break;
            case R.id.lnr_group_chat:
                //SECOND TAB GROUP CHAT
                mEtSearch.getText().clear();

                closeKeyboard();
                mTvNoRecords_chat.setVisibility(View.GONE);

                mvw_chat.setVisibility(View.GONE);
                mvw_groupchat.setVisibility(View.VISIBLE);
                mvw_contacts.setVisibility(View.GONE);

                mrvFriendlistchat.setVisibility(View.GONE);
                mrvChatHistoryUser.setVisibility(View.VISIBLE);
                ((TestMainActivity) getActivity()).setIsFromGroupChat(Constants.FROM_GROUP_CHAT_TAB);
                initUI();

                if (!isGroupClicked) {
                    isGroupClicked = true;

                    getDialogForChatHistory(QBDialogType.GROUP);

                } else {
                    if (mUserHistoryGroup != null && mUserHistoryGroup.size() > 0) {
                        setList(mUserHistoryGroup);
                    } else {
                        ArrayList<ChatUserHistoryModel> mTempuserHistoryModels = new ArrayList<>();
                        setList(mTempuserHistoryModels);
                        // if group does not have any names then hide the recylerview.
                    }
                }


                break;
            case R.id.lnr_contacts:
                //THIRD TAB CONTACT
                closeKeyboard();
                ((TestMainActivity) getActivity()).setIsFromGroupChat(-1);
                mEtSearch.getText().clear();
                mTvNoRecords_chat.setVisibility(View.GONE);

                mvw_chat.setVisibility(View.GONE);
                mvw_groupchat.setVisibility(View.GONE);
                mvw_contacts.setVisibility(View.VISIBLE);

                mrvFriendlistchat.setVisibility(View.VISIBLE);
                mrvChatHistoryUser.setVisibility(View.GONE);

                if (!isContactsClicked) {
                    isContactsClicked = true;
                    callContactsWS();
                }
                initUI();
                break;
            case R.id.mfl_ivClear:
                mEtSearch.getText().clear();
                try {
                    CommonUtility.hideKeyboard(getActivity(), mEtSearch);
                } catch (Throwable throwable) {
                    CustomLogHandler.printErrorlog(throwable);
                }
                break;
        }
    }
}
