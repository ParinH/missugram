package wg.com.missugram.fragments;

import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;


import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.adapter.MemorialAdapter;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.MemorialResponseVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;
import wg.com.missugram.utils.PrefUtils;
import wg.com.missugram.utils.VerticalDividerItemDecoration;


public class MemorialFragment extends BaseFragment implements View.OnClickListener {

    Call<MemorialResponseVo> call;
    private static final int REQUEST_CODE = 1;
    private RecyclerView mRecycler_view_private, mRecycler_view_public;
    private TextView mTvInvite, mTvPublic;
    private TextView mTvCreateMemotial;
    private RelativeLayout mRlBottom;
    private ProgressBar pb;
    private static final String KEY_FROM_WHERE = "fromWhere";
    private int fromWhere;
    private int currentPage = 1;
    protected int pastVisiblesItemsPrivate, visibleItemCountPrivate, totalItemCountPrivate;
    protected int pastVisiblesItemsPublic, visibleItemCountPublic, totalItemCountPublic;


    private LinearLayoutManager linearLayoutManagerPrivate, linearLayoutManagerPublic;
    private ArrayList<MemorialVo> alMemorialVosPrivate;
    private ArrayList<MemorialVo> alMemorialVosPublic;
    private MemorialAdapter memorialAdapterPublic;
    private MemorialAdapter memorialAdapterPrivate;
    protected boolean isPullingMoreResultsPrivate, isPullingMoreResultsPublic;
    private String memoriamType = Constants.MEMORIUM_TYPE_PUBLIC;//private
    private String memorialUrl;
    private EditText mEtSearch;
    final int DRAWABLE_RIGHT = 2;
    private TextView mTvNoRecords;
    private LinearLayout mLlSearch;
    private ImageView mIvClear;
    int user_id;
    String tkn;
    UserModel user;

    public static MemorialFragment newInstance(int fromWhere) {
        MemorialFragment settingsFragment = new MemorialFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        settingsFragment.setArguments(bundle);
        return settingsFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @NonNull Bundle savedInstanceState) {
        getActivity().setTheme(R.style.AppTheme_NoActionBar);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        View root = inflater.inflate(R.layout.memorial_fragment_layout, container, false);
        Log.e("a",Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()));
        call = apiInterface.getMemorials(memorialUrl, Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Constants.PAGE_COUNT, currentPage, memoriamType, user_id);

        // makeWsCall();
        initViews(root);
        user_id=MissUGram.getApp(getActivity()).getUserModel().getId();
        tkn=MissUGram.getApp(getActivity()).getUserModel().getToken();

//        user = PrefUtils.getUser(getContext(), Constants.PREF_USER);
//        if (user != null) {
//            user_id = user.getId();
//            tkn = user.getToken();
//            //  StaticConfig.UID = data.getFcmId();
//
//        }
        Log.e("idd", "" + user_id);
        Log.e("tkn", "" + tkn);
        initUI();
        addScrollListener();
        setListeners();
        extractArguments();
        onLoad();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        initViews(view);

       /* user = PrefUtils.getUser(getContext(), Constants.PREF_USER);
        if (user != null) {
            user_id = user.getId();
            tkn = user.getToken();
            //  StaticConfig.UID = data.getFcmId();
            Log.e("idd", "" + user_id);
            Log.e("tkn", "" + tkn);
        }
*/
//        Thread t = new Thread() {
//            public void run() {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        initUI();
//                        addScrollListener();
//                        setListeners();
//                        extractArguments();
//                        onLoad();
////                        user_id = MissUGram.getApp(getActivity()).getUserModel().getId();
//                    }
//                });
//                // makeWsCall();
//            }
//        };
//        t.start();

//        initViews(view);
//        setListeners();
//        addScrollListener();
//        extractArguments();
//        initUI();
//
//        // makeWsCall();
//        onLoad();
//        user = PrefUtils.getUser(getContext(), Constants.PREF_USER);
//        if (user != null) {
//
//            user_id = user.getId();
//            //  StaticConfig.UID = data.getFcmId();
//            Log.e("key", "" + user_id);
//        }
//        user_id = MissUGram.getApp(getActivity()).getUserModel().getId();
//        Log.e("id", String.valueOf(user_id));
    }

    private void onLoad() {


        mEtSearch.setText("");
        mTvPublic.setBackgroundResource(0);
        mTvInvite.setBackgroundResource(R.drawable.left_selected_view);
        mTvPublic.setTextColor(getResources().getColor(R.color.colorPrimary));
        mTvInvite.setTextColor(Color.WHITE);
        memoriamType = Constants.MEMORIUM_TYPE_PUBLIC;
        mRecycler_view_public.setVisibility(View.VISIBLE);
        mRecycler_view_private.setVisibility(View.GONE);
        currentPage = 1;
        if (alMemorialVosPublic == null) {

            makeWsCall();
        }
        if (alMemorialVosPublic != null && alMemorialVosPublic.size() == 0) {
            mTvNoRecords.setVisibility(View.VISIBLE);
        } else {
            mTvNoRecords.setVisibility(View.GONE);
        }
    }

    private void makeWsCall() {

        CommonUtills.showProgressDialog(getContext(), getString(R.string.str_pleasewait), false);
        call.cancel();
        call = apiInterface.getMemorials(memorialUrl, Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Constants.PAGE_COUNT, currentPage, memoriamType, user_id);

        call.enqueue(new Callback<MemorialResponseVo>() {
            @Override
            public void onResponse(Call<MemorialResponseVo> call, Response<MemorialResponseVo> response) {
                if (response != null && response.body() != null) {
                    CommonUtills.dismissProgressDialog();

                    MemorialResponseVo memorialResponseVo = response.body();
                    if (memorialResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {

                        if (memorialResponseVo.getMemorialinfo().size() == 0) {
                            mTvNoRecords.setVisibility(View.VISIBLE);
                            pb.setVisibility(View.GONE);

                        } else {
                            mTvNoRecords.setVisibility(View.GONE);
                            pb.setVisibility(View.GONE);
                        }
                        if (memorialResponseVo.getMemorialinfo().size() < Constants.PAGE_COUNT) {
                            isPullingMoreResultsPublic = true;
                            isPullingMoreResultsPrivate = true;
                        } else {
                            isPullingMoreResultsPublic = false;
                            isPullingMoreResultsPrivate = false;
                        }


                        setListData(memorialResponseVo.getMemorialinfo());

                    } else {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<MemorialResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //  setListeners();
                //  addScrollListener();
                //extractArguments();
                initUI();

                // makeWsCall();
                onLoad();
            }
        });


//        setListeners();
//        addScrollListener();
//        extractArguments();
//        initUI();
//
//        // makeWsCall();
//        onLoad();
    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromWhere = bundle.getInt(KEY_FROM_WHERE, Constants.FROM_MEMORIAL_LISTING);
            ((MainActivity) getActivity()).setCurrentFriendListVal(fromWhere);

        }
    }

    private void setListeners() {
        mTvInvite.setOnClickListener(this);
        mTvPublic.setOnClickListener(this);
        mTvCreateMemotial.setOnClickListener(this);
        mIvClear.setOnClickListener(this);
        mEtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                String text = mEtSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (memoriamType.equals(Constants.MEMORIUM_TYPE_PRIVATE) || memoriamType.equals(Constants.MEMORIUM_TYPE_INVITE)) {

                    if (memorialAdapterPrivate != null) {
                        memorialAdapterPrivate.filter(text);


                        if (memorialAdapterPrivate.getItemCount() == 0) {
                            mTvNoRecords.setVisibility(View.VISIBLE);
                        } else {
                            mTvNoRecords.setVisibility(View.GONE);
                        }
                        memorialAdapterPrivate.notifyDataSetChanged();
                    }


                } else {

                    if (memorialAdapterPublic != null) {
                        memorialAdapterPublic.filter(text);
                        if (memorialAdapterPublic.getItemCount() == 0) {
                            mTvNoRecords.setVisibility(View.VISIBLE);
                        } else {
                            mTvNoRecords.setVisibility(View.GONE);
                        }
                        memorialAdapterPublic.notifyDataSetChanged();
                    }

                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {


            }
        });

    }

    private void setListData(ArrayList<MemorialVo> pAlMemorialVos) {

        if (memoriamType.equals(Constants.MEMORIUM_TYPE_PRIVATE) || memoriamType.equals(Constants.MEMORIUM_TYPE_INVITE)) {
            if (memorialAdapterPrivate == null) {
                alMemorialVosPrivate = pAlMemorialVos;
                mRecycler_view_public.setVisibility(View.GONE);
                memorialAdapterPrivate = new MemorialAdapter(getActivity(), alMemorialVosPrivate);
                mRecycler_view_private.setAdapter(memorialAdapterPrivate);
                mRecycler_view_private.addItemDecoration(new VerticalDividerItemDecoration((int) getActivity().getResources().getDimension(R.dimen.divider_space), false));
                memorialAdapterPrivate.setOnRecyclerItemListener(new MemorialAdapter.OnRecyclerItemClickListener() {
                    @Override
                    public void onViewItemClicked(MemorialVo memorialVo) {
                        //since from the web e are not getting the categoryId so adding statically based on the memorial requested provate or public. That will be set in edit memoriuam as a value.

                        memorialVo.setCategory_id(!memoriamType.equalsIgnoreCase(Constants.MEMORIUM_TYPE_INVITE) ? memoriamType : "");

                        FragmentController.addDescriptionFragment(getActivity(), memorialVo, fromWhere);
                    }
                });
            } else {
                alMemorialVosPrivate.addAll(pAlMemorialVos);
                memorialAdapterPrivate.notifyDataSetChanged();
                memorialAdapterPrivate.updateMemorialVo(pAlMemorialVos);
            }
        } else {
            if (memorialAdapterPublic == null) {
                alMemorialVosPublic = pAlMemorialVos;
                mRecycler_view_public.setVisibility(View.VISIBLE);
                memorialAdapterPublic = new MemorialAdapter(getActivity(), alMemorialVosPublic);
                mRecycler_view_public.setAdapter(memorialAdapterPublic);
                mRecycler_view_public.addItemDecoration(new VerticalDividerItemDecoration((int) getActivity().getResources().getDimension(R.dimen.divider_space), false));
                memorialAdapterPublic.setOnRecyclerItemListener(new MemorialAdapter.OnRecyclerItemClickListener() {
                    @Override
                    public void onViewItemClicked(MemorialVo memorialVo) {
                        memorialVo.setCategory_id(!memoriamType.equalsIgnoreCase(Constants.MEMORIUM_TYPE_INVITE) ? memoriamType : "");
                        FragmentController.addDescriptionFragment(getActivity(), memorialVo, fromWhere);
                    }
                });
            } else {
                alMemorialVosPublic.addAll(pAlMemorialVos);
                memorialAdapterPublic.notifyDataSetChanged();
                memorialAdapterPublic.updateMemorialVo(pAlMemorialVos);

            }

        }
    }

    private void initUI() {
        ((MainActivity) getActivity()).updateToolbar();
        mRlBottom.setVisibility(fromWhere == Constants.FROM_MY_MEMORIAL ? View.VISIBLE : View.GONE);
        if (fromWhere == Constants.FROM_MY_MEMORIAL) {
            mTvInvite.setText(getString(R.string.str_public));
            mTvPublic.setText("Private");
            memorialUrl = Constants.URL_MY_MEMORIAM;
        } else {
            memorialUrl = Constants
                    .URL_MEMORIAM_PAGE;
        }

    }


    private void initViews(View root) {

        mTvInvite = (TextView) root.findViewById(R.id.mfl_tv_public);
        pb = (ProgressBar) root.findViewById(R.id.pb);
        mTvPublic = (TextView) root.findViewById(R.id.mfl_tv_invite);
        mTvCreateMemotial = (TextView) root.findViewById(R.id.mfl_tv_create_memoriam);
        mRlBottom = (RelativeLayout) root.findViewById(R.id.mfl_rl_bottom);
        mEtSearch = (EditText) root.findViewById(R.id.mfl_etSearch);
        mLlSearch = (LinearLayout) root.findViewById(R.id.llSearch);
        mIvClear = (ImageView) root.findViewById(R.id.mfl_ivClear);

        mTvNoRecords = (TextView) root.findViewById(R.id.mfl_tv_no_records);
        mRecycler_view_public = (RecyclerView) root.findViewById(R.id.recycler_view_public);
        linearLayoutManagerPublic = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecycler_view_public.setLayoutManager(linearLayoutManagerPublic);
        mRecycler_view_public.setNestedScrollingEnabled(true);

        mRecycler_view_private = (RecyclerView) root.findViewById(R.id.recycler_view_private);
        linearLayoutManagerPrivate = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecycler_view_private.setLayoutManager(linearLayoutManagerPrivate);
        mRecycler_view_public.setNestedScrollingEnabled(true);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mfl_tv_public:
                mEtSearch.setText("");
                mTvPublic.setBackgroundResource(0);
                mTvInvite.setBackgroundResource(R.drawable.left_selected_view);
                mTvPublic.setTextColor(getResources().getColor(R.color.colorPrimary));
                mTvInvite.setTextColor(Color.WHITE);
                memoriamType = Constants.MEMORIUM_TYPE_PUBLIC;
                mRecycler_view_public.setVisibility(View.VISIBLE);
                mRecycler_view_private.setVisibility(View.GONE);
                currentPage = 1;
                if (alMemorialVosPublic == null) {
                    makeWsCall();
                }
                if (alMemorialVosPublic != null && alMemorialVosPublic.size() == 0) {
                    mTvNoRecords.setVisibility(View.VISIBLE);
                } else {
                    mTvNoRecords.setVisibility(View.GONE);
                }
                break;
            case R.id.mfl_tv_invite:
                mEtSearch.setText("");
                mTvPublic.setBackgroundResource(R.drawable.right_selected_view);
                mTvPublic.setTextColor(Color.WHITE);
                mTvInvite.setTextColor(getResources().getColor(R.color.colorPrimary));
                mTvInvite.setBackgroundResource(0);
                mRecycler_view_private.setVisibility(View.VISIBLE);
                mRecycler_view_public.setVisibility(View.GONE);
                if (fromWhere == Constants.FROM_MY_MEMORIAL)
                    memoriamType = Constants.MEMORIUM_TYPE_PRIVATE;
                else
                    memoriamType = Constants.MEMORIUM_TYPE_INVITE;
                if (alMemorialVosPrivate == null) {
                    makeWsCall();
                }
                if (alMemorialVosPrivate != null && alMemorialVosPrivate.size() == 0) {
                    mTvNoRecords.setVisibility(View.VISIBLE);
                } else {
                    mTvNoRecords.setVisibility(View.GONE);
                }
                currentPage = 1;
                break;
            case R.id.mfl_tv_create_memoriam:
                FragmentController.addCreateMemoriumFragment(getActivity(), null, Constants.FROM_MY_MEMORIAL);

                break;
            case R.id.mfl_ivClear:
                mEtSearch.setText("");
                mLlSearch.setVisibility(View.GONE);
                try {
                    CommonUtility.hideKeyboard(getActivity(), mEtSearch);
                } catch (Throwable throwable) {
                    CustomLogHandler.printErrorlog(throwable);
                }

                break;
        }
    }


    private void addScrollListener() {


        mRecycler_view_private.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCountPrivate = linearLayoutManagerPrivate.getChildCount();
                    totalItemCountPrivate = linearLayoutManagerPrivate.getItemCount();
                    pastVisiblesItemsPrivate = linearLayoutManagerPrivate.findFirstVisibleItemPosition();

                    if (!isPullingMoreResultsPrivate) {
                        if ((visibleItemCountPrivate + pastVisiblesItemsPrivate) >= totalItemCountPrivate) {
                            isPullingMoreResultsPrivate = true;
                            currentPage++;

                            makeWsCall();
                        }
                    }
                }
            }
        });


        mRecycler_view_public.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCountPublic = linearLayoutManagerPublic.getChildCount();
                    totalItemCountPublic = linearLayoutManagerPublic.getItemCount();
                    pastVisiblesItemsPublic = linearLayoutManagerPublic.findFirstVisibleItemPosition();

                    if (!isPullingMoreResultsPublic) {
                        if ((visibleItemCountPublic + pastVisiblesItemsPublic) >= totalItemCountPublic) {
                            isPullingMoreResultsPublic = true;
                            currentPage++;
                            makeWsCall();
                        }
                    }
                }
            }
        });
    }

    public void showSearchBar() {
        mLlSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        CommonUtills.dismissProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        CommonUtills.dismissProgressDialog();

    }
}