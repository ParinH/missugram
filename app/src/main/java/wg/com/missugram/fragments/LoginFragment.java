package wg.com.missugram.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.wg.framework.util.CommonUtility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.LoginResponseVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;


public class LoginFragment extends BaseFragment implements View.OnClickListener {


    private ProgressBar mPbLogin;
    private EditText liEtEmail;
    private EditText liEtPassword;
    private TextView liTvForgot;
    private TextView liTvCreateAccount;
    private Button liBtSignUp;
    private Context mContext;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_fragment, container, false);

        Thread t = new Thread() {
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initViews(root);
                        setListeners();


                    }
                });
                // makeWsCall();

            }
        };
        t.start();




        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void setListeners() {
        liBtSignUp.setOnClickListener(this);
        liTvCreateAccount.setOnClickListener(this);
        liTvForgot.setOnClickListener(this);
        FragmentController.removeThisFragment(getActivity(), FragmentController.TAG_SPLASH_FRAG);
    }

    private void initViews(View root) {
        mPbLogin = (ProgressBar) root.findViewById(R.id.loginPb);
        liEtEmail = (EditText) root.findViewById(R.id.li_et_email);
        liEtPassword = (EditText) root.findViewById(R.id.li_et_password);
        liTvForgot = (TextView) root.findViewById(R.id.li_tv_forgot);
        liTvCreateAccount = (TextView) root.findViewById(R.id.li_tv_createaccount);
        liBtSignUp = (Button) root.findViewById(R.id.li_bt_signup);
        mPbLogin.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);


    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private void closekeyboard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), liEtEmail);
            CommonUtility.hideKeyboard(getActivity(), liEtPassword);

        } catch (Throwable throwable) {

        }
    }

    private void validateUserData() {
        if (CommonUtills.checkLength(liEtEmail.getText().toString().trim()) <= 0 || liEtEmail.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_email), Constants.NO_ACTION);
        } else if (!CommonUtills.isValidEmail(liEtEmail.getText().toString().trim())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_invalidemail), Constants.NO_ACTION);

        } else if (CommonUtills.checkLength(liEtPassword.getText().toString().trim()) <= 0 || liEtPassword.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_password), Constants.NO_ACTION);
        } else {

            if (CommonUtills.isNetworkAvailable(getActivity())) {

                callLoginUserWS();
            } else {
                CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.NO_ACTION);
            }
        }
    }

    private void checkTokenStored() {

        if (CommonUtills.getPrefFirebaseDeviceToken(getActivity()).equals("token")) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            CommonUtills.setPrefFirebaseDeviceToken(getActivity(), refreshedToken);
        }

    }

    private void callLoginUserWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        UserModel userLoginRequest = new UserModel();
        userLoginRequest.setEmail(liEtEmail.getText().toString().trim());
        userLoginRequest.setPassword(liEtPassword.getText().toString().trim());
        userLoginRequest.setDevice_type("2");
        checkTokenStored();
        userLoginRequest.setDevice_token(CommonUtills.getPrefFirebaseDeviceToken(getActivity()));

        final Call<LoginResponseVo> loginResponse = apiInterface.loginUser(userLoginRequest);
        loginResponse.enqueue(new Callback<LoginResponseVo>() {
            @Override
            public void onResponse(Call<LoginResponseVo> call, Response<LoginResponseVo> response) {


                if (response != null && response.body() != null) {

                    LoginResponseVo userModel = response.body();
                    if (userModel.getsStatus() == Constants.STATUS_SUCCESS) {
                        MissUGram.getApp(mContext).setUserModel(userModel.getUserInfo());
                        TokenKeeper.getInstance().storeTokens(getActivity(), userModel.getUserInfo().getToken(), 3600);

                        CommonUtills.setPrefFirstName(mContext, userModel.getUserInfo().getFirst_name());
                        CommonUtills.setPrefLastName(mContext, userModel.getUserInfo().getLast_name());
                        CommonUtills.setPrefUserAvatar(mContext, userModel.getUserInfo().getAvatar());
                        CommonUtills.setPrefUserPassword(mContext, liEtPassword.getText().toString().trim());
                        CommonUtills.setPrefUserEmail(mContext, userModel.getUserInfo().getEmail());
                        CommonUtills.setPrefToken(mContext, userModel.getUserInfo().getToken());
                        CommonUtills.setPrefIsNotificationEnabled(mContext, userModel.getUserInfo().getPush_flag().equals("1") ? true : false);
                        CommonUtills.setPrefId(mContext, userModel.getUserInfo().getId());


                        if (mContext != null && !((Activity) mContext).isFinishing()) {
                            QBUser qbRegisteredUser = new QBUser(userModel.getUserInfo().getEmail(), userModel.getUserInfo().getEmail());

                            if (CommonUtills.getPrefChatId(mContext) != 0) {
                                CommonUtills.dismissProgressDialog();
                                // will work for a user who came from signup to this screen.
                                //CommonUtills.showAlert(mContext, userModel.getsMessage(), Constants.ACTION_MAIN);
                                closeActivity();

                            }

                            ChatProvider.getInstance().signInChatUser(qbRegisteredUser, CommonUtills.getPrefUserEmail(LoginFragment.this.getActivity()), new QBEntityCallback<QBUser>() {
                                @Override
                                public void onSuccess(QBUser qbUser, Bundle bundle) {
                                    // if already do not have the chat credential store again and jump to the main screen.
                                    if (CommonUtills.getPrefChatId(mContext) == 0) {
                                        CommonUtills.setPrefCurrentChatUname(mContext, userModel.getUserInfo().getEmail());
                                        CommonUtills.setPrefChatId(mContext, qbUser.getId());
                                        CommonUtills.dismissProgressDialog();
                                        // CommonUtills.showAlert(mContext, userModel.getsMessage(), Constants.ACTION_MAIN);

                                        closeActivity();
                                    }
                                    clearAllText();
                                }

                                @Override
                                public void onError(QBResponseException e) {
                                    CommonUtills.dismissProgressDialog();
                                    CommonUtills.showAlert(mContext, getString(R.string.str_something_went_wrong), Constants.NO_ACTION);
                                }
                            });


                            if (CommonUtills.getPrefChatId(mContext) != 0) {
                                CommonUtills.dismissProgressDialog();
                                // will work for a user who came from signup to this screen.
                                //CommonUtills.showAlert(mContext, userModel.getsMessage(), Constants.ACTION_MAIN);
                                closeActivity();

                            }
                        }
                    } else {
                        //temp text

                        if (mContext != null && !((Activity) mContext).isFinishing()) {
                            CommonUtills.dismissProgressDialog();
                            CommonUtills.showAlert(mContext, userModel.getsMessage(), Constants.NO_ACTION);
                        }
                    }
                } else {
                    CommonUtills.dismissProgressDialog();
                    //temp text
                    if (mContext != null && !((Activity) mContext).isFinishing()) {
                        CommonUtills.showAlert(mContext, getString(R.string.str_invalid_credentials), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CommonUtills.showAlert(mContext, getString(R.string.str_something_went_wrong), Constants.NO_ACTION);
            }
        });

    }

    private void clearAllText() {
        liEtEmail.setText("");
        liEtPassword.setText("");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.li_bt_signup:
                closekeyboard();
                validateUserData();

                break;
            case R.id.li_tv_forgot:
                clearAllText();
                FragmentController.addForgotPasswordFragment(getActivity());
                break;
            case R.id.li_tv_createaccount:
                clearAllText();
                FragmentController.addSignUpFragment(getActivity(), Constants.FROM_OTHER);
                break;
        }
    }

    private void closeActivity() {
        getActivity().startActivity(new Intent(getActivity(), TestMainActivity.class));
        ((Activity) getActivity()).finish();
    }
    
}