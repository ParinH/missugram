package wg.com.missugram.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.LoginResponseVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.ApiClient;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class SignUpFragment extends BaseFragment implements View.OnClickListener {
    private static final String KEY_IS_FROM_SETTINGS = "isfromSettings";
    QBUser qbnNewUser;
    private EditText siEtFirstName;
    private EditText siEtLastName;
    private EditText siEtEmail;
    private EditText siEtPhone;
    private EditText siEtPassword;
    private EditText siEtConfirmPassword;
    private Button siBtSignUp;
    private CircleImageView siIvUserImage;
    private TextView siTvAlready;
    private SpannableString s_alreadyknow;
    private int REQUEST_CAMERA = 100;
    private int SELECT_FILE = 2;
    private int isFromSettings;
    private RelativeLayout siRlMain;
    private TextView siTvWelcome;
    private TextView siTvVersion;
    private File file;
    UserModel getUserModel;
    private static final int PERMISSIONS_REQUEST_CAMERA=100;
    private static final int PERMISSIONS_REQUEST_EXTERNAL_STORAGE=200;
    String imagePath="";
    Context context;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 101;
    public static SignUpFragment newInstance(int fromWhere) {
        SignUpFragment signUpFragment = new SignUpFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_IS_FROM_SETTINGS, fromWhere);
        signUpFragment.setArguments(bundle);
        return signUpFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getActivity();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.registration_fragment_layout, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initViews(root);
        extractArguments();
        initUI();

      /*  getcameraPermission();
        getStoragePermission();*/
        return root;
    }

    private void getStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(),Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void getcameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(),Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isFromSettings = bundle.getInt(KEY_IS_FROM_SETTINGS, Constants.FROM_SETTINGS);
        }

        if (isFromSettings == Constants.FROM_SETTINGS) {
            adjustForEditProfile();
            ((TestMainActivity) getActivity()).setIsFromSettings(Constants.FROM_SETTINGS);
        }
    }

    private void adjustForEditProfile() {
        int backgroundColor = ContextCompat.getColor(getActivity(), R.color.app_background);
        siRlMain.setBackgroundColor(backgroundColor);
        siTvWelcome.setVisibility(View.GONE);
        siBtSignUp.setText(R.string.str_update);
        siEtConfirmPassword.setVisibility(View.GONE);
        siEtPassword.setVisibility(View.GONE);

        siEtEmail.setFocusable(false);
        siEtEmail.setFocusableInTouchMode(false);
        siEtEmail.setClickable(false);

        siEtPhone.setFocusable(false);
        siEtPhone.setFocusableInTouchMode(false);
        siEtPhone.setClickable(false);

        siTvVersion.setVisibility(View.GONE);
        siTvAlready.setVisibility(View.GONE);
//==============
        if (MissUGram.getApp(getActivity()) != null) {
            siEtFirstName.setText(MissUGram.getApp(getActivity()).getUserModel().getFirst_name());
            siEtLastName.setText(MissUGram.getApp(getActivity()).getUserModel().getLast_name());
            siEtEmail.setText(MissUGram.getApp(getActivity()).getUserModel().getEmail());
            siEtPhone.setText(MissUGram.getApp(getActivity()).getUserModel().getPhone() == null ? "" : MissUGram.getApp(getActivity()).getUserModel().getPhone());
            //ImageLoader.getInstance().displayImage(MissUGram.getApp(getActivity()).getUserModel().getAvatar(), siIvUserImage, CommonUtills.getRoundImageOptions());



//                ImageLoader imageLoader = ImageLoader.getInstance();
//                imageLoader.displayImage(ApiClient.BASE_Image+MissUGram.getApp(getActivity()).getUserModel().getAvatar(), siIvUserImage);


//            ImageLoader.getInstance().displayImage(ApiClient.BASE_Image + MissUGram.getApp(getActivity()).getUserModel().getAvatar(), siIvUserImage, CommonUtills.getRoundImageOptions());

             Glide.with(this).load(MissUGram.getApp(getActivity()).getUserModel().getAvatar())
                     .placeholder(R.drawable.ic_missugram_logo)
                     .into(siIvUserImage);

        }
    }

    private void initViews(View root) {
        siEtFirstName = (EditText) root.findViewById(R.id.si_et_firstname);
        siEtLastName = (EditText) root.findViewById(R.id.si_et_lastname);
        siEtEmail = (EditText) root.findViewById(R.id.si_et_email);
        siEtPhone = (EditText) root.findViewById(R.id.si_et_phone);
        siEtPassword = (EditText) root.findViewById(R.id.si_et_password);
        siEtConfirmPassword = (EditText) root.findViewById(R.id.si_et_confirmpassword);
        siBtSignUp = (Button) root.findViewById(R.id.si_bt_signup);
        siTvAlready = (TextView) root.findViewById(R.id.si_Tv_already);
        siIvUserImage = (CircleImageView) root.findViewById(R.id.si_et_userimage);
        siRlMain = (RelativeLayout) root.findViewById(R.id.si_rl_main);
        siTvWelcome = (TextView) root.findViewById(R.id.si_tv_welcome);
        siTvVersion = (TextView) root.findViewById(R.id.si_tv_version);

        siBtSignUp.setOnClickListener(this);
        siIvUserImage.setOnClickListener(this);

        s_alreadyknow = new SpannableString(getString(R.string.str_already_register_sign_in));


        if (CommonUtills.getVersionNumber(getActivity()) != null) {
            siTvVersion.setText(getActivity().getString(R.string.str_missugram_version_1_0) + " " + CommonUtills.getVersionNumber(getActivity()));
        } else {
            siTvVersion.setText(getActivity().getString(R.string.str_missugram_version_1_0) + " 1");

        }

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                FragmentController.removeFragment(getActivity(), FragmentController.TAG_SIGNUP_FRAGMENT);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        s_alreadyknow.setSpan(clickableSpan, 18, s_alreadyknow.toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s_alreadyknow.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 18, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s_alreadyknow.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(android.R.color.white)), 18, s_alreadyknow.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s_alreadyknow.setSpan(new UnderlineSpan(), 18, s_alreadyknow.length(), 0);
        siTvAlready.setMovementMethod(LinkMovementMethod.getInstance());
        siTvAlready.setText(s_alreadyknow);
    }

    private void initUI() {
        if (isFromSettings == 3) {
            ((TestMainActivity) getActivity()).updateToolbar();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.si_bt_signup:
                closeKeyboard();
                validateUserData();

                break;
            case R.id.si_et_userimage:
              /*  if (isFromSettings == Constants.FROM_SETTINGS) {
                  //  selectImage();


                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(getContext(), this);
                }*/

               /* CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getContext(), this);*/

                if(checkAndRequestPermissions(getActivity())){
                    chooseImage(context);
                }

                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(),
                            "FlagUp Requires Access to Camara.", Toast.LENGTH_SHORT)
                            .show();
                } else if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(),
                            "FlagUp Requires Access to Your Storage.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    chooseImage(context);
                }
                break;
        }
    }

    private void callUpdateQuickBloxId(final String QuickBloxId) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("userid", String.valueOf(MissUGram.getApp(getActivity()).getUserModel().getId()));
        params.put("quickblox_account_id", QuickBloxId);
        final Call<CommonVo> callIdUpdate = apiInterface.updateQuickBloxAccount(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), params);
        callIdUpdate.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.setPrefChatId(getActivity(), Integer.valueOf(QuickBloxId));

                        if (file != null) {
                            ChatProvider.getInstance().uploadUserProfileImage(file, qbnNewUser);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });

    }

    private void callRegisterUserWSMultiPart() {
        UserModel userRegisterRequest = new UserModel();
        userRegisterRequest.setFirst_name(siEtFirstName.getText().toString().trim());
        userRegisterRequest.setLast_name(siEtLastName.getText().toString().trim());
        userRegisterRequest.setEmail(siEtEmail.getText().toString().trim());
        userRegisterRequest.setPassword(siEtPassword.getText().toString().trim());
        userRegisterRequest.setPhone(siEtPhone.getText().toString().trim());

        Gson gson = new Gson();
        String convertedJson = gson.toJson(userRegisterRequest);

        RequestBody name = RequestBody.create(MediaType.parse("application/json"), convertedJson);

        final Call<LoginResponseVo> loginResponse;

        if (TextUtils.isEmpty(imagePath)) {
            // image not provided. from client feedback image will be set and edited from the settings module.
            loginResponse = apiInterface.registerUserWithoutProfile(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), name);

        } else {
           /* RequestBody imageBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), imageBody);
            loginResponse = apiInterface.registerUser(body, name);
*/
            MultipartBody.Part imageMB = null;

            if (!TextUtils.isEmpty(imagePath)) {
                RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(imagePath));
                imageMB = MultipartBody.Part.createFormData("avatar", new File(imagePath).getName(), imageBody);
            }
            loginResponse = apiInterface.registerUser(imageMB, name);

        }

        loginResponse.enqueue(new Callback<LoginResponseVo>() {
            @Override
            public void onResponse(Call<LoginResponseVo> call, Response<LoginResponseVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    LoginResponseVo loginResponseModel = response.body();
                    if (loginResponseModel.getsStatus() == Constants.STATUS_SUCCESS) {
                        MissUGram.getApp(getActivity()).setUserModel(loginResponseModel.getUserInfo());
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            MissUGram.getApp(getActivity()).setUserModel(loginResponseModel.getUserInfo());
                            TokenKeeper.getInstance().storeTokens(getActivity(), loginResponseModel.getUserInfo().getToken(), 3600);

                            String email = loginResponseModel.getUserInfo().getEmail().trim();
                            clearAllTexts();
                            CommonUtills.setPrefFirstName(getActivity(), loginResponseModel.getUserInfo().getFirst_name());
                            CommonUtills.setPrefLastName(getActivity(), loginResponseModel.getUserInfo().getLast_name());
                            CommonUtills.setPrefUserAvatar(getActivity(), loginResponseModel.getUserInfo().getAvatar());
                            CommonUtills.setPrefUserPassword(getActivity(), loginResponseModel.getUserInfo().getEmail().trim());
                         //   CommonUtills.setPrefUserEmail(getActivity(), loginResponseModel.getUserInfo().getEmail());
                            CommonUtills.setPrefToken(getActivity(), loginResponseModel.getUserInfo().getToken());
                            CommonUtills.setPrefIsNotificationEnabled(getActivity(), loginResponseModel.getUserInfo().getPush_flag().equals("1"));
                            CommonUtills.setPrefId(getActivity(), loginResponseModel.getUserInfo().getId());

                            //Using email as uname and also as a password. as per suggested by IOS.
                            qbnNewUser = new QBUser(email, email, email);
                            qbnNewUser.setFullName(loginResponseModel.getUserInfo().getFirst_name() + " " + loginResponseModel.getUserInfo().getLast_name());
                            ChatProvider.getInstance().registerUserForChat(qbnNewUser, email, new QBEntityCallback<QBUser>() {
                                @Override
                                public void onSuccess(QBUser qbUser, Bundle bundle) {
                                    CommonUtills.setPrefCurrentChatUname(getActivity(), loginResponseModel.getUserInfo().getEmail());
                                    CommonUtills.setPrefChatId(getActivity(), qbUser.getId());
                                    callUpdateQuickBloxId(String.valueOf(qbUser.getId()));
                                }

                                @Override
                                public void onError(QBResponseException e) {

                                }
                            });
                            FragmentController.removeFragment(getActivity(), FragmentController.TAG_SIGNUP_FRAGMENT);
                            // CommonUtills.showAlert(getActivity(), loginResponseModel.getsMessage(), Constants.ACTION_SIGN_IN);

                            // /server message has been changed based on the client feedback.
                            CommonUtills.showAlert(getActivity(), getString(R.string.str_register_email_link), Constants.ACTION_SIGN_IN);

                        }
                    } else {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            CommonUtills.showAlert(getActivity(), loginResponseModel.getsMessage(), Constants.NO_ACTION);
                        }
                    }
                } else {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        CommonUtills.showAlert(getActivity(), getString(R.string.str_something_went_wrong), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });

    }

    private void clearAllTexts() {
        siEtFirstName.setText("");
        siEtLastName.setText("");
        siEtEmail.setText("");
        siEtPassword.setText("");
        siEtConfirmPassword.setText("");
        siEtPhone.setText("");

    }

    private void callEditUserProfileWSWithMultiPart() {
        UserModel userEditProfileRequest = new UserModel();
        userEditProfileRequest.setFirst_name(siEtFirstName.getText().toString().trim());
        userEditProfileRequest.setLast_name(siEtLastName.getText().toString().trim());
        userEditProfileRequest.setPhone(siEtPhone.getText().toString().trim());
        userEditProfileRequest.setId(MissUGram.getApp(getActivity()).getUserModel().getId());
        userEditProfileRequest.setEmail(MissUGram.getApp(getActivity()).getUserModel().getEmail());
        userEditProfileRequest.setQuickblox_account_id(MissUGram.getApp(getActivity()).getUserModel().getQuickblox_account_id() == null ? "0" : MissUGram.getApp(getActivity()).getUserModel().getQuickblox_account_id());

        Gson gson = new Gson();
        String convertedJson = gson.toJson(userEditProfileRequest);

        RequestBody name = RequestBody.create(MediaType.parse("application/json"), convertedJson);


        final Call<LoginResponseVo> loginResponse;
        if (TextUtils.isEmpty(imagePath)) {
       // if (file == null) {
            loginResponse = apiInterface.editProfileWithOutImage(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), name);

        } else {
          /*  RequestBody imageBody = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), imageBody);

*/
            MultipartBody.Part imageMB = null;

            if (!TextUtils.isEmpty(imagePath)) {
                RequestBody imageBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(imagePath));
                imageMB = MultipartBody.Part.createFormData("avatar", new File(imagePath).getName(), imageBody);
            }



            loginResponse = apiInterface.editProfile(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), imageMB, name);
//            loginResponse = apiInterface.editProfile(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), body, name);



        }

        loginResponse.enqueue(new Callback<LoginResponseVo>() {
            @Override
            public void onResponse(Call<LoginResponseVo> call, Response<LoginResponseVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    LoginResponseVo loginResponseModel = response.body();
                    if (loginResponseModel.getsStatus() == Constants.STATUS_SUCCESS) {
                        loginResponseModel.getUserInfo().setToken(CommonUtills.getPrefToken(getActivity()));
                        MissUGram.getApp(getActivity()).setUserModel(loginResponseModel.getUserInfo());

                        ((TestMainActivity) getActivity()).setData();
                        CommonUtills.setPrefFirstName(getActivity(), loginResponseModel.getUserInfo().getFirst_name());
                        CommonUtills.setPrefLastName(getActivity(), loginResponseModel.getUserInfo().getLast_name());
                        CommonUtills.setPrefUserAvatar(getActivity(), loginResponseModel.getUserInfo().getAvatar());

                        if (getActivity() != null && !getActivity().isFinishing()) {
                            clearAllTexts();
                            FragmentController.removeThisFragment(getActivity(), FragmentController.TAG_SIGNUP_FRAGMENT);
                            CommonUtills.showAlert(getActivity(), loginResponseModel.getsMessage(), Constants.ACTION_SETTINGS);
                        }
                    } else {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            CommonUtills.showAlert(getActivity(), loginResponseModel.getsMessage(), Constants.NO_ACTION);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    private void closeKeyboard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), siEtFirstName);
            CommonUtility.hideKeyboard(getActivity(), siEtLastName);
            CommonUtility.hideKeyboard(getActivity(), siEtEmail);
            CommonUtility.hideKeyboard(getActivity(), siEtPhone);
            CommonUtility.hideKeyboard(getActivity(), siEtPassword);
            CommonUtility.hideKeyboard(getActivity(), siEtConfirmPassword);
        } catch (Throwable throwable) {

        }
    }

    private void validateUserData() {

        if (CommonUtills.checkLength(siEtFirstName.getText().toString().trim()) <= 0 || siEtFirstName.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_firstname), Constants.NO_ACTION);
        } else if (CommonUtills.checkLength(siEtLastName.getText().toString().trim()) <= 0 || siEtLastName.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_lastname), Constants.NO_ACTION);

        } else if (isFromSettings != Constants.FROM_SETTINGS && CommonUtills.checkLength(siEtEmail.getText().toString().trim()) <= 0 || siEtEmail.getText().toString().trim().isEmpty()) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_email), Constants.NO_ACTION);

        } else if (isFromSettings != Constants.FROM_SETTINGS && !CommonUtills.isValidEmail(siEtEmail.getText().toString().trim())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_invalidemail), Constants.NO_ACTION);

        } else if (isFromSettings != Constants.FROM_SETTINGS && CommonUtills.checkLength(siEtPhone.getText().toString().trim()) <= 0) {
            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_phone), Constants.NO_ACTION);

        } else if (isFromSettings != Constants.FROM_SETTINGS && (CommonUtills.checkLength(siEtPassword.getText().toString().trim()) <= 0 || siEtPassword.getText().toString().trim().isEmpty())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_password), Constants.NO_ACTION);
        } else if (isFromSettings != Constants.FROM_SETTINGS && CommonUtills.checkLength(siEtPassword.getText().toString().trim()) < 6) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_length_pwd), Constants.NO_ACTION);
        } else if (isFromSettings != Constants.FROM_SETTINGS && (CommonUtills.checkLength(siEtConfirmPassword.getText().toString().trim()) <= 0 || siEtConfirmPassword.getText().toString().trim().isEmpty())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_confirm_password), Constants.NO_ACTION);
        } else if (isFromSettings != Constants.FROM_SETTINGS && CommonUtills.checkLength(siEtConfirmPassword.getText().toString().trim()) < 6) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_confirmpwd), Constants.NO_ACTION);
        } else if (isFromSettings != Constants.FROM_SETTINGS && !CommonUtills.checkPassWordAndConfirmPassword(siEtPassword.getText().toString().trim(), siEtConfirmPassword.getText().toString().trim())) {

            CommonUtills.showAlert(getActivity(), getString(R.string.su_empty_password_confirmspass_mismatch), Constants.NO_ACTION);
        } else {
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
            if (isFromSettings == Constants.FROM_OTHER) {
                if (CommonUtills.isNetworkAvailable(getActivity())) {

                    callRegisterUserWSMultiPart();
                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.NO_ACTION);
                }
            } else {
                if (CommonUtills.isNetworkAvailable(getActivity())) {

                    callEditUserProfileWSWithMultiPart();
                } else {
                    CommonUtills.showAlert(getActivity(), getString(R.string.no_internet_connection), Constants.NO_ACTION);
                }

            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                siIvUserImage.setImageURI(resultUri);
                imagePath = getPath1(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
*/
       /* if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }*/


        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK && data != null) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    siIvUserImage.setImageBitmap(photo);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getActivity(), photo);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    //File finalFile = new File(getRealPathFromURI(tempUri));

                    imagePath=getRealPathFromURI(tempUri);
                }
                break;
            case 1:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    if (selectedImage != null) {
                        Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);
                            siIvUserImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                            cursor.close();
                            imagePath=picturePath;
                        }
                    }
                }
                break;
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private void selectImage() {
     /*   final CharSequence[] items = {getActivity().getResources().getString(R.string.str_takephoto), getActivity().getResources().getString(R.string.str_choosefromlibrary),
                getActivity().getResources().getString(R.string.dialog_btn_cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.str_add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getActivity().getResources().getString(R.string.str_takephoto))) {
                    cameraIntent();
                } else if (items[item].equals(getActivity().getResources().getString(R.string.str_choosefromlibrary))) {
                    galleryIntent();
                } else if (items[item].equals(getActivity().getResources().getString(R.string.dialog_btn_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/
    }


    private String getPath1(Uri uri) {
        String result;
        Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            // Source is Dropbox or other similar local file path
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                file = new File(CommonUtills.getRealPathFromURI(getActivity(), CommonUtills.getImageUri(getActivity(), bm)));
                Glide.with(this).load(data.getData().toString()).apply(RequestOptions.circleCropTransform()).into(siIvUserImage);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {

        try {
            File sd = Environment.getExternalStorageDirectory();
            File dest = new File(sd, String.valueOf(System.currentTimeMillis()) + ".jpg");

            if (data.hasExtra("data")) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                try {
                    FileOutputStream out = new FileOutputStream(dest);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ImageLoader.getInstance().displayImage("file://" + dest.getAbsolutePath(), siIvUserImage, CommonUtills.getRoundImageOptions());

                file = new File(dest.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkAndRequestPermissions(final Activity context) {
        int WExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (WExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    private void chooseImage(Context context){
        final CharSequence[] optionsMenu = {"Take Photo", "Choose from Gallery", "Exit" }; // create a menuOption Array
        // create a dialog for showing the optionsMenu
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // set the items in builder
        builder.setItems(optionsMenu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(optionsMenu[i].equals("Take Photo")){
                    // Open the camera and get the photo
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                }
                else if(optionsMenu[i].equals("Choose from Gallery")){
                    // choose from  external storage
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                }
                else if (optionsMenu[i].equals("Exit")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }
}
