package wg.com.missugram.fragments;

import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.wg.framework.log.CustomLogHandler;
import com.wg.framework.util.CommonUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.adapter.CommentListingAdapter;
import wg.com.missugram.adapter.StickerImageAdapter;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommentResponseVo;
import wg.com.missugram.model.CommentsSingleResponseVo;
import wg.com.missugram.model.CommentsVo;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;

public class CommentsListing extends BaseFragment implements View.OnClickListener {
    private static final String SELECTED_TAB = "selectedTab";
    private static final String KEY_MEMORIUMID = "memoriamid";
    private static final String KEY_FROM_WHERE = "fromWhere";
    protected int pastVisibleItemsComments, visibleItemCountComments, totalItemCountComments;
    protected boolean isPullingMoreResultsComments;
    private RecyclerView mClRvCommentListing;
    private EditText mClEtComment;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private CommentListingAdapter mCommentListingAdapter;
    private StickerImageAdapter mStickerImageAdapter;
    private ImageView mCommentsIvAddImages;
    private ImageView mCommentsIvAddComments;
    private String clickedImageName;
    private int currentPage = 1;
    private RecyclerView mCommentsRvStickers;
    private ArrayList<String> alStickerImagePath;
    private int mMemoriumId;
    private int fromWhere;
    private ArrayList<CommentsVo> malCommentsVoArrayList;
    private int totalCommentSize;
    private boolean isloading;

    public static CommentsListing newInstance(int id, int fromWhere) {
        CommentsListing commentsListingFragment = new CommentsListing();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_MEMORIUMID, id);
        bundle.putInt(KEY_FROM_WHERE, fromWhere);
        commentsListingFragment.setArguments(bundle);
        return commentsListingFragment;
    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mMemoriumId = bundle.getInt(KEY_MEMORIUMID, 0);
            fromWhere = bundle.getInt(KEY_FROM_WHERE, 0);
            callGetCommentWS();
        }
        ((TestMainActivity) getActivity()).setCurrentMemoriamVal(fromWhere);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.commentslisting_layout_fragment, container, false);
        initViews(root);

        initUI();
        extractArguments();
        addScrollListener();
        return root;
    }

    private void initUI() {

        ((TestMainActivity) getActivity()).updateToolbar();
    }

    private void initViews(View root) {

        mClRvCommentListing = (RecyclerView) root.findViewById(R.id.mcomments_rv);
        mClEtComment = (EditText) root.findViewById(R.id.mcomments_et_comment);
        mCommentsIvAddComments = (ImageView) root.findViewById(R.id.mcomments_iv_addcomment);
        mCommentsIvAddImages = (ImageView) root.findViewById(R.id.mcomments_iv_addimages);
        mCommentsRvStickers = (RecyclerView) root.findViewById(R.id.mcomments_rv_stickers);

        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mClRvCommentListing.setLayoutManager(mLinearLayoutManager);
        mClRvCommentListing.setItemAnimator(new DefaultItemAnimator());

        setListeners();
        listAllImages();


        malCommentsVoArrayList = new ArrayList<>();

    }

    private void setListeners() {
        mCommentsIvAddComments.setOnClickListener(this);
        mCommentsIvAddImages.setOnClickListener(this);

    }

    private void prepareStickerImageAdapter() {
        mGridLayoutManager = new GridLayoutManager(getActivity(), 10);
        mCommentsRvStickers.setItemAnimator(new DefaultItemAnimator());
        mCommentsRvStickers.setLayoutManager(mGridLayoutManager);
        mStickerImageAdapter = new StickerImageAdapter(alStickerImagePath, getActivity());
        mCommentsRvStickers.setAdapter(mStickerImageAdapter);

        mStickerImageAdapter.setOnRecyclerItemListener(tag -> {
            clickedImageName = tag;
            if (clickedImageName.indexOf(".") > 0)
                clickedImageName = clickedImageName.substring(0, clickedImageName.lastIndexOf("."));


            mCommentsRvStickers.setVisibility(View.GONE);
            CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
            CommentsVo commentsVo = new CommentsVo();
            commentsVo.setMemorial_id(mMemoriumId);
            commentsVo.setUser_id(String.valueOf(MissUGram.getApp(getActivity()).getUserModel().getId()));
            commentsVo.setComment(clickedImageName);
            commentsVo.setIs_image(clickedImageName == null || clickedImageName.isEmpty() ? 0 : 1);
            final Call<CommentsSingleResponseVo> commentResponse = apiInterface.createComment(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), commentsVo);
            commentResponse.enqueue(new Callback<CommentsSingleResponseVo>() {

                @Override
                public void onResponse(Call<CommentsSingleResponseVo> call, Response<CommentsSingleResponseVo> response) {
                    CommonUtills.dismissProgressDialog();
                    if (response != null && response.body() != null) {
                        CommentsSingleResponseVo mCommentResponseVo = response.body();
                        if (mCommentResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {
                            CommentsVo mCommentsVo1 = new CommentsVo();
                            mCommentsVo1.setCreated_at(mCommentResponseVo.getCommentinfo().getCreated_at());
                            mCommentsVo1.setUserInfo(mCommentResponseVo.getCommentinfo().getUserInfo());
                            mCommentsVo1.setComment(mCommentResponseVo.getCommentinfo().getComment());
                            mCommentsVo1.setIs_image(mCommentResponseVo.getCommentinfo().getIs_image());
                            mCommentListingAdapter.addComments(mCommentsVo1, true);
                            mClRvCommentListing.scrollToPosition(mClRvCommentListing.getAdapter().getItemCount() - 1);
                            clickedImageName = null;

                        }
                    }
                }

                @Override
                public void onFailure(Call<CommentsSingleResponseVo> call, Throwable t) {
                    CommonUtills.dismissProgressDialog();
                    CustomLogHandler.printErrorlog(t);
                }
            });


        });


    }

    private void closeKeyboard() {
        try {
            CommonUtility.hideKeyboard(getActivity(), mClEtComment);

        } catch (Throwable throwable) {
            CustomLogHandler.printErrorlog(throwable);
        }
    }


    public void listAllImages() {

        try {
            alStickerImagePath = new ArrayList<>();
            alStickerImagePath = new ArrayList<String>(Arrays.asList(getActivity().getAssets().list(Constants.ASSEST_IMAGE_FOLDERNAME)));
            String[] images = getActivity().getAssets().list(Constants.ASSEST_IMAGE_FOLDERNAME);
            Arrays.sort(images, new AlphanumericSorting());
            alStickerImagePath = new ArrayList<String>(Arrays.asList(images));

            prepareStickerImageAdapter();

        } catch (IOException e) {
            CustomLogHandler.printErrorlog(e);
        }
    }

    private void callGetCommentWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);
        final Call<CommentResponseVo> commentsListing = apiInterface.getCommentsListing(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), Integer.valueOf(mMemoriumId), Constants.PAGE_COUNT, currentPage);
        commentsListing.enqueue(new Callback<CommentResponseVo>() {
            @Override
            public void onResponse(Call<CommentResponseVo> call, Response<CommentResponseVo> response) {

                if (response != null && response.body() != null) {
                    CommonUtills.dismissProgressDialog();

                    CommentResponseVo commentsListing1 = response.body();
                    if (commentsListing1.getsStatus() == Constants.STATUS_SUCCESS) {
                        totalCommentSize = commentsListing1.getTotal();
                        if (commentsListing1.getAlCommentListing().size() < Constants.PAGE_COUNT) {
                            isPullingMoreResultsComments = true;

                        } else {
                            isPullingMoreResultsComments = false;
                        }
                        totalCommentSize = commentsListing1.getTotal();
                        doProcessListReverse(commentsListing1.getAlCommentListing());

                    } else {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });


    }

    private void doProcessListReverse(ArrayList<CommentsVo> pCommentsVos) {

        isloading = false;

        if (mCommentListingAdapter == null) {
            for (int i = pCommentsVos.size() - 1; i >= 0; i--) {

                malCommentsVoArrayList.add(pCommentsVos.get(i));
            }

            mCommentListingAdapter = new CommentListingAdapter(getActivity(), malCommentsVoArrayList);
            mClRvCommentListing.setAdapter(mCommentListingAdapter);
        } else {
            //Create a temp array list. Since new array list contain the items that needs to be added from last item added to top in array list.
            ArrayList<CommentsVo> pCommentsVosTemp = new ArrayList<>();

            pCommentsVosTemp.addAll(pCommentsVos);
            pCommentsVos.clear();

            // So we are reversing the the list and adding the items again in list after clearing it and assigning into temp list.
            for (int i = pCommentsVosTemp.size() - 1; i >= 0; i--) {

                pCommentsVos.add(pCommentsVosTemp.get(i));
            }
            pCommentsVosTemp.clear();

            //now adding list to the 0 th position.
            mCommentListingAdapter.setCommentsVo(pCommentsVos);


        }
        callScrollToPosition(malCommentsVoArrayList);

        mCommentListingAdapter.setOnRecyclerItemListener((id -> {callCommentAbuse(id);}));
    }

    private void callScrollToPosition(final ArrayList<CommentsVo> pCommentsVos) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mClRvCommentListing.scrollToPosition(pCommentsVos.size() - 1);

            }
        }, 500);
    }

    private void callCommentAbuse(String p_CommentId) {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

        CommentsVo commentsVo = new CommentsVo();
        commentsVo.setUser_id(String.valueOf(MissUGram.getApp(getActivity()) == null ? "" : MissUGram.getApp(getActivity()).getUserModel().getId()));
        commentsVo.setComment_id(p_CommentId);
        final Call<CommonVo> commonVoCall = apiInterface.reportCommentAsAbuse(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), commentsVo);
        commonVoCall.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    CommonVo CommonVoReponse = response.body();
                    if (CommonVoReponse.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    } else {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    }

                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    private void callAddCommentWS() {
        CommonUtills.showProgressDialog(getActivity(), getString(R.string.str_pleasewait), false);

        CommentsVo commentsVo = new CommentsVo();
        commentsVo.setMemorial_id(mMemoriumId);
        commentsVo.setUser_id(String.valueOf(MissUGram.getApp(getActivity()).getUserModel().getId()));
        commentsVo.setComment(mClEtComment.getText().toString().trim());
        commentsVo.setIs_image(clickedImageName == null || clickedImageName.isEmpty() ? 0 : 1);

        final Call<CommentsSingleResponseVo> commentResponse = apiInterface.createComment(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(getActivity()), commentsVo);
        commentResponse.enqueue(new Callback<CommentsSingleResponseVo>() {
            @Override
            public void onResponse(Call<CommentsSingleResponseVo> call, Response<CommentsSingleResponseVo> response) {

                if (response != null && response.body() != null) {
                    CommonUtills.dismissProgressDialog();
                    CommentsSingleResponseVo mCommentsSingleResponseVo = response.body();
                    if (mCommentsSingleResponseVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommentsVo mCommentsVo1 = new CommentsVo();
                        mCommentsVo1.setCreated_at(mCommentsSingleResponseVo.getCommentinfo().getCreated_at());
                        mCommentsVo1.setUserInfo(mCommentsSingleResponseVo.getCommentinfo().getUserInfo());
                        mCommentsVo1.setComment(mCommentsSingleResponseVo.getCommentinfo().getComment());
                        mCommentsVo1.getUserInfo().setAvatar(mCommentsSingleResponseVo.getCommentinfo().getUserInfo().getAvatar());
                        mCommentsVo1.setIs_image(mCommentsSingleResponseVo.getCommentinfo().getIs_image());

                        mCommentListingAdapter.addComments(mCommentsVo1, true);
                        mClEtComment.setText("");
                        mClRvCommentListing.scrollToPosition(mLinearLayoutManager.findLastVisibleItemPosition() + 1);
                    } else {
                        CommonUtills.showAlert(getActivity(), response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }

            }

            @Override
            public void onFailure(Call<CommentsSingleResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
                CustomLogHandler.printErrorlog(t);
            }
        });
    }

    private void validateUserData() {
        if (CommonUtills.checkLength(mClEtComment.getText().toString().trim()) <= 0 || mClEtComment.getText().toString().trim().isEmpty()) {
            CommonUtills.showAlert(getActivity(), getString(R.string.cl_empty_comment), Constants.NO_ACTION);
        } else {
            callAddCommentWS();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mcomments_iv_addcomment:
                closeKeyboard();

                if (mCommentsRvStickers.getVisibility() == View.GONE) {
                    validateUserData();
                }

                break;
            case R.id.mcomments_iv_addimages:

                if (mCommentsRvStickers.getVisibility() == View.VISIBLE) {
                    mCommentsRvStickers.setVisibility(View.GONE);
                } else {
                    mCommentsRvStickers.setVisibility(View.VISIBLE);
                }

                break;

        }

    }

    private void addScrollListener() {
        mClRvCommentListing.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCountComments = mLinearLayoutManager.getChildCount();
                totalItemCountComments = mLinearLayoutManager.getItemCount();
                pastVisibleItemsComments = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (dy < 0) {
                    if (!isPullingMoreResultsComments) {

                        if (!isloading && (visibleItemCountComments + pastVisibleItemsComments) >= totalItemCountComments) {
                            isPullingMoreResultsComments = true;
                            currentPage++;
                            isloading = true;
                            callGetCommentWS();
                        }
                    }
                }

            }

        });
    }


    public class AlphanumericSorting implements Comparator {
        /**
         * The compare method that compares the alphanumeric strings
         */
        public int compare(Object firstObjToCompare, Object secondObjToCompare) {
            String firstString = firstObjToCompare.toString();
            String secondString = secondObjToCompare.toString();

            if (secondString == null || firstString == null) {
                return 0;
            }

            int lengthFirstStr = firstString.length();
            int lengthSecondStr = secondString.length();

            int index1 = 0;
            int index2 = 0;

            while (index1 < lengthFirstStr && index2 < lengthSecondStr) {
                char ch1 = firstString.charAt(index1);
                char ch2 = secondString.charAt(index2);

                char[] space1 = new char[lengthFirstStr];
                char[] space2 = new char[lengthSecondStr];

                int loc1 = 0;
                int loc2 = 0;

                do {
                    space1[loc1++] = ch1;
                    index1++;

                    if (index1 < lengthFirstStr) {
                        ch1 = firstString.charAt(index1);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch1) == Character.isDigit(space1[0]));

                do {
                    space2[loc2++] = ch2;
                    index2++;

                    if (index2 < lengthSecondStr) {
                        ch2 = secondString.charAt(index2);
                    } else {
                        break;
                    }
                } while (Character.isDigit(ch2) == Character.isDigit(space2[0]));

                String str1 = new String(space1);
                String str2 = new String(space2);

                int result;

                if (Character.isDigit(space1[0]) && Character.isDigit(space2[0])) {
                    Integer firstNumberToCompare = new Integer(Integer
                            .parseInt(str1.trim()));
                    Integer secondNumberToCompare = new Integer(Integer
                            .parseInt(str2.trim()));
                    result = firstNumberToCompare.compareTo(secondNumberToCompare);
                } else {
                    result = str1.compareTo(str2);
                }

                if (result != 0) {
                    return result;
                }
            }
            return lengthFirstStr - lengthSecondStr;
        }
    }
}
