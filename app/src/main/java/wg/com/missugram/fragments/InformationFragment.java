package wg.com.missugram.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.model.CmsInfoVo;

public class InformationFragment extends BaseFragment {

    private static final String KEY_CMSINFO_VO = "cmsInfoVo";
    private static final String KEY_SERCH_URLS = "searchurls";
    private WebView mWVInformation;
    private CmsInfoVo mCmsInfoVo;
    private String url;
    private ProgressDialog mProgressDialog = null;

    public static InformationFragment newInstance(CmsInfoVo pCmsInfoVo, String p_Urls) {
        InformationFragment mInformationFragment = new InformationFragment();
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(KEY_CMSINFO_VO, pCmsInfoVo);
        mBundle.putString(KEY_SERCH_URLS, p_Urls);
        mInformationFragment.setArguments(mBundle);
        return mInformationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.information_fragment_layout, container, false);
        initUI(root);
        extractArguments();
        return root;
    }

    private void initUI(View view) {
        mWVInformation = (WebView) view.findViewById(R.id.if_wv);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(this.getResources().getString(R.string.str_pleasewait));
        mProgressDialog.setCancelable(false);

        ((TestMainActivity) getActivity()).updateToolbar();



    }

    private void extractArguments() {
        Bundle bundle = getArguments();
        if (bundle != null) {

            if (bundle.getParcelable(KEY_CMSINFO_VO) != null) {
                mCmsInfoVo = bundle.getParcelable(KEY_CMSINFO_VO);
                // update the toolbar title from here.
                ((TestMainActivity) getActivity()).setToolbarTitle(mCmsInfoVo.getPage_name());
                ((TestMainActivity) getActivity()).updateToolbar();

                loadWebView(mCmsInfoVo.getContent());
            } else {

                if (bundle.getString(KEY_SERCH_URLS) != null && !bundle.getString(KEY_SERCH_URLS).isEmpty()) {
                    url = bundle.getString(KEY_SERCH_URLS);
                    loadWebView(url);
                }
            }
        }
    }

    private void loadWebView(String p_Content) {
        mWVInformation.getSettings().setJavaScriptEnabled(true);
        mWVInformation.getSettings().setDomStorageEnabled(true);
        if (p_Content.trim().startsWith("http://") || p_Content.trim().startsWith("https://")) {
            loadUrlInWebView(p_Content);
        } else {
            mWVInformation.loadData(p_Content.trim(), "text/html", "UTF-8");
        }
    }

    private void loadUrlInWebView(String p_Url) {

        mWVInformation.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {


                if (!getActivity().isFinishing() && mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

        mWVInformation.loadUrl(p_Url);
    }
}
