package wg.com.missugram.network;


import com.wg.framework.log.CustomLogHandler;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    //public final static String BASE_ADDRESS = "http://192.168.101.173:8021/api/";
   // public final static String BASE_ADDRESS = "http://ec2-35-169-133-55.compute-1.amazonaws.com/api/";
   // public final static String BASE_ADDRESS = "http://54.189.89.162/api/";
    public final static String BASE_ADDRESS = "https://m-u-g.com/api/";
       // public final static String BASE_ADDRESS = "http://chessmafia.com/php/MissUGram/api/";
    public final static String BASE_ADDRESS_STORE = "https://api.magnaversum.com/";
    public final static String BASE_Image = "http://chessmafia.com/php/MissUGram/public/";


    private static Retrofit retrofit = null;
    private static Retrofit storeRetrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_ADDRESS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }

        return retrofit;
    }

    public static Retrofit getStore() {
        if (storeRetrofit == null) {
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                }


                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                }


                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext;
            try {
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(120, TimeUnit.SECONDS)
                        .writeTimeout(120, TimeUnit.SECONDS)
                        .readTimeout(120, TimeUnit.SECONDS)
                        .sslSocketFactory(sslSocketFactory).hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .build();
                storeRetrofit = new Retrofit.Builder()
                        .baseUrl(BASE_ADDRESS_STORE)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();


            } catch (NoSuchAlgorithmException | KeyManagementException e1) {
                CustomLogHandler.printErrorlog(e1);
            }

        }

        return storeRetrofit;
    }


}
