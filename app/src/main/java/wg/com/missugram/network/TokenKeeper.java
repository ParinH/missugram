package wg.com.missugram.network;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import wg.com.missugram.constants.Constants;


public class TokenKeeper {

    private static TokenKeeper tokenInstance;
    private final static String TAG = "TokenKeeper";


    //private String mToken;
    private String accessToken;
    private int expireIn = 3600;
    private int refreshTries;


    private long lastRefreshedAt;


    public static TokenKeeper getInstance() {
        if (tokenInstance == null)
            tokenInstance = new TokenKeeper();
        return tokenInstance;
    }

    public void storeTokens(Context context, String accessToken, int expireIn) {
        this.accessToken = accessToken;
        this.expireIn = expireIn;
        lastRefreshedAt = System.currentTimeMillis();
        context.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_ACCESS_TOKEN, accessToken).apply();
    }


    public String getAccessToken(Activity activity) {
        if (accessToken != null && !TextUtils.isEmpty(accessToken))
            return accessToken;
        else if (activity != null) {
            SharedPreferences prefs = activity.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE);
            if (prefs.contains(Constants.PREF_ACCESS_TOKEN)) {
                return prefs.getString(Constants.PREF_ACCESS_TOKEN, "");
            } else
                return "";
        } else
            return "";
    }

}