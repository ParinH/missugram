
package wg.com.missugram.network;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import wg.com.missugram.model.CommentResponseVo;
import wg.com.missugram.model.CommentsSingleResponseVo;
import wg.com.missugram.model.CommentsVo;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.ContactRequestVo;
import wg.com.missugram.model.ContactResponseVo;
import wg.com.missugram.model.InviteRequestVo;
import wg.com.missugram.model.LoginResponseVo;
import wg.com.missugram.model.MemorialDetailSingleResponseVo;
import wg.com.missugram.model.MemorialEditResponseVo;
import wg.com.missugram.model.MemorialResponseVo;
import wg.com.missugram.model.NotificationRequestVo;
import wg.com.missugram.model.SlugVo;
import wg.com.missugram.model.UserModel;

public interface ApiInterface {

    @GET("{path}")
    Call<MemorialResponseVo> getMemorials(@Path("path") String path, @Header("Authorization") String token, @Query("perpage") int perPage, @Query("pageno") int pageNo, @Query("type") String type, @Query("userid") int userid);

    @POST("register")
    Call<LoginResponseVo> registerUser(@Body UserModel registerRequestVo);

    @Multipart
    @POST("register")
    Call<LoginResponseVo> registerUser(@Part MultipartBody.Part image, @Part("webdata") RequestBody typedString);

    @Multipart
    @POST("register")
    Call<LoginResponseVo> registerUserWithoutProfile(@Header("Authorization") String token, @Part("webdata") RequestBody typedString);

    @POST("login")
    Call<LoginResponseVo> loginUser(@Body UserModel loginRequestVo);

    @POST("resetpassword")
    Call<CommonVo> changeUserPassword(@Header("Authorization") String token, @Body UserModel changePasswordRequestVo);

    @POST("forgotpassword")
    Call<CommonVo> forgotPassword(@Header("Authorization") String token, @Body UserModel forgotPasswordRequestVo);

    @Multipart
    @POST("memoriam")
    Call<CommonVo> createMemorium(@Header("Authorization") String token,
                                  @Part MultipartBody.Part image,
                                  @Part("webdata") RequestBody typedString);

    @Multipart
    @POST("updateprofile")
    Call<LoginResponseVo> editProfile(@Header("Authorization") String token, @Part MultipartBody.Part image, @Part("webdata") RequestBody jsonString);

    @Multipart
    @POST("updateprofile")
    Call<LoginResponseVo> editProfileWithOutImage(@Header("Authorization") String token, @Part("webdata") RequestBody jsonString);


    @POST("createcomment")
    Call<CommentsSingleResponseVo> createComment(@Header("Authorization") String token, @Body CommentsVo memorialVo);

    @GET("getcomments")
    Call<CommentResponseVo> getCommentsListing(@Header("Authorization") String token, @Query("memoriam_id") int memoriam_id, @Query("perpage") int perpage, @Query("pageno") int pageno);

    @POST("updatematchcontact")
    Call<CommonVo> submitContactDetail(@Header("Authorization") String token, @Body ContactRequestVo loginRequestVo);

    @GET("getmatchcontact")
    Call<ContactResponseVo> getMatchedContact(@Header("Authorization") String token, @Query("perpage") int perPage, @Query("pageno") int pageNo, @Query("userid") int userid);

    @POST("sendinvite")
    Call<CommonVo> sendInvitation(@Header("Authorization") String token, @Body InviteRequestVo inviteRequestVo);

    /* @HTTP(method = "DELETE", path = "deleteprofile", hasBody = true)
     Call<CommonVo> deleteProfile(@Header("Authorization") String token, @Body UserModel commonVo);
 */
    @HTTP(method = "DELETE", path = "deleteprofile", hasBody = true)
    Call<CommonVo> deleteProfile(@Header("Authorization") String token, @QueryMap Map<String, String> params);

    @Multipart
    @POST("updatememoriam")
    Call<MemorialEditResponseVo> updateMemorium(@Header("Authorization") String token,
                                                @Part MultipartBody.Part image,
                                                @Part("webdata") RequestBody jsonString);

    @GET("getcms")
    Call<SlugVo> callSlug(@Header("Authorization") String token, @Query("slug") String slug);

    @GET
    Call<MemorialDetailSingleResponseVo> getDetailFromMemoriumId(@Header("Authorization") String token, @Url String url);

    /*@GET("memoriam/{path}")
    Call<MemorialDetailSingleResponseVo> getDetailFromMemoriumId(@Header("Authorization") String token,@Path("path") String path);*/

    @PUT("usersetting")
    Call<CommonVo> updateNotification(@Header("Authorization") String token, @Body NotificationRequestVo notificationRequestVo);

    @POST("commentabuse")
    Call<CommonVo> reportCommentAsAbuse(@Header("Authorization") String token, @Body CommentsVo commentsVo);

    @GET("logout")
    Call<CommonVo> logout(@Header("Authorization") String token, @Query("userid") int userid);

    @PUT("updatequickbloxaccount")
    Call<CommonVo> updateQuickBloxAccount(@Header("Authorization") String token, @QueryMap Map<String, String> params);

}
