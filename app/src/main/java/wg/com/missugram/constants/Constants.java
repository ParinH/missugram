package wg.com.missugram.constants;


import java.io.File;

public class Constants {

    public static boolean BY_PASS_SPLASH = false;

    public static boolean DEBUG = false;
    public static int FROM_MY_MEMORIAL = 1;
    public static int FROM_MEMORIAL_LISTING = 2;
    public static int FROM_SETTINGS = 3;
    public static int FROM_OTHER = 4;

    public static int FRIEND_LIST_FROM_FROM_MAIN = 1;
    public static int FRIEND_LIST_FROM_FROM_INVITE = 2;

    public static int FROM_MYMEMORIAL_EDIT = 5;
    public static int FROM_SEARCH_DECEASED = 6;
    public static int FROM_SEARCH_QUOTE = 7;
    public static int FROM_SLUG = 8;

    public static  int myCate_id = 1;


    public static int FROM_GROUP_CHAT_TAB = 51;
    public static int FROM_GROUP_CHAT_TAB_TO_FRIENDS = 52;

    public static int FOR_GROUP_CHAT = 71;
    public static int FOR_PRIVATE_CHAT = 72;
    public static final String PREF_USER = "user";


    public static int PAGE_COUNT = 1000000000;
    public static String MISSU_GRAM_PREF = "MISSU_GRAM_PREF";
    public static String PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN";
    public static String PREF_FIRST_NAME = "PREF_FIRST_NAME";
    public static String PREF_LAST_NAME = "PREF_LAST_NAME";
    public static String PREF_USER_AVATAR = "PREF_USER_AVATAR";
    public static String PREF_USER_PASSWORD = "PREF_USER_PASSWORD";
    public static String PREF_USER_EMAIL = "PREF_USER_EMAIL";
    public static String PREF_USER_ID = "PREF_USER_ID";
    public static String PREF_TOKEN = "PREF_TOKEN";
    public static String PREF_FIREBASE_DEVICE_TOKEN = "PREF_FIREBASE_DEVICE_TOKEN";
    public static String PREF_FIREBASE_FROM_NOTIFICATION = "PREF_FIREBASE_FROM_NOTIFICATION";
    public static String PREF_NOTIFICATION_ON = "PREF_NOTIFICATION_ON";
    public static String PREF_PRIVATE_CHAT_DIALOG_ID = "PREF_PRIVATE_CHAT_DIALOG_ID";
    public static String PREF_PREF_GROUP_CHAT_DIALOG_ID = "PREF_PREF_GROUP_CHAT_DIALOG_ID";

    public static String PREF_PREF_CURRENT_CHAT_UNAME = "PREF_PREF_CURRENT_CHAT_UNAME";
    public static String PREF_PREF_CURRENT_CHAT_USER_PWD = "PREF_PREF_CURRENT_CHAT_USERPWD";

    public static String PREF_CHAT_ID = "PREF_CHAT_ID";
    public static String PREF_CHAT_LOGGEDIN = "PREF_CHAT_LOGGEDIN";

    public static String KEY_AUTHORIZATION = "Authorization";
    public static final String KEY_BEARER = "Bearer ";

    public static final int ACTION_SIGN_IN = 1;
    public static final int ACTION_REGISTER = 2;
    public static final int NO_ACTION = 0;
    public static final int ACTION_MAIN = 3;
    public static final int ACTION_FINISH = 4;
    public static final int ACTION_SETTINGS = 5;
    public static final int ACTION_FRAGMENT_FINISH = 6;
    public static final int ACTION_FRAGMENT_DESCRIPTION = 7;
    public static final int ACTION_FRAGMENT_CREATE_MYMEMORIAL = 8;
    public static final int ACTION_LOGIN_FROM_SETTINGS = 9;


    public static final String MEMORIUM_TYPE_PUBLIC = "Public";
    public static final String MEMORIUM_TYPE_PRIVATE = "Private";
    public static final String MEMORIUM_TYPE_INVITE = "Invited";

    public static final String URL_MY_MEMORIAM = "mymemoriam";
    public static final String URL_MEMORIAM_PAGE = "memoriam";
    public static final int STATUS_SUCCESS = 1;
    public static final String ASSEST_IMAGE_FOLDERNAME = "StickerImages";
    public static final String PATH_ASSEST_IMAGE_FOLDERNAME = "assets://" + ASSEST_IMAGE_FOLDERNAME + File.separator;

    public static final String SLUG_TERMSNCONDITIONS = "terms-and-conditions";
    public static final String SLUG_ABOUTUS = "about-us";
    public static final String SLUG_CONTACTUS = "contact-us";

    public static final int NOTIFICATION_ID_MEMORIAL_INVITATION = 1;
    public static final int NOTIFICATION_ID_COMMENT = 2;

    public static final String KEY_PUSH_NOTIFICATIONID = "push_notificationid";
    public static final String KEY_PUSH_USERID = "push_userid";
    public static final String KEY_PUSH_MEMORIAMID = "pushmemorialid";

    public static final String CHAT_APP_ID = "62642";
    public static final String CHAT_AUTH_KEY = "7QreK4LuVTpL3Rw";
    public static final String CHAT_AUTH_SECRET = "B3Crv5w7N26LC9N";
    public static final String CHAT_APP_ACCOUNTKEY = "a2dXxpden8pyhYYTmKtZ";


    public static final int CHAT = 1;

    public static String KEY_STICKER_IMAGE = "stickerImage";
    public static String KEY_MESSAGE_DEFAULT = "default";


    public static String KEY_STICKER_IMAGE1 = "sticker1.png";
    public static String KEY_STICKER_IMAGE2 = "sticker2.png";
    public static String KEY_STICKER_IMAGE3 = "sticker3.png";
    public static String KEY_STICKER_IMAGE4 = "sticker4.png";
    public static String KEY_STICKER_IMAGE5 = "sticker5.png";
    public static String KEY_STICKER_IMAGE6 = "sticker6.png";
    public static String KEY_STICKER_IMAGE7 = "sticker7.png";
    public static String KEY_STICKER_IMAGE8 = "sticker8.png";
    public static String KEY_STICKER_IMAGE9 = "sticker9.png";
    public static String KEY_STICKER_IMAGE10 = "sticker10.png";
    public static String KEY_STICKER_IMAGE11 = "sticker11.png";
    public static String KEY_STICKER_IMAGE12 = "sticker12.png";
    public static String KEY_STICKER_IMAGE13 = "sticker13.png";
    public static String KEY_STICKER_IMAGE14 = "sticker14.png";
    public static String KEY_STICKER_IMAGE15 = "sticker15.png";
    public static String KEY_STICKER_IMAGE16 = "sticker16.png";
    public static String KEY_STICKER_IMAGE17 = "sticker17.png";
    public static String KEY_STICKER_IMAGE18 = "sticker18.png";
    public static String KEY_STICKER_IMAGE19 = "sticker19.png";
    public static String KEY_STICKER_IMAGE20 = "sticker20.png";
    public static String KEY_STICKER_IMAGE21 = "sticker21.png";
    public static String KEY_STICKER_IMAGE22 = "sticker22.png";
    public static String KEY_STICKER_IMAGE23 = "sticker23.png";

    public static String IMAGE_DIRECTORY_NAME = "images";
}
