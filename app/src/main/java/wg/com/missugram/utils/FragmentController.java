package wg.com.missugram.utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import wg.com.missugram.R;
import wg.com.missugram.activities.LoginActivity;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.fragments.ChangePasswordFragment;
import wg.com.missugram.fragments.ChatFragment;
import wg.com.missugram.fragments.CommentsListing;
import wg.com.missugram.fragments.CreateGroup;
import wg.com.missugram.fragments.CreateMemorium;
import wg.com.missugram.fragments.DescriptionFragment;
import wg.com.missugram.fragments.ForgotPasswordFragment;
import wg.com.missugram.fragments.FriendsAndFamilyFragment;
import wg.com.missugram.fragments.GrievanceChatFragment;
import wg.com.missugram.fragments.InformationFragment;
import wg.com.missugram.fragments.LoginFragment;
import wg.com.missugram.fragments.MemorialFragment;
import wg.com.missugram.fragments.SettingsFragment;
import wg.com.missugram.fragments.SignUpFragment;
import wg.com.missugram.fragments.SplashFragment;
import wg.com.missugram.fragments.TestMemorialFragment;
import wg.com.missugram.fragments.TestMyMemorialFragment;
import wg.com.missugram.model.CmsInfoVo;
import wg.com.missugram.model.MemorialVo;


public class FragmentController {

    public final static String TAG_SPLASH_FRAG = "SplashFragment";
    public final static int LOGIN_ROOT = R.id.login_root;
    public final static int MAIN_ROOT = R.id.content_main;
    public final static String TAG_SPLASH_FRAGMENT = "splashFragment";
    public final static String TAG_LOGIN_FRAGMENT = "loginFragment";
    public final static String TAG_SIGNUP_FRAGMENT = "signupFragment";

    public final static String TAG_FIRST_FRAGMENT = "firstFragment";
    public final static String TAG_MEMORIAL_FRAGMENT = "memorialFragment";
    public final static String TAG_MY_MEMORIAL_FRAGMENT = "mymemorialFragment";
    public final static String TAG_DESCRIPTION_FRAGMENT = "descriptionFragment";
    public final static String TAG_SETTINGS_FRAGMENT = "settingsFragment";
    public final static String TAG_COMMENTSLISTING_FRAGMENT = "commentslistingFragment";
    public final static String TAG_CREATE_MEMORIUM_FRAGMENT = "creatememoriumFragment";
    public final static String TAG_CHANGE_PASSWORD_FRAGMENT = "changepasswordFragment";
    public final static String TAG_FORGOT_PASSWORD_FRAGMENT = "forgotpasswordFragment";
    public final static String TAG_EDIT_PROFILE_FRAGMENT = "editFragment";
    public final static String TAG_FRIEND_AND_FAMILY_FRAGMENT = "Favorites";
    public final static String TAG_INFORMATION_FRAGMENT = "informationFragment";
    public final static String TAG_GRIEVANCE_FRAGMENT = "grievanceFragment";
    public final static String TAG_CHAT_FRAGMENT = "chatFragment";
    public final static String TAG_CREATE_GROUP_FRAGMENT = "creategroupFragment";


    public static Fragment fragment;

    public static Fragment addSplashFragment(FragmentActivity activity) {
        if (activity != null) {

            fragment = SplashFragment.newInstance();
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_SPLASH_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addLoginFragment(FragmentActivity activity) {
        if (activity != null) {

            fragment = LoginFragment.newInstance();
            activity.getSupportFragmentManager().beginTransaction().replace(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_LOGIN_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addChangePasswordFragment(FragmentActivity activity) {
        if (activity != null) {

            fragment = ChangePasswordFragment.newInstance();
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_CHANGE_PASSWORD_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addForgotPasswordFragment(FragmentActivity activity) {
        if (activity != null) {

            fragment = ForgotPasswordFragment.newInstance();
            activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right, R.anim.slide_in_left, R.anim.slide_in_right)
                    .add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_FORGOT_PASSWORD_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addSignUpFragment(FragmentActivity activity, int fromSettings) {
        if (activity != null) {

            fragment = SignUpFragment.newInstance(fromSettings);
            if (fromSettings == Constants.FROM_SETTINGS) {
                activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_EDIT_PROFILE_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();

            } else {
                activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_SIGNUP_FRAGMENT).addToBackStack(null).commitAllowingStateLoss();

            }
            return fragment;

        } else
            return null;
    }


    public static Fragment addSettingsFragment(FragmentActivity activity, String str) {
        if (activity != null) {

            fragment = SettingsFragment.newInstance(str);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_SETTINGS_FRAGMENT).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addGrievanceChatFragment(FragmentActivity activity) {

        if (activity != null) {
            fragment = GrievanceChatFragment.newInstance();
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment,TAG_GRIEVANCE_FRAGMENT).commitAllowingStateLoss();
            return fragment;
        }
        return null;
    }
    public static Fragment addCreateGroupFragment(FragmentActivity activity, ArrayList<Integer>  idList) {

        if (activity != null) {
            fragment = CreateGroup.newInstance(idList);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment,TAG_CREATE_GROUP_FRAGMENT).commitAllowingStateLoss();
            return fragment;
        }
        return null;
    }

    public static Fragment addMemorialFragment(FragmentActivity activity, int fromWhere) {
        if (activity != null) {

            fragment = TestMemorialFragment.newInstance(fromWhere);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_MEMORIAL_FRAGMENT).commitAllowingStateLoss();

            return fragment;

        } else
            return null;
    }


    public static Fragment myMemorialFragment(FragmentActivity activity, int fromWhere) {
        if (activity != null) {

            fragment = TestMyMemorialFragment.newInstance(fromWhere);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_MY_MEMORIAL_FRAGMENT).commitAllowingStateLoss();

            return fragment;

        } else
            return null;
    }

    public static Fragment addFriendListFragment(FragmentActivity activity, int fromWhere, MemorialVo memorialVo) {
        if (activity != null) {

            fragment = FriendsAndFamilyFragment.newInstance(fromWhere, memorialVo);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_FRIEND_AND_FAMILY_FRAGMENT).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }

    public static Fragment addInformationFragment(FragmentActivity activity, CmsInfoVo p_cmsInfoVo, String p_Url) {

        if (activity != null) {
            fragment = InformationFragment.newInstance(p_cmsInfoVo, p_Url);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_INFORMATION_FRAGMENT).commitAllowingStateLoss();
            return fragment;
        } else
            return null;

    }

    public static Fragment addCommentsListingFragment(FragmentActivity activity, int p_MemoriumId, int fromWhere) {
        if (activity != null) {

            fragment = CommentsListing.newInstance(p_MemoriumId, fromWhere);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_COMMENTSLISTING_FRAGMENT).commitAllowingStateLoss();

            return fragment;

        } else
            return null;
    }

    public static Fragment addCreateMemoriumFragment(FragmentActivity activity, MemorialVo memorialVo, int fromWhere) {
        if (activity != null) {

            fragment = CreateMemorium.newInstance(memorialVo, fromWhere);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_CREATE_MEMORIUM_FRAGMENT).commitAllowingStateLoss();

            return fragment;

        } else
            return null;
    }

    public static Fragment addDescriptionFragment(FragmentActivity activity, MemorialVo memorialVo, int fromWhere) {
        if (activity != null) {

            fragment = DescriptionFragment.newInstance(memorialVo, fromWhere);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_DESCRIPTION_FRAGMENT).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }
    public static Fragment addChatFragment(FragmentActivity activity, int quickbloxId,int fromWhere,String GroupDailogId,String name,String url) {
        if (activity != null) {

            fragment = ChatFragment.newInstance(quickbloxId,fromWhere,GroupDailogId,name,url);
            activity.getSupportFragmentManager().beginTransaction().add(activity instanceof LoginActivity ? LOGIN_ROOT : MAIN_ROOT, fragment, TAG_CHAT_FRAGMENT).commitAllowingStateLoss();
            return fragment;

        } else
            return null;
    }


    public static void removeThisFragment(FragmentActivity activity, String which) {
        Fragment frag = isThisFragmentInFront(activity, which);
        if (frag != null) {
            //activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_out_left, R.anim.slide_in_right).remove(frag).commit();
            activity.onBackPressed();
        }
    }

    public static Fragment isThisFragmentInFront(FragmentActivity activity, String which) {

        try {
            if (activity != null && which != null) {
                Fragment frag = activity.getSupportFragmentManager().findFragmentById(activity instanceof TestMainActivity ? MAIN_ROOT : LOGIN_ROOT);
                if (which.equals(frag.getTag()))
                    return frag;
                else
                    return null;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

    }

    public static String getFrontFragmentTag(FragmentActivity activity) {
        if (activity != null) {
            return activity.getSupportFragmentManager().findFragmentById(activity instanceof TestMainActivity ? MAIN_ROOT : LOGIN_ROOT).getTag();
        }
        return null;
    }

    public static Fragment getFragmentByTag(FragmentActivity activity, String tag) {
        try {
            return activity.getSupportFragmentManager().findFragmentByTag(tag);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean removeFragment(FragmentActivity activity, String fragmentName) {
        try {
            if (activity != null && fragmentName != null) {
                Fragment frag = activity.getSupportFragmentManager().findFragmentById(activity instanceof TestMainActivity ? MAIN_ROOT : LOGIN_ROOT);
                if (frag != null && fragmentName.equals(frag.getTag())) {
                    activity.getSupportFragmentManager().beginTransaction()

                            .remove(frag).commit();
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
        }
        return false;

    }


    public static boolean isThisFragmentLastInStack(FragmentActivity activity) {
        return (activity.getSupportFragmentManager().getBackStackEntryCount() == 1);
    }

    public static void removeAllFragmentsFromStack(FragmentActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    public static Fragment getCurrentFragment(FragmentActivity activity) {
//        FragmentManager fragmentManager = activity.getSupportFragmentManager();
//        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
//        Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
        return fragment;
    }

}
