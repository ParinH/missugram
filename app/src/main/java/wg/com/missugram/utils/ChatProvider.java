package wg.com.missugram.utils;

import android.os.Bundle;
import android.text.TextUtils;

import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatModel;

public class ChatProvider {
    public static final int CHAT_HISTORY_ITEMS_PER_PAGE = 30;
    public static final String PROPERTY_OCCUPANTS_IDS = "occupants_ids";
    public static final String PROPERTY_DIALOG_TYPE = "type";
    public static final String PROPERTY_DIALOG_NAME = "name";
    public static final String PROPERTY_NOTIFICATION_TYPE = "notification_type";


    public static final String PROPERTY_ROOMJID = "xmpp_room_jid";
    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_TYPE_LAST_MESSAGE_DATE = "lastMessageDate";


    private static final String CHAT_HISTORY_ITEMS_SORT_FIELD = "date_sent";
    public static ChatProvider chatProvider;

    private OnUserWithMessageResponse mOnUserWithMessageResponse;

    public static ChatProvider getInstance() {
        if (chatProvider == null)
            chatProvider = new ChatProvider();
        return chatProvider;
    }

    public void setmOnUserWithMessageResponse(OnUserWithMessageResponse mOnUserWithMessageResponse) {
        this.mOnUserWithMessageResponse = mOnUserWithMessageResponse;
    }

    public void logoutUser() {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        QBChatService.getInstance().logout();
                    }
                    catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

    }


    public void registerUserForChat(final QBUser user, final String password, final QBEntityCallback<QBUser> callback) {
        QBUsers.signUp(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                signInChatUser(qbUser, password, callback);
            }

            @Override
            public void onError(QBResponseException e) {
            }
        });
    }

    public void signInChatUser(final QBUser user, final String password, final QBEntityCallback<QBUser> callback) {
        QBUsers.signInByEmail(user.getLogin(), password).performAsync(callback);

    }

    public boolean checkSignIn() {
        return QBSessionManager.getInstance().getSessionParameters() != null;
    }

    public void loadChatHistory(QBChatDialog dialog,
                                final QBEntityCallback<ArrayList<QBChatMessage>> callback) {

        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
       // customObjectRequestBuilder.setLimit(CHAT_HISTORY_ITEMS_PER_PAGE);
        customObjectRequestBuilder.sortDesc(CHAT_HISTORY_ITEMS_SORT_FIELD);

        QBRestChatService.getDialogMessages(dialog, customObjectRequestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {
                Set<Integer> userIds = new HashSet<>();
                for (QBChatMessage message : qbChatMessages) {
                    userIds.add(message.getSenderId());
                }
                if (!userIds.isEmpty()) {
                    getUsersFromMessages(qbChatMessages, userIds, callback);
                } else {
                    callback.onSuccess(qbChatMessages, bundle);
                }

                // Not calling super.onSuccess() because
                // we're want to load chat users before triggering the callback
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    public void loadGroupChatWithUsers(QBChatDialog chatDialog, int CurrentUserId) {

        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setLimit(CHAT_HISTORY_ITEMS_PER_PAGE);
        customObjectRequestBuilder.sortDesc(CHAT_HISTORY_ITEMS_SORT_FIELD);

        QBRestChatService.getDialogMessages(chatDialog, customObjectRequestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {

                ArrayList<ChatModel> messageList = new ArrayList<>();

                if (qbChatMessages != null && qbChatMessages.size() > 0) {

                    Set<Integer> userIds = new HashSet<>();
                    ChatModel model;
                    for (QBChatMessage message : qbChatMessages) {
                        if (message.getBody()!=null&&!message.getBody().equalsIgnoreCase("null")) {
                            userIds.add(message.getSenderId());
                            model = new ChatModel();
                            model.setQbChatMessage(message);

                            messageList.add(model);
                        }
                        }
                    if (!userIds.isEmpty()) {
                        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
                        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(
                                new QBEntityCallback<ArrayList<QBUser>>() {
                                    @Override
                                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                                        if (qbUsers.size() > 0 && messageList.size() > 0) {

                                            for (int i = 0; i < messageList.size(); i++) {
                                                for (QBUser qbUser : qbUsers) {

                                                    if (qbUser.getId() != CurrentUserId && qbUser.getId().equals(messageList.get(i).getQbChatMessage().getSenderId())) {
                                                        // System.out.println("id is:" + qbUser.getId() + " with photo :" + qbUser.getFileId());
                                                        messageList.get(i).setFileId(qbUser.getFileId() == null ? -1 : qbUser.getFileId());
                                                        messageList.get(i).setname(TextUtils.isEmpty(qbUser.getFullName()) ? "" : qbUser.getFullName());
                                                    }

                                                }
                                            }
                                            mOnUserWithMessageResponse.onData(messageList);
                                        }
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        e.getLocalizedMessage();
                                        mOnUserWithMessageResponse.onData(messageList);
                                    }
                                });
                    }
                    else {
                        /// if user information not got somehow also get a callback to  a calling fragment.
                        mOnUserWithMessageResponse.onData(messageList);
                    }
                } else {
                    mOnUserWithMessageResponse.onData(messageList);
                }
            }

            @Override
            public void onError(QBResponseException e) {
                mOnUserWithMessageResponse.onData(new ArrayList<ChatModel>());
                e.printStackTrace();
            }
        });

    }


    public void getUsersFromMessages(final ArrayList<QBChatMessage> messages,
                                     final Set<Integer> userIds,
                                     final QBEntityCallback<ArrayList<QBChatMessage>> callback) {

        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(
                new QBEntityCallback<ArrayList<QBUser>>() {
                    @Override
                    public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                        callback.onSuccess(messages, bundle);
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });
    }


    public void sendMessage(String msg, QBChatDialog qbChatDialog, int recipientQuickbloxid, QBAttachment pQbAttachment, int senderId, String stickerImage) {


        final QBChatMessage chatMessage = new QBChatMessage();
        if (pQbAttachment != null) {
            //chatMessage.setAttachments(pQbAttachment);
            chatMessage.addAttachment(pQbAttachment);
        }

        if (stickerImage != null) {
            chatMessage.setProperty(Constants.KEY_STICKER_IMAGE, stickerImage);
            chatMessage.setBody(stickerImage);
        } else {
            chatMessage.setBody(msg);
        }
        chatMessage.setSaveToHistory(true);
        chatMessage.setDateSent(System.currentTimeMillis() / 1000);
        chatMessage.setMarkable(true);
        chatMessage.setSenderId(senderId);
        chatMessage.setRecipientId(recipientQuickbloxid);

        try {
            qbChatDialog.sendMessage(chatMessage);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }  catch (Exception e) {
        //    Log.e(TAG, "Failed to send a message", e);
        }

    }

    public void sendGroupMessage(QBChatDialog qbChatDialogGroup, String msg, Integer senderId, QBAttachment qbAttachment, String stickerImage) {
        try {
            QBChatMessage chatMessage = new QBChatMessage();

            if (qbAttachment != null) {
                chatMessage.addAttachment(qbAttachment);
            }

            if (stickerImage != null) {
                chatMessage.setProperty(Constants.KEY_STICKER_IMAGE, stickerImage);
                // for a convienience to IOS sending sticker as body
                chatMessage.setBody(stickerImage);
            } else {
                chatMessage.setBody(msg);
            }

            chatMessage.setSaveToHistory(true);
            chatMessage.setSenderId(senderId);
            chatMessage.setDateSent(System.currentTimeMillis() / 1000);
            qbChatDialogGroup.sendMessage(chatMessage);
        } catch (SmackException.NotConnectedException e) {
            e.getLocalizedMessage();
        }
    }

    public void getImageUrlFromFileId(int fileId, QBEntityCallback<QBFile> callback) {
        QBContent.getFile(fileId).performAsync(callback);
    }

    public void uploadUserProfileImage(File mFile, QBUser user) {

        File file = mFile;


        QBContent.uploadFileTask(file, true, "").performAsync(new QBEntityCallback<QBFile>() {
            @Override
            public void onSuccess(QBFile qbFile, Bundle bundle) {
                String publicUrl = qbFile.getPublicUrl();


                int id = qbFile.getId();
                user.setFileId(id);
                try {
                    QBUsers.updateUser(user).perform();
                } catch (QBResponseException e) {
                    e.printStackTrace();
                }

                //Here fileid is blobid in android.
                // QBChatService.getInstance().getUser().setFileId(id);


            }

            @Override
            public void onError(QBResponseException e) {

            }
        });

    }

    public void uploadDialogPhoto(File file, final QBEntityCallback<QBFile> callback) {
        QBContent.uploadFileTask(file, true, "").performAsync(callback);

    }



    public void join(QBChatDialog chatDialog, final QBEntityCallback<Void> callback) {
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);

        try {
            chatDialog.join(history, callback);
        } catch (NullPointerException e) {

        }
    }

    private QBChatMessage buildSystemMessageAboutCreatingGroupDialog(QBChatDialog dialog) {

        QBChatMessage qbChatMessage = new QBChatMessage();
        qbChatMessage.setDialogId(dialog.getDialogId());
        qbChatMessage.setProperty(PROPERTY_OCCUPANTS_IDS, getOccupantsIdsStringFromList(dialog.getOccupants()));
        // use 1 in case if dont work below.
        qbChatMessage.setProperty(PROPERTY_DIALOG_TYPE, String.valueOf(dialog.getType().getCode()));
        qbChatMessage.setProperty(PROPERTY_DIALOG_NAME, String.valueOf(dialog.getName()));
        qbChatMessage.setProperty(PROPERTY_ROOMJID, dialog.getRoomJid());
        qbChatMessage.setProperty(PROPERTY_NOTIFICATION_TYPE, "1");
        qbChatMessage.setProperty(PROPERTY_TYPE_LAST_MESSAGE_DATE, dialog.getCreatedAt().toString());


        return qbChatMessage;
    }

    private String getOccupantsIdsStringFromList(List<Integer> occupants) {
        StringBuilder stringBuilder = new StringBuilder();


        for (int i = 0; i < occupants.size(); i++) {
            stringBuilder.append(String.valueOf(occupants.get(i)));

            if (i != occupants.size() - 1) {
                stringBuilder.append(",");
            }

        }
        return stringBuilder.toString();
    }


    public void sendSystemMessageAboutCreatingDialog(QBSystemMessagesManager systemMessagesManager, QBChatDialog dialog) {
        QBChatMessage systemMessageCreatingDialog = buildSystemMessageAboutCreatingGroupDialog(dialog);

        for (Integer recipientId : dialog.getOccupants()) {
            if (!recipientId.equals(QBChatService.getInstance().getUser().getId())) {
                systemMessageCreatingDialog.setRecipientId(recipientId);
                try {
                    systemMessagesManager.sendSystemMessage(systemMessageCreatingDialog);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public interface OnResponse {
        void onData(int id, String msg);
    }



    public interface OnUserWithMessageResponse {
        void onData(ArrayList<ChatModel> chatModels);
    }

}
