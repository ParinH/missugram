package wg.com.missugram.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import wg.com.missugram.R;
import wg.com.missugram.activities.LoginActivity;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.fragments.DescriptionFragment;
import wg.com.missugram.model.MemorialVo;


public class CommonUtills {

    private static ProgressDialog mProgrsessDialog;

    public static void clearAllNotification(Context pContext)

    {
        NotificationManager notifManager = (NotificationManager) pContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static int checkLength(String value) {

        return (int) value.length();

    }

    public static boolean checkPassWordAndConfirmPassword(String password, String confirmPassword) {
        boolean pstatus = false;
        if (confirmPassword != null && password != null) {
            if (password.equals(confirmPassword)) {
                pstatus = true;
            }
        }

        return pstatus;
    }

    public static void showAlert(final Context p_Context, final String p_message, final int action) {
        showAlert(p_Context, p_message, action, null);

    }

    public static void showAlert(final Context p_Context, final String p_message, final int action, final MemorialVo memorialVo) {
        AlertDialog alertDialog = new AlertDialog.Builder(p_Context).create();
        alertDialog.setTitle(p_Context.getResources().getString(R.string.app_name));
        alertDialog.setMessage(p_message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (action) {
                            case Constants.ACTION_SIGN_IN:
                                FragmentController.addLoginFragment(((LoginActivity) p_Context));
                                break;
                            case Constants.ACTION_REGISTER:
                                FragmentController.addSignUpFragment(((LoginActivity) p_Context), Constants.FROM_OTHER);
                                break;
                            case Constants.NO_ACTION:
                                dialog.dismiss();
                                break;
                            case Constants.ACTION_MAIN:
                                dialog.dismiss();
                                p_Context.startActivity(new Intent(p_Context, TestMainActivity.class));
                                ((Activity) p_Context).finish();
                                break;
                            case Constants.ACTION_FINISH:
                                ((Activity) p_Context).finish();
                                break;

                            case Constants.ACTION_SETTINGS:
                                FragmentController.addSettingsFragment((FragmentActivity) p_Context, FragmentController.TAG_SETTINGS_FRAGMENT);
                                break;
                            case Constants.ACTION_FRAGMENT_FINISH:
                                ((TestMainActivity) p_Context).onBackPressed();
                                break;
                            case Constants.ACTION_FRAGMENT_CREATE_MYMEMORIAL:
                                FragmentController.addMemorialFragment((FragmentActivity) p_Context, ((TestMainActivity) p_Context).getCurrentMemorialVal());
                                break;
                            case Constants.ACTION_LOGIN_FROM_SETTINGS:
                                FragmentController.removeAllFragmentsFromStack((FragmentActivity) p_Context);
                                p_Context.startActivity(new Intent(p_Context, LoginActivity.class));
                                ((Activity) p_Context).finish();
                                break;
                            case Constants.ACTION_FRAGMENT_DESCRIPTION:
                                FragmentController.removeFragment((FragmentActivity) p_Context, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                                // we are getting a updated vo here.

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (FragmentController.isThisFragmentInFront((FragmentActivity) p_Context, FragmentController.TAG_DESCRIPTION_FRAGMENT) instanceof DescriptionFragment) {
                                            DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront((FragmentActivity) p_Context, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                                            descriptionFragment.setmMemoriamVo(memorialVo);
                                            descriptionFragment.setData();

                                        }
                                    }
                                }, 200);

                                break;

                        }
                        //dialog.dismiss();
                    }
                });
        alertDialog.show();

    }

    /**
     * This method set options for making image in round shape.
     *
     * @return display option  with round image values
     */
    public static DisplayImageOptions getRoundImageOptions() {
        return new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(360))
                .showImageOnLoading(android.R.color.transparent)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.ic_userplus)
                .showImageForEmptyUri(R.drawable.ic_userplus)
                .build();


    }
    /**
     * This method set options for making image in for only memorial listing and for description.
     *
     * @return display option  with round image values
     */
    public static DisplayImageOptions getRoundImageOptionsForMemorial() {
        return new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(360))
                .showImageOnLoading(android.R.color.transparent)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.ic_butt_drawer)
                .showImageForEmptyUri(R.drawable.ic_butt_drawer)
                .build();


    }
    /**
     * This method set options for making image in round shape.
     *
     * @return display option  with round image values
     */
    public static DisplayImageOptions getRoundImageOptionsForMenuProfile() {
        return new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(360))
                .showImageOnLoading(android.R.color.transparent)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.missugram_icon)
                .showImageForEmptyUri(R.drawable.missugram_icon)
                .build();


    }

    /**
     * This method set options for making image in round shape.
     *
     * @return display option  with round image values
     */
    public static DisplayImageOptions getRoundImageOptionsForGroup() {
        return new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(360))
                .showImageOnLoading(android.R.color.transparent)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.group)
                .showImageForEmptyUri(R.drawable.group)
                .build();


    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void setPrefFirstName(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_FIRST_NAME, prefValue).apply();

    }

    public static void setPrefPrivateDialogId(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_PRIVATE_CHAT_DIALOG_ID, prefValue).apply();

    }


    public static void setPrefLastName(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_LAST_NAME, prefValue).apply();

    }

    public static void setPrefUserAvatar(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_USER_AVATAR, prefValue).apply();

    }

    public static void setPrefUserPassword(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_USER_PASSWORD, prefValue).apply();

    }

    public static void setPrefUserEmail(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_USER_EMAIL, prefValue).apply();

    }

    public static void setPrefToken(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_TOKEN, prefValue).apply();

    }

    public static void setPrefFirebaseDeviceToken(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_FIREBASE_DEVICE_TOKEN, prefValue).apply();

    }

    public static void setPrefIsFromNotification(Context pContext, boolean prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putBoolean(Constants.PREF_FIREBASE_FROM_NOTIFICATION, prefValue).apply();

    }

    public static void setPrefIsNotificationEnabled(Context pContext, boolean prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putBoolean(Constants.PREF_NOTIFICATION_ON, prefValue).apply();

    }

    public static void setPrefId(Context pContext, int prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putInt(Constants.PREF_USER_ID, prefValue).apply();

    }

    public static void setPrefCurrentChatUname(Context pContext, String prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putString(Constants.PREF_PREF_CURRENT_CHAT_UNAME, prefValue).apply();

    }


    public static String getPrefCurrentChatUname(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_PREF_CURRENT_CHAT_UNAME, "");

    }

    public static String getPrefCurrentChatPwd(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_PREF_CURRENT_CHAT_USER_PWD, "");

    }

    public static int getPrefId(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getInt(Constants.PREF_USER_ID, 0);

    }

    public static void setPrefChatId(Context pContext, int prefValue) {
        pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().putInt(Constants.PREF_CHAT_ID, prefValue).apply();

    }


    public static int getPrefChatId(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getInt(Constants.PREF_CHAT_ID, 0);

    }

    public static String getPrefFirstName(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_FIRST_NAME, "");

    }

    public static String getPrefUserAvatar(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_USER_AVATAR, "");

    }

    public static String getPrefLastName(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_LAST_NAME, "");

    }

    public static String getPrefUserPassword(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_USER_PASSWORD, "");

    }

    public static String getPrefUserEmail(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_USER_EMAIL, "");

    }

    public static String getPrefToken(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_TOKEN, "");

    }

    public static String getPrefFirebaseDeviceToken(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getString(Constants.PREF_FIREBASE_DEVICE_TOKEN, "token");
    }

    public static boolean getPrefisFromNotification(Context pContext) {
        return pContext.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getBoolean(Constants.PREF_FIREBASE_FROM_NOTIFICATION, false);
    }

    public static boolean getPrefIsNotificationEnabled(Context pContex) {
        return pContex.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).getBoolean(Constants.PREF_NOTIFICATION_ON, false);

    }

    public static String getRealPathFromURI(final Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void showProgressDialog(Context pContext, String pMessage, boolean isCancelable) {

        try {
            if (mProgrsessDialog != null && mProgrsessDialog.isShowing()) {
                mProgrsessDialog.dismiss();
            }
            mProgrsessDialog = new ProgressDialog(pContext);
            mProgrsessDialog.setTitle(pContext.getResources().getString(R.string.app_name));
            mProgrsessDialog.setMessage(pContext.getResources().getString(R.string.str_pleasewait));
            mProgrsessDialog.setCancelable(false);
            mProgrsessDialog.setCanceledOnTouchOutside(false);
            mProgrsessDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (!((Activity) pContext).isFinishing())
//            mProgrsessDialog = ProgressDialog.show(pContext, pContext.getString(R.string.app_name), pMessage, isCancelable);
    }

    public static void dismissProgressDialog() {

        try {
            if (mProgrsessDialog != null && mProgrsessDialog.isShowing()) {
                mProgrsessDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (mProgrsessDialog != null && mProgrsessDialog.isShowing()) {
//            mProgrsessDialog.dismiss();
//        }
    }

    public static boolean isNetworkAvailable(Context pContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean hasStickerImage(String text) {
        boolean hasImageName = false;
        switch (text) {
            case "sticker1":
                hasImageName = true;
                break;
            case "sticker2":
                hasImageName = true;
                break;
            case "sticker3":
                hasImageName = true;
                break;
            case "sticker4":
                hasImageName = true;
                break;
            case "sticker5":
                hasImageName = true;
                break;
            case "sticker6":
                hasImageName = true;
                break;
            case "sticker7":
                hasImageName = true;
                break;
            case "sticker8":
                hasImageName = true;
                break;
            case "sticker9":
                hasImageName = true;
                break;
            case "sticker10":
                hasImageName = true;
                break;
            case "sticker11":
                hasImageName = true;
                break;
            case "sticker12":
                hasImageName = true;
                break;
            case "sticker13":
                hasImageName = true;
                break;
            case "sticker14":
                hasImageName = true;
                break;
            case "sticker15":
                hasImageName = true;
                break;
            case "sticker16":
                hasImageName = true;
                break;
            case "sticker17":
                hasImageName = true;
                break;
            case "sticker18":
                hasImageName = true;
                break;
            case "sticker19":
                hasImageName = true;
                break;
            case "sticker20":
                hasImageName = true;
                break;
            case "sticker21":
                hasImageName = true;
                break;

            case "sticker22":
                hasImageName = true;
                break;
            case "sticker23":
                hasImageName = true;
                break;
        }
        return hasImageName;
    }

    public static Bitmap loadImageFromStorage(String path) {
        Bitmap b = null;
        try {
            File f = new File(path, "profile.jpg");
            b = BitmapFactory.decodeStream(new FileInputStream(f));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static String getVersionNumber(Context pContext) {
        if (pContext == null)
            return null;

        PackageInfo pinfo = null;
        try {
            pinfo = pContext.getPackageManager().getPackageInfo("com.miss_u_gram", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
       if (pinfo==null)
           return  null;

        return pinfo.versionName;
    }
}