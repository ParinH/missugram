package wg.com.missugram.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wg.framework.log.CustomLogHandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;


public class StickerImageAdapter extends RecyclerView.Adapter<StickerImageAdapter.itemViewHolder> {
    private ArrayList<String> alSticketImagePath;
    private Context mContext;
    private ItemClickListener mClickListener;

    public StickerImageAdapter(ArrayList<String> alSticketImagePath, Context pContext) {
        this.alSticketImagePath = alSticketImagePath;
        mContext = pContext;
    }

    @Override
    public itemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemViewHolder itemViewHolder = null;
        View layout_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_commentlisting_stickerimage, parent, false);
        itemViewHolder = new itemViewHolder(layout_view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(itemViewHolder holder, int position) {
        final itemViewHolder mItemViewHolder = holder;
        try {
            InputStream is = mContext.getAssets().open(Constants.ASSEST_IMAGE_FOLDERNAME + File.separator + alSticketImagePath.get(position));

            Bitmap bitmap = BitmapFactory.decodeStream(is);
            mItemViewHolder.rowIvSticker.setImageBitmap(bitmap);
            mItemViewHolder.rowIvSticker.setTag(alSticketImagePath.get(position));

            mItemViewHolder.rowIvSticker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onItemClick((String) v.getTag());
                }
            });
        } catch (IOException e) {
            CustomLogHandler.printErrorlog(e);
        }
    }


    @Override
    public int getItemCount() {
        return alSticketImagePath.size();
    }


    public class itemViewHolder extends RecyclerView.ViewHolder {
        ImageView rowIvSticker;

        public itemViewHolder(View itemView) {
            super(itemView);
            rowIvSticker = (ImageView) itemView.findViewById(R.id.row_comment_iv_sticker);


        }


    }

    public interface ItemClickListener {
        void onItemClick(String tag);
    }

    public void setOnRecyclerItemListener(StickerImageAdapter.ItemClickListener _onItemClickListener) {
        mClickListener = _onItemClickListener;
    }
}