package wg.com.missugram.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatUserHistoryModel;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;


public class ChatUserListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    OnRecyclerItemClickListener onRecyclerItemClickListener;
    private Context mContext;
    private ArrayList<ChatUserHistoryModel> mChatUserHistoryModels;
    private ArrayList<ChatUserHistoryModel> temopVoListChat = null;

    public ChatUserListingAdapter(Context mContext, ArrayList<ChatUserHistoryModel> userHistoryModels) {
        this.mContext = mContext;
        mChatUserHistoryModels = userHistoryModels;
        this.temopVoListChat = new ArrayList<ChatUserHistoryModel>();
        this.temopVoListChat.addAll(mChatUserHistoryModels);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_layout_chathistory, parent, false);
        HistoryUserHolder historyUserHolder = new HistoryUserHolder(v);
        return historyUserHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final HistoryUserHolder mHistoryUserHolder = (HistoryUserHolder) holder;
        ChatUserHistoryModel chatUserHistoryModel = mChatUserHistoryModels.get(position);


        if (chatUserHistoryModel.getType() == QBDialogType.PRIVATE) {

            mHistoryUserHolder.mrow_mchatuser_tv_username.setText(chatUserHistoryModel.getChatuser_name());

            if (!TextUtils.isEmpty(chatUserHistoryModel.getChatuser_status()) && CommonUtills.hasStickerImage(chatUserHistoryModel.getChatuser_status())) {
                mHistoryUserHolder.mimage_sticker_image.setVisibility(View.VISIBLE);
                mHistoryUserHolder.mrow_mchatuser_tv_status.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Constants.PATH_ASSEST_IMAGE_FOLDERNAME + chatUserHistoryModel.getChatuser_status().trim() + ".png", mHistoryUserHolder.mimage_sticker_image);

            } else {
                mHistoryUserHolder.mimage_sticker_image.setVisibility(View.GONE);
                mHistoryUserHolder.mrow_mchatuser_tv_status.setVisibility(View.VISIBLE);

                mHistoryUserHolder.mrow_mchatuser_tv_status.setText(chatUserHistoryModel.getChatuser_status());
            }


            //add the data for message count badge
            if (chatUserHistoryModel.getUnread_messagecount() == 0) {
                mHistoryUserHolder.mbadge_notification_unread_message.setVisibility(View.GONE);

            } else {
                mHistoryUserHolder.mbadge_notification_unread_message.setVisibility(View.VISIBLE);

                mHistoryUserHolder.mbadge_notification_unread_message.setText(String.valueOf(chatUserHistoryModel.getUnread_messagecount()));
            }


            // load the user photo from fileid from quickblox.
            if (chatUserHistoryModel.getChatuser_fileid() != -1) {

                ChatProvider.getInstance().getImageUrlFromFileId(chatUserHistoryModel.getChatuser_fileid(), new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {

                        ImageLoader.getInstance().displayImage(qbFile.getPublicUrl(), mHistoryUserHolder.mrow_mchatuser_iv_photo, CommonUtills.getRoundImageOptions());


                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });


            } else {
                mHistoryUserHolder.mrow_mchatuser_iv_photo.setImageResource(R.drawable.ic_userplus);
            }


        } else if (chatUserHistoryModel.getType() == QBDialogType.GROUP || chatUserHistoryModel.getType() == QBDialogType.PUBLIC_GROUP) {
            mHistoryUserHolder.mrow_mchatuser_tv_username.setText(chatUserHistoryModel.getChatDialog().getName());


            if (!TextUtils.isEmpty(chatUserHistoryModel.getChatuser_status()) && CommonUtills.hasStickerImage(chatUserHistoryModel.getChatuser_status())) {
                mHistoryUserHolder.mimage_sticker_image.setVisibility(View.VISIBLE);
                mHistoryUserHolder.mrow_mchatuser_tv_status.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Constants.PATH_ASSEST_IMAGE_FOLDERNAME + chatUserHistoryModel.getChatuser_status().trim() + ".png", mHistoryUserHolder.mimage_sticker_image);

            } else {
                mHistoryUserHolder.mimage_sticker_image.setVisibility(View.GONE);
                mHistoryUserHolder.mrow_mchatuser_tv_status.setVisibility(View.VISIBLE);

                mHistoryUserHolder.mrow_mchatuser_tv_status.setText(chatUserHistoryModel.getChatuser_status());
            }


            if (chatUserHistoryModel.getUnread_messagecount() == 0) {
                mHistoryUserHolder.mbadge_notification_unread_message.setVisibility(View.GONE);

            } else {
                mHistoryUserHolder.mbadge_notification_unread_message.setVisibility(View.VISIBLE);

                mHistoryUserHolder.mbadge_notification_unread_message.setText(String.valueOf(chatUserHistoryModel.getUnread_messagecount()));

            }

            if (chatUserHistoryModel.getChatDialog().getPhoto() != null && !chatUserHistoryModel.getChatDialog().getPhoto().equalsIgnoreCase("null")) {

                ChatProvider.getInstance().getImageUrlFromFileId(Integer.valueOf(chatUserHistoryModel.getChatDialog().getPhoto()), new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {
                        DisplayImageOptions displayImageOptions = CommonUtills.getRoundImageOptions();

                        ImageLoader.getInstance().displayImage(qbFile.getPublicUrl(), mHistoryUserHolder.mrow_mchatuser_iv_photo, CommonUtills.getRoundImageOptionsForGroup());


                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });


            } else {
                mHistoryUserHolder.mrow_mchatuser_iv_photo.setImageResource(R.drawable.group);
            }
        }


        mHistoryUserHolder.mrl_main_chat.setOnClickListener(v -> {
            if (chatUserHistoryModel.getType() == QBDialogType.PRIVATE) {
                onRecyclerItemClickListener.onViewItemClicked(chatUserHistoryModel.getChatuser_id(), QBDialogType.PRIVATE, "", chatUserHistoryModel.getChatuser_name(), String.valueOf(chatUserHistoryModel.getChatuser_fileid()));
            } else if (chatUserHistoryModel.getType() == QBDialogType.GROUP) {
                onRecyclerItemClickListener.onViewItemClicked(0, QBDialogType.GROUP, chatUserHistoryModel.getChatDialog().getDialogId(), chatUserHistoryModel.getChatDialog().getName(), chatUserHistoryModel.getChatDialog().getPhoto());
            }
        });
    }

    public void addChatGroupNewEntry(ChatUserHistoryModel pHistoryModel) {
        boolean isFound = false;
        for (ChatUserHistoryModel historyModel : mChatUserHistoryModels) {
            if (historyModel.getChatDialog().getDialogId().equalsIgnoreCase(pHistoryModel.getChatDialog().getDialogId())) {
                mChatUserHistoryModels.remove(historyModel);
                mChatUserHistoryModels.add(pHistoryModel);
                isFound = true;
                break;
            }
        }
        if (isFound) {
            Collections.sort(mChatUserHistoryModels, (o1, o2) -> {

                long date1 = o1.getChatDialog().getLastMessageDateSent();
                long date2 = o2.getChatDialog().getLastMessageDateSent();
                return new Date(date2).compareTo(new Date(date1));

            });
            notifyDataSetChanged();
        } else {

            mChatUserHistoryModels.add(pHistoryModel);
            notifyItemInserted(mChatUserHistoryModels.size());
        }
    }

    public void updateHistoryChat(ChatUserHistoryModel p_HistoryModels) {
        QBDialogType qbDialogType = p_HistoryModels.getType();

        if (qbDialogType == QBDialogType.PRIVATE) {
            for (ChatUserHistoryModel historyModel : mChatUserHistoryModels) {
                if (historyModel.getChatuser_id() == p_HistoryModels.getChatuser_id()) {
                    mChatUserHistoryModels.remove(historyModel);
                    mChatUserHistoryModels.add(p_HistoryModels);

                    break;
                }
            }
        } else {
            for (ChatUserHistoryModel historyModel : mChatUserHistoryModels) {
                if (historyModel.getChatDialog()!=null&&historyModel.getChatDialog()!=null&&historyModel.getChatDialog().getDialogId().equalsIgnoreCase(p_HistoryModels.getChatDialog()==null?"":p_HistoryModels.getChatDialog().getDialogId())) {
                    mChatUserHistoryModels.remove(historyModel);
                    mChatUserHistoryModels.add(p_HistoryModels);

                    break;
                }
            }
        }


        Collections.sort(mChatUserHistoryModels, (o1, o2) -> {
            if (qbDialogType == QBDialogType.PRIVATE) {
                long date1 = o1.getLastMessageDateSentPrivateChat();
                long date2 = o2.getLastMessageDateSentPrivateChat();

                return new Date(date1).compareTo(new Date(date2));
            } else {
                long date1 = o1.getChatDialog().getLastMessageDateSent();
                long date2 = o2.getChatDialog().getLastMessageDateSent();
                return new Date(date2).compareTo(new Date(date1));


            }
        });

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mChatUserHistoryModels.size();
    }

    public void setOnRecyclerItemListener(OnRecyclerItemClickListener _onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = _onRecyclerItemClickListener;
    }

    public void filter(String charText, QBDialogType type) {
        charText = charText.toLowerCase(Locale.getDefault());

        if (mChatUserHistoryModels != null) {

            mChatUserHistoryModels.clear();
            if (charText.length() == 0) {
                mChatUserHistoryModels.addAll(temopVoListChat);


            } else {
                for (ChatUserHistoryModel wp : temopVoListChat) {
                    if (type == QBDialogType.PRIVATE) {
                        if (wp.getChatuser_name().toLowerCase(Locale.getDefault()).contains(charText)) {

                            mChatUserHistoryModels.add(wp);
                        }
                    } else if (type == QBDialogType.GROUP) {
                        if (wp.getChatDialog().getName().toLowerCase(Locale.getDefault()).contains(charText)) {

                            mChatUserHistoryModels.add(wp);
                        }
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public interface OnRecyclerItemClickListener {

        void onViewItemClicked(int id, QBDialogType type, String dialogId, String name, String url);

    }

    public class HistoryUserHolder extends RecyclerView.ViewHolder {
        ImageView mrow_mchatuser_iv_photo, mimage_sticker_image;
        TextView mrow_mchatuser_tv_username, mbadge_notification_unread_message;
        TextView mrow_mchatuser_tv_status;
        RelativeLayout mrl_main_chat;

        HistoryUserHolder(View itemView) {
            super(itemView);

            mrow_mchatuser_iv_photo = (ImageView) itemView.findViewById(R.id.row_mchatuser_iv_photo);
            mimage_sticker_image = (ImageView) itemView.findViewById(R.id.image_sticker_image);
            mrow_mchatuser_tv_username = (TextView) itemView.findViewById(R.id.row_mchatuser_tv_username);
            mrow_mchatuser_tv_status = (TextView) itemView.findViewById(R.id.row_mchatuser_tv_status);
            mrl_main_chat = (RelativeLayout) itemView.findViewById(R.id.rl_main_chat);
            mbadge_notification_unread_message = (TextView) itemView.findViewById(R.id.badge_notification_unread_message);
            itemView.setTag(this);

        }
    }

}
