package wg.com.missugram.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

import java.util.ArrayList;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatModel;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;


public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private Context mContext;
    private ArrayList<ChatModel> actualChatList;
    private ArrayList<ChatModel> temopVoList = null;
    private int fromWhere;

    public ChatAdapter(Context context, ArrayList<ChatModel> contactVoList, int fromWhere) {
        this.mContext = context;
        this.actualChatList = contactVoList;
        this.fromWhere = fromWhere;
        this.temopVoList = new ArrayList<>();
        this.temopVoList.addAll(contactVoList);

    }


    public void setChatMessage(ArrayList<ChatModel> p_ChatMessage) {
        actualChatList.addAll(0, p_ChatMessage);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);

            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        } else {
            return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel model = (ChatModel) actualChatList.get(position);
        if (model.getQbChatMessage().getSenderId() == CommonUtills.getPrefChatId(mContext)) {
            // add to the right side
            return VIEW_TYPE_MESSAGE_SENT;

        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
            // add to the left side indicates a receive a chat message
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateChatList(ChatModel chatModel) {
        actualChatList.add(chatModel);
        notifyItemInserted(actualChatList.size());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ChatModel message = (ChatModel) actualChatList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }

    }

    @Override
    public int getItemCount() {
        return (null != actualChatList ? actualChatList.size() : 0);
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView mtext_sent_message_body;
        ImageView mimage_sent_message_profile, mimage_user_profile;

        SentMessageHolder(View itemView) {
            super(itemView);

            mtext_sent_message_body = (TextView) itemView.findViewById(R.id.text_sent_message_body);
            mimage_sent_message_profile = (ImageView) itemView.findViewById(R.id.image_sent_message_profile);

        }

        void bind(ChatModel message) {

            String stickerImage = message.getQbChatMessage().getProperty(Constants.KEY_STICKER_IMAGE) != null ? (String) message.getQbChatMessage().getProperty(Constants.KEY_STICKER_IMAGE) : "";

            if (!TextUtils.isEmpty(stickerImage)) {
                mimage_sent_message_profile.setVisibility(View.VISIBLE);
                mtext_sent_message_body.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Constants.PATH_ASSEST_IMAGE_FOLDERNAME + stickerImage + ".png", mimage_sent_message_profile);

            } else {
                mimage_sent_message_profile.setVisibility(View.GONE);
                mtext_sent_message_body.setVisibility(View.VISIBLE);
                String sentMessage = message.getQbChatMessage().getBody();
                if (sentMessage != null && !sentMessage.isEmpty() && !sentMessage.equalsIgnoreCase(Constants.KEY_MESSAGE_DEFAULT))
                    mtext_sent_message_body.setText(sentMessage);


            }


        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView mReceivedMessage, mNameText;
        ImageView mReceivedUserProfileImage, mImage_Received;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            mReceivedMessage = (TextView) itemView.findViewById(R.id.text_received_message_body);
            mNameText = (TextView) itemView.findViewById(R.id.text_received_message_name);
            mReceivedUserProfileImage = (ImageView) itemView.findViewById(R.id.image_received_user_profile);
            mImage_Received = (ImageView) itemView.findViewById(R.id.image_received_image);
        }

        void bind(ChatModel message) {

            String stickerImage = message.getQbChatMessage().getProperty(Constants.KEY_STICKER_IMAGE) != null ? (String) message.getQbChatMessage().getProperty(Constants.KEY_STICKER_IMAGE) : "";

            if (!TextUtils.isEmpty(stickerImage)) {
                mImage_Received.setVisibility(View.VISIBLE);
                mReceivedMessage.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(Constants.PATH_ASSEST_IMAGE_FOLDERNAME + stickerImage + ".png", mImage_Received);

            } else {
                mImage_Received.setVisibility(View.GONE);
                mReceivedMessage.setVisibility(View.VISIBLE);


                String recentMessage = message.getQbChatMessage().getBody();
                if (recentMessage != null && !recentMessage.isEmpty() && !recentMessage.equalsIgnoreCase(Constants.KEY_MESSAGE_DEFAULT))
                    mReceivedMessage.setText(recentMessage);

            }


            if (message.getFileId()!=-1) {
                ChatProvider.getInstance().getImageUrlFromFileId(message.getFileId(), new QBEntityCallback<QBFile>() {
                    @Override
                    public void onSuccess(QBFile qbFile, Bundle bundle) {
                        ImageLoader.getInstance().displayImage(qbFile.getPublicUrl(), mReceivedUserProfileImage, fromWhere==Constants.FOR_GROUP_CHAT?CommonUtills.getRoundImageOptionsForGroup():CommonUtills.getRoundImageOptions());

                    }

                    @Override
                    public void onError(QBResponseException e) {
                        e.getLocalizedMessage();
                    }
                });
            }
            else
            {
              if (fromWhere==Constants.FOR_GROUP_CHAT)
              {
                  mReceivedUserProfileImage.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.group));
              }
              else
              {
                  mReceivedUserProfileImage.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.ic_userplus));
              }
            }

            if (!TextUtils.isEmpty(message.getname()))
            {
                mNameText.setVisibility(View.VISIBLE);
                mNameText.setText(message.getname());
            }
            else
            {
                mNameText.setVisibility(View.GONE);
            }



        }
    }
}
