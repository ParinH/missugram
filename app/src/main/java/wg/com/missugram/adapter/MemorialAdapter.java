package wg.com.missugram.adapter;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.utils.CommonUtills;


public class MemorialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnRecyclerItemClickListener onRecyclerItemClickListener;
    private Context mContext;
    ;
    private ArrayList<MemorialVo> memorialVos = new ArrayList<>();
    private List<MemorialVo> filterMemorialVos;

    public MemorialAdapter(Context context, ArrayList<MemorialVo> mAlFaultCodesMainVos) {
        this.mContext = context;
        this.filterMemorialVos = mAlFaultCodesMainVos;
        if (mAlFaultCodesMainVos != null)
            this.memorialVos.addAll(mAlFaultCodesMainVos);
    }

    public void setOnRecyclerItemListener(OnRecyclerItemClickListener _onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = _onRecyclerItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder rcv = null;
        View layout_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.memorial_raw_layout, parent, false);
        rcv = new ItemViewHolder(layout_view);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        final MemorialVo memorialVo = filterMemorialVos.get(position);
        itemViewHolder.mrlTvTitle.setText(memorialVo.getTitle());
        itemViewHolder.mrlTvOwner.setText(memorialVo.getUserInfo() == null ? "" : memorialVo.getUserInfo().getFirst_name() + " " + memorialVo.getUserInfo().getLast_name());
        itemViewHolder.mrlTvOwner.setVisibility(memorialVo.getUserInfo() == null ? View.GONE : View.VISIBLE);
        ImageLoader.getInstance().displayImage(memorialVo.getImage(), itemViewHolder.mrlIvIcon, CommonUtills.getRoundImageOptionsForMemorial());
        itemViewHolder.content_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onRecyclerItemClickListener.onViewItemClicked(memorialVo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != filterMemorialVos ? filterMemorialVos.size() : 0);
    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            if (filterMemorialVos != null) {
                filterMemorialVos.clear();
                if (charText.length() == 0) {
                    filterMemorialVos.addAll(memorialVos);
                } else {
                    for (MemorialVo wp : memorialVos) {
                        if (wp.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                            filterMemorialVos.add(wp);
                        }
                    }

                }
                notifyDataSetChanged();
            }
        } catch (Exception e) {
        }
    }

    public void updateMemorialVo(ArrayList<MemorialVo> memorialVoArrayList) {
        memorialVos.addAll(memorialVoArrayList);
    }

    public interface OnRecyclerItemClickListener {

        void onViewItemClicked(MemorialVo memorialVo);

    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView mrlIvIcon;
        private TextView mrlTvTitle;
        private TextView mrlTvOwner;
        private CardView content_main;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mrlIvIcon = (ImageView) itemView.findViewById(R.id.mrl_iv_icon);
            mrlTvTitle = (TextView) itemView.findViewById(R.id.mrl_tv_title);
            mrlTvOwner = (TextView) itemView.findViewById(R.id.mrl_tv_owner);
            content_main = (CardView) itemView.findViewById(R.id.content_main);
        }
    }
}
