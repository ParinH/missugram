package wg.com.missugram.adapter;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Locale;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.model.UserModel;


public class FriendListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnRecyclerItemClickListener onRecyclerItemClickListener;
    OnRecyclerItemClickListenerForId onRecyclerItemClickListenerId;
    private Context mContext;
    private ArrayList<UserModel> actualContactVoList;
    private ArrayList<UserModel> temopVoList = null;
    private ArrayList<Integer> quickBloxIdList = null;
    private int fromWhere;

    public FriendListAdapter(Context context, ArrayList<UserModel> contactVoList, int fromWhere) {
        this.mContext = context;
        this.actualContactVoList = contactVoList;
        this.fromWhere = fromWhere;
        this.temopVoList = new ArrayList<UserModel>();
        this.temopVoList.addAll(contactVoList);
        quickBloxIdList = new ArrayList<>();

    }

    public void setOnRecyclerItemListener(OnRecyclerItemClickListener _onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = _onRecyclerItemClickListener;
    }

    public void setOnRecyclerItemListenerForId(OnRecyclerItemClickListenerForId _onRecyclerItemClickListener) {
        this.onRecyclerItemClickListenerId = _onRecyclerItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder rcv = null;
        View layout_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_firend_list, parent, false);
        rcv = new ItemViewHolder(layout_view);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        final UserModel contactVo = actualContactVoList.get(position);
        itemViewHolder.mTvContactName.setText(String.format("%s%s", contactVo.getFirst_name(), contactVo.getLast_name()));
        itemViewHolder.mTvUniqueVal.setText(contactVo.getEmail());
        ImageLoader.getInstance().displayImage(contactVo.getAvatar(), itemViewHolder.mIVUser);
        itemViewHolder.mCbUserSelected.setVisibility(fromWhere == Constants.FRIEND_LIST_FROM_FROM_MAIN || fromWhere == Constants.CHAT ? View.GONE : View.VISIBLE);
        itemViewHolder.mTvContactName.setTextColor(fromWhere == Constants.CHAT ? Color.parseColor("#000000") : Color.parseColor("#626262"));

        itemViewHolder.mCbUserSelected.setOnCheckedChangeListener((compoundButton, b) -> {
            contactVo.setChecked(b);
            //fROM CREATE GROUP
            if (fromWhere == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                if (contactVo.isChecked()) {
                    quickBloxIdList.add(Integer.valueOf(contactVo.getQuickblox_account_id()));
                } else {
                    quickBloxIdList.remove(Integer.valueOf(contactVo.getQuickblox_account_id()));
                }
            }
        });
        itemViewHolder.mCbUserSelected.setChecked(contactVo.isChecked());
//FROM GRIEVANCE CHAT
        if (fromWhere == Constants.CHAT) {
            itemViewHolder.mrl_main.setOnClickListener(v -> onRecyclerItemClickListenerId.onViewItemClicked(Integer.valueOf(contactVo.getQuickblox_account_id())));
        }
    }

    @Override
    public int getItemCount() {
        return (null != actualContactVoList ? actualContactVoList.size() : 0);
    }

    public ArrayList<Integer> getCheckedUserIDForChat() {

        return quickBloxIdList;
    }

    public ArrayList<UserModel> getCheckedContacts() {
        return actualContactVoList;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        final String a = charText;
        if (actualContactVoList != null) {

            actualContactVoList.clear();
            if (charText.length() == 0) {
                actualContactVoList.addAll(temopVoList);


            } else {
                for (UserModel wp : temopVoList) {
                    if (wp.getFirst_name().toLowerCase(Locale.getDefault()).contains(charText)) {

                        actualContactVoList.add(wp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }


    public void updateFriendsVo(ArrayList<UserModel> updatedVoArrayList) {
        temopVoList.addAll(updatedVoArrayList);
    }

    public interface OnRecyclerItemClickListener {
        void onViewItemClicked(MemorialVo memorialVo);
    }

    public interface OnRecyclerItemClickListenerForId {
        void onViewItemClicked(int id);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvContactName;
        private TextView mTvUniqueVal;
        private ImageView mIVUser;
        private CheckBox mCbUserSelected;
        private RelativeLayout mrl_main;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mTvContactName = (TextView) itemView.findViewById(R.id.tv_contact_name);
            mTvUniqueVal = (TextView) itemView.findViewById(R.id.tv_contact_unique);
            mIVUser = (ImageView) itemView.findViewById(R.id.iv_user);
            mCbUserSelected = (CheckBox) itemView.findViewById(R.id.cbUserSelected);
            mrl_main = (RelativeLayout) itemView.findViewById(R.id.rl_main);

        }
    }
}
