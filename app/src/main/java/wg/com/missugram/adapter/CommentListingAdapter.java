package wg.com.missugram.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import wg.com.missugram.R;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommentsVo;
import wg.com.missugram.utils.CommonUtills;


public class CommentListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;


    private ArrayList<CommentsVo> mCommentsVos;
    private boolean isUserCommeted;
    OnRecyclerItemClickListener onRecyclerItemClickListener;
    public CommentListingAdapter(Context mContext, ArrayList<CommentsVo> commentsVos) {
        this.mContext = mContext;
        mCommentsVos = commentsVos;
    }
    public interface OnRecyclerItemClickListener{

        void onViewItemClicked(String id);

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_layout_commentlisting, parent, false);
        commentsHolder commentsHolder = new commentsHolder(v);
        return commentsHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final commentsHolder commentsHolder = (commentsHolder) holder;


        ImageLoader.getInstance().displayImage(mCommentsVos.get(position).getUserInfo().getAvatar(), commentsHolder.mivUserPhoto, CommonUtills.getRoundImageOptions());

        commentsHolder.mtvUserName.setText(mCommentsVos.get(position).getUserInfo().getFirst_name() + " " + mCommentsVos.get(position).getUserInfo().getLast_name());

        //values will be provided from server as discussed by Mr. D.D.
        commentsHolder.mtvUserLastCommented.setText(mCommentsVos.get(position).getCreated_at());


        if (!mCommentsVos.get(position).getComment().isEmpty() && mCommentsVos.get(position).getIs_image() == 1) {

            //IMAGE
            commentsHolder.mivStickerImage.setVisibility(View.VISIBLE);
            commentsHolder.mtvUserCommentDescription.setVisibility(View.GONE);
            commentsHolder.mtvReportAbuse.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage(Constants.PATH_ASSEST_IMAGE_FOLDERNAME + mCommentsVos.get(position).getComment() + ".png", commentsHolder.mivStickerImage);

        } else {

            //TEXT
            commentsHolder.mivStickerImage.setVisibility(View.GONE);
            commentsHolder.mtvUserCommentDescription.setVisibility(View.VISIBLE);
            commentsHolder.mtvUserCommentDescription.setText(mCommentsVos.get(position).getComment());

        }


        if (isUserCommeted || mCommentsVos.get(position).getUserInfo().getId() == MissUGram.getApp(mContext).getUserModel().getId()) {
            commentsHolder.mtvReportAbuse.setVisibility(View.GONE);
        } else {
            commentsHolder.mtvReportAbuse.setVisibility(View.VISIBLE);
        }



        commentsHolder.mtvReportAbuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyclerItemClickListener.onViewItemClicked(mCommentsVos.get(position).getId());
            }
        });
    }

    /**
     * For adding single or stoker image to list.
     *
     * @param p_commentsVo
     * @param is
     */
    public void addComments(CommentsVo p_commentsVo, boolean is) {
        mCommentsVos.add(p_commentsVo);
        notifyItemInserted(mCommentsVos.size() - 1);
        isUserCommeted = is;

    }

    public void setCommentsVo(ArrayList<CommentsVo> p_CommentsVos) {
        mCommentsVos.addAll(0, p_CommentsVos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mCommentsVos.size();
    }

    public class commentsHolder extends RecyclerView.ViewHolder {
        ImageView mivUserPhoto;
        TextView mtvReportAbuse;
        ImageView mivStickerImage;
        TextView mtvUserName;
        TextView mtvUserLastCommented;
        TextView mtvUserCommentDescription;


        commentsHolder(View itemView) {
            super(itemView);

            mivUserPhoto = (ImageView) itemView.findViewById(R.id.row_mcomments_iv_userphoto);
            mivStickerImage = (ImageView) itemView.findViewById(R.id.row_mcomments_iv_stickerimage);
            mtvReportAbuse = (TextView) itemView.findViewById(R.id.row_mcomments_tv_report);

            mtvUserName = (TextView) itemView.findViewById(R.id.row_mcomments_tv_username);
            mtvUserCommentDescription = (TextView) itemView.findViewById(R.id.row_mcomments_tv_usercomments);
            mtvUserLastCommented = (TextView) itemView.findViewById(R.id.row_mcomments_tv_lastcommented);


            itemView.setTag(this);
        }
    }
    public void setOnRecyclerItemListener(OnRecyclerItemClickListener _onRecyclerItemClickListener){
        this.onRecyclerItemClickListener = _onRecyclerItemClickListener;
    }
}
