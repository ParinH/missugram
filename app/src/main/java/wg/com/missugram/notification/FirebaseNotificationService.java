package wg.com.missugram.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import wg.com.missugram.R;
import wg.com.missugram.activities.MainActivity;
import wg.com.missugram.activities.TestMainActivity;
import wg.com.missugram.constants.Constants;


public class FirebaseNotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {

            if (TestMainActivity._Current == null) {
                sendNotification(remoteMessage);
            } else {
                TestMainActivity._Current.runOnUiThread(new Runnable() {
                    public void run() {
                        String id = remoteMessage.getData().get("notification_id");
                        String memId = remoteMessage.getData().get("memorial_id");
                        TestMainActivity.showSettingsAlert(TestMainActivity._Current, remoteMessage.getData().get("title") == null ? "" : remoteMessage.getData().get("title"), id, memId);
                    }
                });
            }

        }


    }

    private void sendNotification(RemoteMessage remoteMessage) {
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        String id = remoteMessage.getData().get("notification_id");
        Intent mIntent = new Intent(this, TestMainActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mIntent.putExtra(Constants.KEY_PUSH_NOTIFICATIONID, id);
        mIntent.putExtra(Constants.KEY_PUSH_USERID, remoteMessage.getData().get("user_id"));
        mIntent.putExtra(Constants.KEY_PUSH_MEMORIAMID, remoteMessage.getData().get("memorial_id"));

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                mIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)

                        .setSmallIcon(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 ? R.mipmap.ic_launcher : R.drawable.ic_push_missugram)
                        .setContentTitle(remoteMessage.getData().get("title") == null ? "" : remoteMessage.getData().get("title"))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(remoteMessage.getData().get("message") == null ? "" : remoteMessage.getData().get("message")))
                        .setContentText(remoteMessage.getData().get("message") == null ? "" : remoteMessage.getData().get("message")).setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);

        // sending notification to system. Here we use unique id (when)for making different each notification. if we use same id,then first notification replace by the last notification.

        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        mNotificationManager.notify(m, mBuilder.build());
    }
}
