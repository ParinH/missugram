package wg.com.missugram.activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import wg.com.missugram.R;


public abstract class BaseActivity extends AppCompatActivity {

    private Dialog mDialog;
    public final static int EXIT_DLG = 1;
    public final static int LOGOUT_DLG = 2;
    public final static int DELETE_DLG = 3;
    private AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check DB status common object - starts

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void showYesNoDialog(final int dialogAction, final onUserPick listener) {

        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.yes_no_dialog, null);

        TextView yes_tv = (TextView) layout.findViewById(R.id.dialogTvYes);
        TextView no_tv = (TextView) layout.findViewById(R.id.dialogTvNo);
        TextView body_tv = (TextView) layout.findViewById(R.id.dialogTvBody);


        switch (dialogAction) {


            case EXIT_DLG:
                body_tv.setText(getString(R.string.are_you_sure_you_want_to_leave));
                break;
        }

        yes_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (dialogAction != EXIT_DLG) {
                            if (listener != null)
                                listener.userPick(true);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, 5);

            }
        });

        no_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null && mDialog.isShowing())
                    mDialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null)
                            listener.userPick(false);
                    }
                }, 5);
            }


        });

        mDialog = new Dialog(this);
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mDialog = null;
            }
        });
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL | Gravity.FILL_HORIZONTAL);
        mDialog.setContentView(layout);
        mDialog.show();
    }

    public interface onUserPick {
        void userPick(Boolean yes);
    }

    public void showYesNoDialog1(final String message, final int dialogAction, final onUserPick listener) {
        builder = new AlertDialog.Builder(this);
        builder.setMessage(message);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (dialogAction != EXIT_DLG) {
                            if (listener != null)
                                listener.userPick(true);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, 5);
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (listener != null)
                                    listener.userPick(false);
                            }
                        }, 5);
                    }
                }).show();
    }

}



