package wg.com.missugram.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.utils.FragmentController;

public class LoginActivity extends BaseActivity {

    private ProgressBar mPbLogin;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSIONS_REQUEST_WRITE_CONTACTS = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        findViews();
        getPermissionReadContact();
        getPermissionWriteContact();
    }

    private void getPermissionWriteContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_WRITE_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void getPermissionReadContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void findViews() {
        mPbLogin = (ProgressBar) findViewById(R.id.loginPb);
        mPbLogin.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        if (!Constants.BY_PASS_SPLASH) {
            Constants.BY_PASS_SPLASH = false;
            FragmentController.addSplashFragment(this);
        } else {
            FragmentController.addLoginFragment(this);
        }
    }

    public void showProgressBar() {
        if (mPbLogin != null)
            mPbLogin.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        if (mPbLogin != null)
            mPbLogin.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if (FragmentController.getFrontFragmentTag(LoginActivity.this).equals(FragmentController.TAG_LOGIN_FRAGMENT)) {
            showYesNoDialog1(getString(R.string.str_suretoexit), BaseActivity.EXIT_DLG, yes -> {
                if (yes)
                    finish();
            });
        } else {
            super.onBackPressed();
        }
    }*/
    }
}
