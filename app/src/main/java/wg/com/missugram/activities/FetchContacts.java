package wg.com.missugram.activities;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.ContactRequestVo;
import wg.com.missugram.model.ContactVo;
import wg.com.missugram.network.ApiClient;
import wg.com.missugram.network.ApiInterface;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.CommonUtills;

public class FetchContacts extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    Context context;
    private static final int CONTACTS_LOADER_ID = 1;
    private ArrayList<ContactVo> contactVos = new ArrayList<>();

    public FetchContacts(Context context,LoaderManager loaderManager) {
        this.context = context;
        loaderManager.initLoader(CONTACTS_LOADER_ID, null, this);

    }

    @NonNull
    @NotNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable @org.jetbrains.annotations.Nullable Bundle args) {
        if (id == CONTACTS_LOADER_ID) {
            return contactsLoader();
        }
        return null;
    }

    private Loader<Cursor> contactsLoader() {
        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI; // The content
        return new CursorLoader(context, contactsUri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull @NotNull Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            contactsFromCursor(data);
        }
    }
    private void contactsFromCursor(Cursor cursor) {
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));


                String conName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                while (emailCur.moveToNext()) {
                    ContactVo m_contactsVo = new ContactVo();
                    m_contactsVo.setName(conName);

                    String email = emailCur.getString(
                            emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    m_contactsVo.setContact(email);
                    contactVos.add(m_contactsVo);

                }
                Cursor phoneCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                if (phoneCur == null)
                    break;
                while (phoneCur.moveToNext()) {
                    ContactVo m_contactsVo = new ContactVo();
                    m_contactsVo.setName(conName);
                    String email = phoneCur.getString(
                            phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    m_contactsVo.setContact(email);
                    contactVos.add(m_contactsVo);

                }
            } while (cursor.moveToNext());


            sendContactToServer();
        }

    }

    private void sendContactToServer() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        ContactRequestVo contactRequestVo = new ContactRequestVo();
        if (MissUGram.getApp(context).getUserModel() == null) {
            contactRequestVo.setUserid(CommonUtills.getPrefId(context));
        } else {
            contactRequestVo.setUserid(MissUGram.getApp(context).getUserModel().getId());
        }
        contactRequestVo.setFriend_list(contactVos);

        final Call<CommonVo> contactResponse = apiInterface.submitContactDetail(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(FetchContacts.this), contactRequestVo);
        contactResponse.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {

            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {

            }
        });

    }

    @Override
    public void onLoaderReset(@NonNull @NotNull Loader<Cursor> loader) {

    }
}
