package wg.com.missugram.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.fragments.ChatFragment;
import wg.com.missugram.fragments.CreateGroup;
import wg.com.missugram.fragments.CreateMemorium;
import wg.com.missugram.fragments.DescriptionFragment;
import wg.com.missugram.fragments.FriendsAndFamilyFragment;
import wg.com.missugram.fragments.GrievanceChatFragment;
import wg.com.missugram.fragments.InformationFragment;
import wg.com.missugram.fragments.MemorialFragment;
import wg.com.missugram.fragments.SettingsFragment;
import wg.com.missugram.model.ChatUserHistoryModel;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.ContactRequestVo;
import wg.com.missugram.model.ContactVo;
import wg.com.missugram.model.MemorialDetailSingleResponseVo;
import wg.com.missugram.model.MemorialResponseVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.ApiClient;
import wg.com.missugram.network.ApiInterface;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

import static wg.com.missugram.R.drawable.ic_arrow_back_white_24dp;
import static wg.com.missugram.R.drawable.ic_menu_white_24dp;
import static wg.com.missugram.R.id.dr_tv_search_des;
import static wg.com.missugram.R.id.toolbar;
import static wg.com.missugram.utils.FragmentController.isThisFragmentInFront;

public class MainActivity extends BaseActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int CONTACTS_LOADER_ID = 1;
    static public MainActivity _Current = null;
    public static String memid;
    ApiInterface apiInterface;
    private Toolbar mToolbar;
    private ProgressBar mPbMain;
    private TextView mTvToolbarTitle;
    // private DrawerLayout drawer;
    private ImageView mIvToolbarIcon, mIvToolbarIconChatUser;
    private MenuDrawer mDrawer;
    private TextView mTvFF;
    private TextView drTvFf;
    private TextView drTvMemPage;
    private TextView drTvMyMem;
    private TextView drTvSearchQuote;
    private TextView drTvSearchDes;
    private TextView drTvChat;
    private TextView drTvSetting;
    private TextView drTvSignOut;
    private TextView drTvGriveanceChat;
    private ImageView mIvProfile;
    private TextView mTvName;
    private TextView mTvEmail;
    private int currentMemorial = Constants.FROM_MEMORIAL_LISTING;
    private int currentFriendList = Constants.FRIEND_LIST_FROM_FROM_MAIN;
    private int isFromSettings;
    private int isFromSlug;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSIONS_REQUEST_WRITE_CONTACTS = 100;
    private int isFromGroupChat;
    private ArrayList<ContactVo> contactVos = new ArrayList<>();
    private QBIncomingMessagesManager incomingMessagesManager;
    private QBSystemMessagesManager systemMessagesManager;
    Call<MemorialResponseVo> call;

    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),100);
            return false;
        }

        return true;
    }
    public static void showSettingsAlert(final Activity activity, final String p_Message, final String p_Id, final String p_MemoriamId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        alertDialog.setTitle(MainActivity._Current.getString(R.string.app_name));

        alertDialog.setMessage(p_Message);

        // On pressing ok button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (p_Id.equalsIgnoreCase("1")) {
                    CommonUtills.setPrefIsFromNotification(MainActivity._Current, true);
                    FragmentController.removeAllFragmentsFromStack(MainActivity._Current);
                    FragmentController.addMemorialFragment(MainActivity._Current, Constants.FROM_MEMORIAL_LISTING);

                } else if (p_Id.equalsIgnoreCase("2")) {
                    memid = p_MemoriamId;
                    CommonUtills.setPrefIsFromNotification(MainActivity._Current, true);
                    FragmentController.removeAllFragmentsFromStack(MainActivity._Current);
                    FragmentController.addCommentsListingFragment(MainActivity._Current, Integer.valueOf(p_MemoriamId), Constants.FROM_MY_MEMORIAL);
                }
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public QBSystemMessagesManager getSystemMessagesManager() {
        return systemMessagesManager;
    }

    private void setIncomingMessageListener() {
        QBUser user1 = new QBUser(CommonUtills.getPrefCurrentChatUname(MainActivity.this), CommonUtills.getPrefCurrentChatUname(MainActivity.this));
        user1.setId(CommonUtills.getPrefChatId(MainActivity.this));
        user1.setPassword(CommonUtills.getPrefCurrentChatUname(MainActivity.this));
        user1.setEmail(CommonUtills.getPrefCurrentChatUname(MainActivity.this));
        new Thread(() -> {
            if (!QBChatService.getInstance().isLoggedIn()) {


                QBChatService.getInstance().login(user1, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        dispatchBadge(user1);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        e.getErrors();
                    }
                });


            } else {
                dispatchBadge(user1);
            }


        }).start();


    }

    private void dispatchBadge(QBUser user1) {
        if (incomingMessagesManager == null) {
            incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        }
        if (systemMessagesManager == null) {
            systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

            // setGroupCreationListener();
        }
        incomingMessagesManager.addDialogMessageListener(new QBChatDialogMessageListener() {
            @Override
            public void processMessage(String dialogId, QBChatMessage qbChatMessage, Integer senderId)
            {



                if (senderId != user1.getId()) {
                    if (FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT) instanceof GrievanceChatFragment) {
                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);


                        QBRestChatService.getChatDialogById(dialogId).performAsync(new QBEntityCallback<QBChatDialog>() {
                            @Override
                            public void onSuccess(QBChatDialog mqbChatDialog, Bundle bundle) {





                                if (qbChatMessage.getSenderId() != null && qbChatMessage.getSenderId() != user1.getId()) {

                                    QBUsers.getUser(qbChatMessage.getSenderId()).performAsync(new QBEntityCallback<QBUser>() {
                                        @Override
                                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                                            ChatUserHistoryModel model = new ChatUserHistoryModel();

                                            if (mqbChatDialog.getType() == QBDialogType.PRIVATE) {
                                                int id = qbUser.getId() == null ? -1 : qbUser.getId();

                                                model.setChatuser_name(qbUser.getFullName() == null ? qbUser.getLogin() : qbUser.getFullName());
                                                model.setChatuser_id(id);
                                                model.setChatuser_fileid(qbUser.getFileId() == null ? -1 : qbUser.getFileId());
                                                model.setType(mqbChatDialog.getType() == QBDialogType.PRIVATE ? QBDialogType.PRIVATE : QBDialogType.GROUP);
                                                model.setChatuser_status(mqbChatDialog.getLastMessage());
                                                model.setUnread_messagecount(mqbChatDialog.getUnreadMessageCount());
                                            } else {
                                                // messages for a group will only be updated If group has been joined once but if you need forcefully you can join every dialog in a loop to get the updated messages from a listener which works fine but not a convenient option to do.
                                                model.setType(QBDialogType.GROUP);
                                                model.setChatDialog(mqbChatDialog);
                                                model.setUnread_messagecount(mqbChatDialog.getUnreadMessageCount());
                                                model.setChatuser_status(mqbChatDialog.getLastMessage());
                                                model.setLastMessageDateSentPrivateChat(mqbChatDialog.getLastMessageDateSent());
                                            }


                                            grievanceChatFragment.callAndUpdate(model);
                                        }

                                        @Override
                                        public void onError(QBResponseException e) {
                                            e.getLocalizedMessage();

                                        }
                                    });
                                }
                            }
                            @Override
                            public void onError(QBResponseException e) {
                                e.getLocalizedMessage();
                            }
                        });

                    }
                }




            }

            @Override
            public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this, MenuDrawer.Type.BEHIND, Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);
        setContentView(R.layout.activity_drawer);
        checkPermissions();
        mDrawer.setMenuView(R.layout.menu_scrollview);
        mDrawer.setMenuSize((int) getResources().getDimension(R.dimen._250sdp));
        initViews();
        setData();
        setListeners();
        hideActionBarTitle(false);
        checkForNotification();
        registerDrawerStates();
        setIncomingMessageListener();

        getPermissionReadContact();
        getPermissionWriteContact();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }

    private void getPermissionWriteContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_WRITE_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void getPermissionReadContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void checkForNotification() {
        if (getIntent().getStringExtra(Constants.KEY_PUSH_NOTIFICATIONID) != null) {
            UserModel userModel = new UserModel();

            userModel.setId(CommonUtills.getPrefId(this));
            userModel.setFirst_name(CommonUtills.getPrefFirstName(this));
            userModel.setLast_name(CommonUtills.getPrefFirstName(this));
            userModel.setAvatar(CommonUtills.getPrefFirstName(this));
            userModel.setPush_flag(CommonUtills.getPrefisFromNotification(this) ? "1" : "0");
            userModel.setEmail(CommonUtills.getPrefUserEmail(this));
            userModel.setPassword(CommonUtills.getPrefUserPassword(this));
            userModel.setPhone(CommonUtills.getPrefUserEmail(this));
            userModel.setToken(CommonUtills.getPrefToken(this));


            MissUGram.getApp(this).setUserModel(userModel);
            String id = getIntent().getStringExtra(Constants.KEY_PUSH_NOTIFICATIONID);
            if (id.equalsIgnoreCase("1")) {
                CommonUtills.setPrefIsFromNotification(MainActivity.this, true);
                call.cancel();
                FragmentController.addMemorialFragment(MainActivity.this, Constants.FROM_MEMORIAL_LISTING);

            } else if (id.equalsIgnoreCase("2")) {
                memid = getIntent().getStringExtra(Constants.KEY_PUSH_MEMORIAMID);
                call.cancel();
                FragmentController.addCommentsListingFragment(MainActivity.this, Integer.valueOf(memid), Constants.FROM_MY_MEMORIAL);
                CommonUtills.setPrefIsFromNotification(MainActivity.this, true);
            }
        } else {
            FragmentController.addMemorialFragment(this, Constants.FROM_MEMORIAL_LISTING);
        }
    }

    protected void onResume() //Activated
    {
        super.onResume();
        _Current = this;


        CommonUtills.dismissProgressDialog();

    }

    protected void onPause() //Deactivated
    {
        super.onPause();

        _Current = null;
    }

    private void registerDrawerStates() {
        mDrawer.setOnDrawerStateChangeListener(new MenuDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {

                if (newState == MenuDrawer.STATE_OPENING) {

                    hideKeyboard();
                }
            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {
            }
        });
    }

    public void setData() {

        if (MissUGram.getApp(this).getUserModel() != null) {
            mTvName.setText(MissUGram.getApp(this).getUserModel().getFirst_name() + " " + MissUGram.getApp(this).getUserModel().getLast_name());
            mTvEmail.setText(MissUGram.getApp(this).getUserModel().getEmail());
            ImageLoader.getInstance().displayImage(MissUGram.getApp(this).getUserModel().getAvatar(), mIvProfile, CommonUtills.getRoundImageOptionsForMenuProfile());
        }

    }

    private void setListeners() {
        mIvToolbarIcon.setOnClickListener(this);
        mIvToolbarIconChatUser.setOnClickListener(this);

        mTvFF.setOnClickListener(this);
        drTvMemPage.setOnClickListener(this);
        drTvMyMem.setOnClickListener(this);
        drTvSearchQuote.setOnClickListener(this);
        drTvSearchDes.setOnClickListener(this);
        drTvChat.setOnClickListener(this);
        drTvSetting.setOnClickListener(this);
        drTvSignOut.setOnClickListener(this);
        drTvGriveanceChat.setOnClickListener(this);
    }

    private void initViews() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTvToolbarTitle = (TextView) findViewById(R.id.tvToolbarTitle);
        mIvToolbarIcon = (ImageView) findViewById(R.id.ivToolbarIcon);
        mIvToolbarIconChatUser = (ImageView) findViewById(R.id.ivToolbarIconChatUser);

        mToolbar = (Toolbar) findViewById(toolbar);

        mTvToolbarTitle = (TextView) findViewById(R.id.tvToolbarTitle);
        mIvToolbarIcon = (ImageView) findViewById(R.id.ivToolbarIcon);
        mIvToolbarIconChatUser = (ImageView) findViewById(R.id.ivToolbarIconChatUser);
        setToolbarIcon(ic_menu_white_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

        mTvFF = (TextView) findViewById(R.id.dr_tv_ff);
        drTvMemPage = (TextView) findViewById(R.id.dr_tv_mem_page);
        drTvMyMem = (TextView) findViewById(R.id.dr_tv_my_mem);
        drTvSearchQuote = (TextView) findViewById(R.id.dr_tv_search_quote);
        drTvSearchDes = (TextView) findViewById(dr_tv_search_des);
        drTvFf = (TextView) findViewById(R.id.dr_tv_ff);
        drTvChat = (TextView) findViewById(R.id.dr_tv_chat);
        drTvSetting = (TextView) findViewById(R.id.dr_tv_setting);
        drTvSignOut = (TextView) findViewById(R.id.dr_tv_sign_out);
        drTvGriveanceChat = (TextView) findViewById(R.id.dr_tv_chat);
        mPbMain = (ProgressBar) findViewById(R.id.mainPb);
        mIvProfile = (ImageView) findViewById(R.id.iv_profile);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvEmail = (TextView) findViewById(R.id.tv_email);
        mPbMain.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);

        getSupportLoaderManager().initLoader(CONTACTS_LOADER_ID, null, this);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public void hideActionBarTitle(boolean showTitle) {
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
    }

    /**
     * This method is useful to change left top toolbar icon
     *
     * @param ic_list_white_24dp icon id
     */
    public void setToolbarIcon(int ic_list_white_24dp) {
        mToolbar.setNavigationIcon(ic_list_white_24dp);

    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {

            if (isThisFragmentInFront(MainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT) instanceof MemorialFragment) {
                showYesNoDialog1(getString(R.string.str_suretoexit), BaseActivity.EXIT_DLG, yes -> {
                    if (yes)
                        finish();
                });
            } else if (isThisFragmentInFront(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT) instanceof FriendsAndFamilyFragment && currentFriendList == Constants.FRIEND_LIST_FROM_FROM_INVITE) {
                if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                    FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                    // if opened from grievance chat and pressing back we have to show the add group icon so calling the initUI.
                    if (FragmentController.getFrontFragmentTag(MainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(MainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                        grievanceChatFragment.initUI();
                    }
                } else {
                    FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                }
                hideKeyboard();

                new Handler().postDelayed(() -> updateToolbar(), 250);

            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                if (CommonUtills.getPrefisFromNotification(MainActivity.this)) {
                    CommonUtills.setPrefIsFromNotification(MainActivity.this, false);
                    FragmentController.addMemorialFragment(MainActivity.this, Constants.FROM_MEMORIAL_LISTING);
                } else {
                    new Handler().postDelayed(() -> updateToolbar(), 250);
                }
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT)) {

                if (getCurrentMemorialVal() == Constants.FROM_MYMEMORIAL_EDIT) {


                    MemorialVo memorialVo = null;
                    if (FragmentController.isThisFragmentInFront(this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT) instanceof CreateMemorium) {
                        CreateMemorium createMemorium = (CreateMemorium) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                        memorialVo = createMemorium.getmMemoriamVo();
                        FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                    }
                    final MemorialVo finalMemorialVo = memorialVo;
                    new Handler().postDelayed(() -> {
                        if (FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT) instanceof DescriptionFragment) {
                            DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                            descriptionFragment.setmMemoriamVo(finalMemorialVo);

                        }
                    }, 200);

                } else {

                    if (getCurrentMemorialVal() == Constants.FROM_MY_MEMORIAL) {
                        FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                    }
                }
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_COMMENTSLISTING_FRAGMENT)) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_COMMENTSLISTING_FRAGMENT);

                if (CommonUtills.getPrefisFromNotification(MainActivity.this)) {

                    // call the memorial detail ws and fill the vo
                    if (!memid.isEmpty() && memid != null) {
                        callDetailFromMemorium(memid);
                    }
                }


            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_EDIT_PROFILE_FRAGMENT) && getIsFromSettings() == Constants.FROM_SETTINGS) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_EDIT_PROFILE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT)) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_INFORMATION_FRAGMENT)) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CHAT_FRAGMENT)) {
                ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to update the chat based on the dialog in grivience screen we are getting the dialog and making a query in quickblox.

                QBChatDialog chatDialogGroup = null;
                QBChatDialog chatDialogPrivate = null;
                if (chatFragment != null) {
                    chatDialogGroup = chatFragment.getQbChatDialogFoGroupChat();
                    chatDialogPrivate = chatFragment.getQbChatDialogFoPrivaterChat();
                }
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);

                mIvToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.group));

                QBChatDialog finalChatDialogGroup = chatDialogGroup;
                QBChatDialog finalChatDialogPrivate = chatDialogPrivate;
                new Handler().postDelayed(new Runnable() {
                                              @Override
                                              public void run() {
                                                  if (FragmentController.getFrontFragmentTag(MainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                                                      GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(MainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                                                      grievanceChatFragment.initUI();
                                                      grievanceChatFragment.checkIfNewMessageArrived(finalChatDialogPrivate);
                                                      grievanceChatFragment.checkIfNewMessageArrivedForGroup(finalChatDialogGroup);
                                                  }

                                              }
                                          }
                        , 250);

            } else {
                FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                FragmentController.addMemorialFragment(MainActivity.this, Constants.FROM_MEMORIAL_LISTING);
            }

            if (!CommonUtills.getPrefisFromNotification(MainActivity.this)) {
                new Handler().postDelayed(() -> updateToolbar(), 250);
            }
        }
    }

    private void callDetailFromMemorium(String id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        CommonUtills.showProgressDialog(this, getString(R.string.str_pleasewait), false);
        Call<MemorialDetailSingleResponseVo> mDetail = apiInterface.getDetailFromMemoriumId(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(this), ApiClient.BASE_ADDRESS + "memoriam/" + id);
        mDetail.enqueue(new Callback<MemorialDetailSingleResponseVo>() {
            @Override
            public void onResponse(Call<MemorialDetailSingleResponseVo> call, Response<MemorialDetailSingleResponseVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    MemorialDetailSingleResponseVo memorialDetail = response.body();

                    if (memorialDetail.getsStatus() == Constants.STATUS_SUCCESS) {
                        memid = "";
                        FragmentController.addDescriptionFragment(MainActivity.this, memorialDetail.getMemorialinfo(), Constants.FROM_MY_MEMORIAL);
                    } else {
                        CommonUtills.showAlert(MainActivity.this, response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<MemorialDetailSingleResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    /**
     * This method update fragment title once user press back button
     */
    public void updateToolbar() {

        switch (FragmentController.getFrontFragmentTag(this)) {
            case FragmentController.TAG_FIRST_FRAGMENT:
                setToolbarTitle(getString(R.string.first_fragment));
                setToolbarIcon(ic_menu_white_24dp);
                mIvToolbarIcon.setVisibility(View.VISIBLE);
                break;
            case FragmentController.TAG_MEMORIAL_FRAGMENT:
                setToolbarIcon(ic_menu_white_24dp);
                mIvToolbarIcon.setVisibility(View.VISIBLE);
                if (currentMemorial == Constants.FROM_MEMORIAL_LISTING) {
                    drTvMemPage.setBackgroundColor(Color.parseColor("#172645"));
                    drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                    setToolbarTitle(getString(R.string.memorial_fragment));
                } else {
                    drTvMyMem.setBackgroundColor(Color.parseColor("#172645"));
                    drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                    setToolbarTitle(getString(R.string.my_memorial));
                }

                drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSetting.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));

                mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));
                break;
            case FragmentController.TAG_DESCRIPTION_FRAGMENT:
                if (FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT) instanceof DescriptionFragment) {
                    DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                    setToolbarTitle(descriptionFragment.getmMemoriamVo().getTitle());
                }
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.VISIBLE);
                mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_draft));
                if (getCurrentMemorialVal() == Constants.FROM_MY_MEMORIAL || getCurrentMemorialVal() == Constants.FROM_MYMEMORIAL_EDIT) {
                    mIvToolbarIcon.setVisibility(View.VISIBLE);
                } else {
                    mIvToolbarIcon.setVisibility(View.GONE);
                }
                drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSetting.setBackgroundColor(Color.parseColor("#172645"));
                drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));

                break;
            case FragmentController.TAG_SETTINGS_FRAGMENT:
                setToolbarTitle(getString(R.string.settings_fragment));
                setToolbarIcon(ic_menu_white_24dp);
                mIvToolbarIcon.setVisibility(View.GONE);
                drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSetting.setBackgroundColor(Color.parseColor("#172645"));
                drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));

                break;
            case FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT:
                setToolbarTitle(getString(R.string.create_memorium_fragment));
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.VISIBLE);
                mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
                break;
            case FragmentController.TAG_COMMENTSLISTING_FRAGMENT:
                setToolbarTitle(getString(R.string.comments_fragment));
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.GONE);
                mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
                break;

            case FragmentController.TAG_EDIT_PROFILE_FRAGMENT:
                setToolbarTitle(getString(R.string.str_profile));
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.GONE);
                break;

            case FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT:


                if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                    setToolbarTitle("Contact List");
                    setToolbarIcon(ic_arrow_back_white_24dp);

                } else {
                    setToolbarTitle(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? getString(R.string.friends_and_family) : getString(R.string.str_header_invitecontacts));
                    setToolbarIcon(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? ic_menu_white_24dp : ic_arrow_back_white_24dp);
                    mIvToolbarIcon.setVisibility(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? View.GONE : View.VISIBLE);

                }

                mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
                drTvFf.setBackgroundColor(Color.parseColor("#172645"));
                drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSetting.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));
                break;
            case FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT:
                setToolbarTitle(getString(R.string.str_changepasswored));
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.GONE);
                break;
            case FragmentController.TAG_INFORMATION_FRAGMENT:

                if (getIsFromSlug() == Constants.FROM_SLUG) {
                    setToolbarIcon(ic_arrow_back_white_24dp);
                    mIvToolbarIcon.setVisibility(View.GONE);
                } else if (getIsFromSlug() == Constants.FROM_SEARCH_DECEASED) {
                    drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSearchDes.setBackgroundColor(Color.parseColor("#172645"));
                    drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSetting.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));

                    mIvToolbarIcon.setVisibility(View.GONE);
                } else if (getIsFromSlug() == Constants.FROM_SEARCH_QUOTE) {
                    drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSearchQuote.setBackgroundColor(Color.parseColor("#172645"));
                    drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSetting.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));
                    drTvGriveanceChat.setBackgroundColor(Color.parseColor("#182D5C"));
                    mIvToolbarIcon.setVisibility(View.GONE);
                } else {

                    setToolbarIcon(ic_menu_white_24dp);
                    mIvToolbarIcon.setVisibility(View.GONE);
                }
                break;
            case FragmentController.TAG_GRIEVANCE_FRAGMENT:

                if (Constants.FROM_GROUP_CHAT_TAB == getIsFromGroupChat()||Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS==getIsFromGroupChat()) {
                    setToolbarTitle(getString(R.string.grievance_fragment));
                    setToolbarIcon(ic_menu_white_24dp);
                    mIvToolbarIcon.setVisibility(View.VISIBLE);
                    mIvToolbarIconChatUser.setVisibility(View.GONE);
                    mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_white_24dp));
                } else {
                    setToolbarTitle(getString(R.string.grievance_fragment));
                    setToolbarIcon(ic_menu_white_24dp);
                    mIvToolbarIcon.setVisibility(View.GONE);
                    mIvToolbarIconChatUser.setVisibility(View.GONE);
                }


                drTvGriveanceChat.setBackgroundColor(Color.parseColor("#172645"));
                drTvFf.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvMemPage.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvChat.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvMyMem.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchDes.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSearchQuote.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSetting.setBackgroundColor(Color.parseColor("#182D5C"));
                drTvSignOut.setBackgroundColor(Color.parseColor("#182D5C"));


                break;
            case FragmentController.TAG_CHAT_FRAGMENT:

                setToolbarTitle(getString(R.string.dr_chat));
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.GONE);
                mIvToolbarIconChatUser.setVisibility(View.GONE);
                break;

            case FragmentController.TAG_CREATE_GROUP_FRAGMENT:
                setToolbarTitle("Create Group");
                setToolbarIcon(ic_arrow_back_white_24dp);
                mIvToolbarIcon.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void initChatUI(String title, String imageId, int type) {

        //  setToolbarTitle(title);
        mTvToolbarTitle.setAllCaps(true);
        mTvToolbarTitle.setText(title);
        if (type == Constants.FOR_GROUP_CHAT) {
            mIvToolbarIcon.setVisibility(View.VISIBLE);
            mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.exit_group));
        } else {
            mIvToolbarIcon.setVisibility(View.GONE);
            mIvToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_userplus));
        }

        mIvToolbarIconChatUser.setVisibility(View.VISIBLE);
// since quicklblox returns the null word in getPhoto() in string so checked as below.
        if (!imageId.equalsIgnoreCase("null") && !TextUtils.isEmpty(imageId) && !imageId.equalsIgnoreCase("-1")) {


            ChatProvider.getInstance().getImageUrlFromFileId(Integer.valueOf(imageId), new QBEntityCallback<QBFile>() {
                @Override
                public void onSuccess(QBFile qbFile, Bundle bundle) {
                    ImageLoader.getInstance().displayImage(qbFile.getPublicUrl(), mIvToolbarIconChatUser,type==Constants.FOR_GROUP_CHAT? CommonUtills.getRoundImageOptionsForGroup():CommonUtills.getRoundImageOptions());
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
            } else if (currentFriendList == Constants.FRIEND_LIST_FROM_FROM_INVITE && FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_COMMENTSLISTING_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_COMMENTSLISTING_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_EDIT_PROFILE_FRAGMENT) && getIsFromSettings() == Constants.FROM_SETTINGS) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_EDIT_PROFILE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_INFORMATION_FRAGMENT) && getIsFromSlug() == Constants.FROM_SLUG) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {
                FragmentController.removeThisFragment(MainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CHAT_FRAGMENT)) {
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to refresh the list of count and user in the chat re add the fragment below.

                ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to update the chat based on the dialog in grievance screen we are getting the dialog and making a query in quickblox.

                QBChatDialog chatDialogGroup = null;
                QBChatDialog chatDialogPrivate = null;
                if (chatFragment != null) {
                    chatDialogGroup = chatFragment.getQbChatDialogFoGroupChat();
                    chatDialogPrivate = chatFragment.getQbChatDialogFoPrivaterChat();
                }
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);

                mIvToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.group));

                QBChatDialog finalChatDialogGroup = chatDialogGroup;
                QBChatDialog finalChatDialogPrivate = chatDialogPrivate;
                new Handler().postDelayed(new Runnable() {
                                              @Override
                                              public void run() {
                                                  if (FragmentController.getFrontFragmentTag(MainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                                                      GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(MainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                                                      grievanceChatFragment.initUI();
                                                      grievanceChatFragment.checkIfNewMessageArrived(finalChatDialogPrivate);
                                                      grievanceChatFragment.checkIfNewMessageArrivedForGroup(finalChatDialogGroup);
                                                  }

                                              }
                                          }
                        , 250);


            } else if (FragmentController.getFrontFragmentTag(MainActivity.this).equals(FragmentController.TAG_CREATE_GROUP_FRAGMENT)) {
                setIsFromGroupChat(Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS);
                setToolbarTitle("Contact List");
                setToolbarIcon(ic_arrow_back_white_24dp);
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CREATE_GROUP_FRAGMENT);
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);

            } else {
                mDrawer.toggleMenu();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgressBar() {
        if (mPbMain != null)
            mPbMain.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        if (mPbMain != null)
            mPbMain.setVisibility(View.GONE);
    }

    /**
     * Method to change toolbar title according to fragment being selected
     *
     * @param title title to be change
     */
    public void setToolbarTitle(String title) {
        mTvToolbarTitle.setText(title);
        // to prevent title appear in a capital words. it was doen all caps for chat fragment.
        mTvToolbarTitle.setAllCaps(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolbarIcon:
                switch (FragmentController.getFrontFragmentTag(this)) {
                    case FragmentController.TAG_MEMORIAL_FRAGMENT:
                        MemorialFragment mainFragment = (MemorialFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT);
                        if (mainFragment != null) {
                            mainFragment.showSearchBar();
                        }
                        break;
                    case FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT:
                        CreateMemorium createMemorium = (CreateMemorium) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                        if (createMemorium != null) {
                            createMemorium.doProcessCreateMemorium();
                        }

                        break;

                    case FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT:
                        FriendsAndFamilyFragment friendsAndFamilyFragment = (FriendsAndFamilyFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                        if (friendsAndFamilyFragment != null) {

                            if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                                friendsAndFamilyFragment.collectAndOpenCreateGroup();
                            } else {
                                friendsAndFamilyFragment.senInvitation();
                            }
                        }
                        break;
                    case FragmentController.TAG_DESCRIPTION_FRAGMENT:
                        DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                        if (descriptionFragment != null) {
                            descriptionFragment.openCreateMemoriumForEdit();
                        }
                        break;
                    case FragmentController.TAG_GRIEVANCE_FRAGMENT:
                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                        if (grievanceChatFragment != null) {
                            FragmentController.addFriendListFragment(MainActivity.this, Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS, null);
                        }
                        break;
                    case FragmentController.TAG_CREATE_GROUP_FRAGMENT:
                        CreateGroup createGroup = (CreateGroup) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_CREATE_GROUP_FRAGMENT);
                        if (createGroup != null) {
                            createGroup.validateData();
                        }
                        break;
                    case FragmentController.TAG_CHAT_FRAGMENT:
                        ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                        if (chatFragment != null) {
                            chatFragment.removeUser();
                        }
                        break;

                }
                break;

            case R.id.ivToolbarIconChatUser:
                FragmentController.removeFragment(MainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                mIvToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.group));
                break;

            case R.id.dr_tv_setting:
                mDrawer.closeMenu();
                SettingsFragment settingsFragment = (SettingsFragment) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_SETTINGS_FRAGMENT);
                if (settingsFragment == null) {
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    FragmentController.addSettingsFragment(MainActivity.this, "");
                }

                break;
            case R.id.dr_tv_chat:
                mDrawer.closeMenu();
                GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_GRIEVANCE_FRAGMENT);

                if (grievanceChatFragment == null && ChatProvider.getInstance().checkSignIn()) {
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    FragmentController.addGrievanceChatFragment(MainActivity.this);
                } else {
                    CommonUtills.showAlert(MainActivity.this, "Please wait logging into chat", Constants.NO_ACTION);
                }

                break;
            case dr_tv_search_des:
                mDrawer.closeMenu();

                InformationFragment informationFragment = (InformationFragment) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_INFORMATION_FRAGMENT);
                if (informationFragment == null) {
                    setIsFromSlug(Constants.FROM_SEARCH_DECEASED);
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    setToolbarTitle(getString(R.string.dr_search_deceased));
                    FragmentController.addInformationFragment(MainActivity.this, null, "http://www.ancestry.com/cs/us/death-records-exp-i-overlay?state=&kw=Death+Records&s_kwcid=death+records+free&gclid=Cj0KEQjwi7vIBRDpo9W8y7Ct6ZcBEiQA1CwV2DftemjV8uoF7RemXVaRMeFqCl9oMAOvvi1tS2-VIjAaAoio8P8HAQ&o_xid=21837&o_lid=21837&o_sch=Paid+Search+Non+Brand");

                } else {
                    setIsFromSlug(Constants.FROM_SEARCH_DECEASED);
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    setToolbarTitle(getString(R.string.dr_search_deceased));
                    FragmentController.addInformationFragment(MainActivity.this, null, "http://www.ancestry.com/cs/us/death-records-exp-i-overlay?state=&kw=Death+Records&s_kwcid=death+records+free&gclid=Cj0KEQjwi7vIBRDpo9W8y7Ct6ZcBEiQA1CwV2DftemjV8uoF7RemXVaRMeFqCl9oMAOvvi1tS2-VIjAaAoio8P8HAQ&o_xid=21837&o_lid=21837&o_sch=Paid+Search+Non+Brand");

                }
                break;
            case R.id.dr_tv_search_quote:
                mDrawer.closeMenu();
                InformationFragment informationFragment_Quote = (InformationFragment) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_INFORMATION_FRAGMENT);
                if (informationFragment_Quote == null) {
                    setIsFromSlug(Constants.FROM_SEARCH_QUOTE);
                    setToolbarTitle(getString(R.string.dr_search_quote));
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    FragmentController.addInformationFragment(MainActivity.this, null, "https://www.brainyquote.com/quotes/topics/topic_inspirational.html");

                } else {
                    setIsFromSlug(Constants.FROM_SEARCH_QUOTE);
                    setToolbarTitle(getString(R.string.dr_search_quote));
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    FragmentController.addInformationFragment(MainActivity.this, null, "https://www.brainyquote.com/quotes/topics/topic_inspirational.html");

                }
                break;
            case R.id.dr_tv_ff:
                setCurrentFriendListVal(Constants.FRIEND_LIST_FROM_FROM_MAIN);
                mDrawer.closeMenu();

                FriendsAndFamilyFragment friendsAndFamilyFragment = (FriendsAndFamilyFragment) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                if (friendsAndFamilyFragment == null) {
                    FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                    FragmentController.addFriendListFragment(MainActivity.this, Constants.FRIEND_LIST_FROM_FROM_MAIN, null);
                }
                break;

            case R.id.dr_tv_my_mem:
                mDrawer.closeMenu();
                currentMemorial = Constants.FROM_MY_MEMORIAL;
                FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                FragmentController.addMemorialFragment(MainActivity.this, Constants.FROM_MY_MEMORIAL);
                break;
            case R.id.dr_tv_mem_page:
                mDrawer.closeMenu();
                currentMemorial = Constants.FROM_MEMORIAL_LISTING;
                FragmentController.removeAllFragmentsFromStack(MainActivity.this);
                FragmentController.addMemorialFragment(MainActivity.this, Constants.FROM_MEMORIAL_LISTING);
                break;
            case R.id.dr_tv_sign_out:
                showYesNoDialog1(getString(R.string.str_logout), BaseActivity.LOGOUT_DLG, new onUserPick() {
                    @Override
                    public void userPick(Boolean yes) {
                        if (yes)
                            if (CommonUtills.isNetworkAvailable(MainActivity.this)) {
                                //callUpdateNotification("0");
                                callUserLogout();
                            } else {
                                CommonUtills.showAlert(MainActivity.this, getString(R.string.no_internet_connection), Constants.NO_ACTION);
                            }
                    }
                });
                break;


        }
    }

    private void clearAndLogout() {


        FragmentController.removeAllFragmentsFromStack(this);
        this.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().clear().apply();
        Constants.BY_PASS_SPLASH = true;
        this.startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CommonUtills.dismissProgressDialog();
    }

    private void callUserLogout() {
        CommonUtills.showProgressDialog(this, getString(R.string.str_pleasewait), false);
        final Call<CommonVo> callUserLogout = apiInterface.logout(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(this), MissUGram.getApp(this) == null ? 0 : MissUGram.getApp(this).getUserModel().getId());
        callUserLogout.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {

                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.setPrefIsNotificationEnabled(MainActivity.this, false);
                        CommonUtills.clearAllNotification(MainActivity.this);
                        ChatProvider.getInstance().logoutUser();
                        clearAndLogout();
                    } else {
                        CommonUtills.showAlert(MainActivity.this, commonVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == CONTACTS_LOADER_ID) {
            return contactsLoader();
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            contactsFromCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private Loader<Cursor> contactsLoader() {
        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI; // The content
        return new CursorLoader(getApplicationContext(), contactsUri, null, null, null, null);
    }

    private void contactsFromCursor(Cursor cursor) {
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));


                String conName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                while (emailCur.moveToNext()) {
                    ContactVo m_contactsVo = new ContactVo();
                    m_contactsVo.setName(conName);

                    String email = emailCur.getString(
                            emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    m_contactsVo.setContact(email);
                    contactVos.add(m_contactsVo);

                }
                Cursor phoneCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                if (phoneCur == null)
                    break;
                while (phoneCur.moveToNext()) {
                    ContactVo m_contactsVo = new ContactVo();
                    m_contactsVo.setName(conName);
                    String email = phoneCur.getString(
                            phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    m_contactsVo.setContact(email);
                    contactVos.add(m_contactsVo);

                }
            } while (cursor.moveToNext());


            sendContactToServer();
        }

    }

    private void sendContactToServer() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        ContactRequestVo contactRequestVo = new ContactRequestVo();
        if (MissUGram.getApp(this).getUserModel() == null) {
            contactRequestVo.setUserid(CommonUtills.getPrefId(this));
        } else {
            contactRequestVo.setUserid(MissUGram.getApp(this).getUserModel().getId());
        }
        contactRequestVo.setFriend_list(contactVos);

        final Call<CommonVo> contactResponse = apiInterface.submitContactDetail(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(MainActivity.this), contactRequestVo);
        contactResponse.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {

            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {

            }
        });

    }

    public void setCurrentFriendListVal(int val) {
        currentFriendList = val;
    }

    public void setCurrentMemoriamVal(int val) {
        currentMemorial = val;
    }

    public int getCurrentMemorialVal() {
        return currentMemorial;
    }

    public int getIsFromSettings() {
        return isFromSettings;
    }

    public void setIsFromSettings(int isFromSettings) {
        this.isFromSettings = isFromSettings;
    }

    public int getIsFromSlug() {
        return isFromSlug;
    }

    public void setIsFromSlug(int isFromSlug) {
        this.isFromSlug = isFromSlug;
    }

    public int getIsFromGroupChat() {
        return isFromGroupChat;
    }

    public void setIsFromGroupChat(int isFromGroupChat) {
        this.isFromGroupChat = isFromGroupChat;
    }
}




