package wg.com.missugram.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wg.com.missugram.R;
import wg.com.missugram.app.MissUGram;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.databinding.ActivityTestMainBinding;
import wg.com.missugram.fragments.ChatFragment;
import wg.com.missugram.fragments.CreateGroup;
import wg.com.missugram.fragments.CreateMemorium;
import wg.com.missugram.fragments.DescriptionFragment;
import wg.com.missugram.fragments.FriendsAndFamilyFragment;
import wg.com.missugram.fragments.GrievanceChatFragment;
import wg.com.missugram.fragments.InformationFragment;

import wg.com.missugram.fragments.SettingsFragment;
import wg.com.missugram.fragments.TestMemorialFragment;
import wg.com.missugram.fragments.TestMyMemorialFragment;
import wg.com.missugram.model.ChatUserHistoryModel;
import wg.com.missugram.model.CommonVo;
import wg.com.missugram.model.ContactRequestVo;
import wg.com.missugram.model.ContactVo;
import wg.com.missugram.model.MemorialDetailSingleResponseVo;
import wg.com.missugram.model.MemorialVo;
import wg.com.missugram.model.UserModel;
import wg.com.missugram.network.ApiClient;
import wg.com.missugram.network.ApiInterface;
import wg.com.missugram.network.TokenKeeper;
import wg.com.missugram.utils.ChatProvider;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.FragmentController;

import static wg.com.missugram.R.drawable.ic_arrow_back_white_24dp;
import static wg.com.missugram.R.drawable.ic_menu_white_24dp;
import static wg.com.missugram.R.id.dr_tv_search_des;
import static wg.com.missugram.R.id.drawer;
import static wg.com.missugram.R.id.gone;
import static wg.com.missugram.utils.FragmentController.isThisFragmentInFront;


public class TestMainActivity extends BaseActivity implements View.OnClickListener{


    TextView tv_name, tvToolbarTitle;
    TextView tv_email;
    private static final int CONTACTS_LOADER_ID = 1;
    private ArrayList<ContactVo> contactVos = new ArrayList<>();

    ImageView iv_profile, ivToolbarIcon, ivToolbarIconChatUser,spe_search;
    private LinearLayout mLlSearch;
    private Toolbar mToolbar;
    private int isFromGroupChat;
    public static String memid;
    private int isFromSettings;
    private int isFromSlug;
    static public TestMainActivity _Current = null;

    private QBIncomingMessagesManager incomingMessagesManager;
    private QBSystemMessagesManager systemMessagesManager;
    ApiInterface apiInterface;
    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private AppBarConfiguration mAppBarConfiguration;
    private ActivityTestMainBinding binding;
    private int currentFriendList = Constants.FRIEND_LIST_FROM_FROM_MAIN;
    private int currentMemorial = Constants.FROM_MEMORIAL_LISTING;
    DrawerLayout drawer;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSIONS_REQUEST_WRITE_CONTACTS = 100;
    public static void showSettingsAlert(final Activity activity, final String p_Message, final String p_Id, final String p_MemoriamId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        alertDialog.setTitle(TestMainActivity._Current.getString(R.string.app_name));

        alertDialog.setMessage(p_Message);

        // On pressing ok button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (p_Id.equalsIgnoreCase("1")) {
                    CommonUtills.setPrefIsFromNotification(TestMainActivity._Current, true);
                    FragmentController.removeAllFragmentsFromStack(TestMainActivity._Current);
                    FragmentController.addMemorialFragment(TestMainActivity._Current, Constants.FROM_MEMORIAL_LISTING);

                } else if (p_Id.equalsIgnoreCase("2")) {
                    memid = p_MemoriamId;
                    CommonUtills.setPrefIsFromNotification(TestMainActivity._Current, true);
                    FragmentController.removeAllFragmentsFromStack(TestMainActivity._Current);
                    FragmentController.addCommentsListingFragment(TestMainActivity._Current, Integer.valueOf(p_MemoriamId), Constants.FROM_MY_MEMORIAL);
                }
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        binding = ActivityTestMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

      String tkn=MissUGram.getApp(TestMainActivity.this).getUserModel().getToken();
        Log.e("tkn", "" + tkn);

        mToolbar.setTitle("");
        setSupportActionBar(binding.appBarTestMain.toolbar);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        setToolbarIcon(ic_menu_white_24dp);
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setHomeButtonEnabled(true);
      //  getSupportLoaderManager().initLoader(CONTACTS_LOADER_ID, null, this);

        ivToolbarIcon = findViewById(R.id.ivToolbarIcon);
        spe_search = findViewById(R.id.spe_search);
        ivToolbarIconChatUser = findViewById(R.id.ivToolbarIconChatUser);
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle);
      //  ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));

        hideActionBarTitle(false);
        setToolbarIcon(ic_menu_white_24dp);
        ivToolbarIcon.setVisibility(View.VISIBLE);
        if (currentMemorial == Constants.FROM_MEMORIAL_LISTING) {
            setToolbarTitle(getString(R.string.memorial_fragment));
        }
        ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));

        ivToolbarIcon.setOnClickListener(this);

        checkPermissions();
        setData();
        checkForNotification();
        setIncomingMessageListener();
        getPermissionReadContact();
        getPermissionWriteContact();
        drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow, R.id.nav_search_des
                , R.id.nav_search_quote, R.id.nav_tv_chat, R.id.nav_dr_settings, R.id.nav_sign_out)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_test_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.nav_home) {

                    setToolbarIcon(ic_menu_white_24dp);
                    ivToolbarIcon.setVisibility(View.VISIBLE);
                    if (currentMemorial == Constants.FROM_MEMORIAL_LISTING) {
                        setToolbarTitle(getString(R.string.memorial_fragment));
                    }
                    new Handler().postDelayed(() -> updateToolbar(), 250);
                    ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));
                    FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                    FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MEMORIAL_LISTING);
                    drawer.close();
                }

                if (id == R.id.nav_gallery) {

                    FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                    FragmentController.myMemorialFragment(TestMainActivity.this, Constants.FROM_MY_MEMORIAL);
                    new Handler().postDelayed(() -> updateToolbar(), 250);
                    drawer.close();
                }
                if (id == R.id.nav_slideshow) {

                    FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                    FragmentController.addFriendListFragment(TestMainActivity.this, Constants.FRIEND_LIST_FROM_FROM_MAIN, null);
                    drawer.close();
                }

                if (id == R.id.nav_search_des) {
                    InformationFragment informationFragment = (InformationFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
                    if (informationFragment == null) {
                        setIsFromSlug(Constants.FROM_SEARCH_DECEASED);
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        setToolbarTitle(getString(R.string.dr_search_deceased));
                        FragmentController.addInformationFragment(TestMainActivity.this, null, "http://www.ancestry.com/cs/us/death-records-exp-i-overlay?state=&kw=Death+Records&s_kwcid=death+records+free&gclid=Cj0KEQjwi7vIBRDpo9W8y7Ct6ZcBEiQA1CwV2DftemjV8uoF7RemXVaRMeFqCl9oMAOvvi1tS2-VIjAaAoio8P8HAQ&o_xid=21837&o_lid=21837&o_sch=Paid+Search+Non+Brand");

                    } else {
                        setIsFromSlug(Constants.FROM_SEARCH_DECEASED);
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        setToolbarTitle(getString(R.string.dr_search_deceased));
                        FragmentController.addInformationFragment(TestMainActivity.this, null, "http://www.ancestry.com/cs/us/death-records-exp-i-overlay?state=&kw=Death+Records&s_kwcid=death+records+free&gclid=Cj0KEQjwi7vIBRDpo9W8y7Ct6ZcBEiQA1CwV2DftemjV8uoF7RemXVaRMeFqCl9oMAOvvi1tS2-VIjAaAoio8P8HAQ&o_xid=21837&o_lid=21837&o_sch=Paid+Search+Non+Brand");

                    }
                    drawer.close();
                }

                if (id == R.id.nav_search_quote) {
                    InformationFragment informationFragment_Quote = (InformationFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
                    if (informationFragment_Quote == null) {
                        setIsFromSlug(Constants.FROM_SEARCH_QUOTE);
                        setToolbarTitle(getString(R.string.dr_search_quote));
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        FragmentController.addInformationFragment(TestMainActivity.this, null, "https://www.brainyquote.com/quotes/topics/topic_inspirational.html");

                    } else {
                        setIsFromSlug(Constants.FROM_SEARCH_QUOTE);
                        setToolbarTitle(getString(R.string.dr_search_quote));
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        FragmentController.addInformationFragment(TestMainActivity.this, null, "https://www.brainyquote.com/quotes/topics/topic_inspirational.html");

                    }
                    drawer.close();
                }

                if (id == R.id.nav_tv_chat) {

                    GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);

                    if (grievanceChatFragment == null && ChatProvider.getInstance().checkSignIn()) {
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        FragmentController.addGrievanceChatFragment(TestMainActivity.this);
                    } else {
                        CommonUtills.showAlert(TestMainActivity.this, "Please wait logging into chat", Constants.NO_ACTION);
                    }
                    drawer.close();

                }
                if (id == R.id.nav_dr_settings) {

                    SettingsFragment settingsFragment = (SettingsFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_SETTINGS_FRAGMENT);
                    if (settingsFragment == null) {
                        FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                        FragmentController.addSettingsFragment(TestMainActivity.this, "");
                    }
                    drawer.close();

                }
                if (id == R.id.nav_sign_out) {

                    showYesNoDialog1(getString(R.string.str_logout), BaseActivity.LOGOUT_DLG, new onUserPick() {
                        @Override
                        public void userPick(Boolean yes) {
                            if (yes)
                                if (CommonUtills.isNetworkAvailable(TestMainActivity.this)) {
                                    //callUpdateNotification("0");
                                    callUserLogout();
                                } else {
                                    CommonUtills.showAlert(TestMainActivity.this, getString(R.string.no_internet_connection), Constants.NO_ACTION);
                                }
                        }
                    });

                }

                return true;
            }
        });

    }

    protected void onResume() //Activated
    {
        super.onResume();
        _Current = this;


        CommonUtills.dismissProgressDialog();

    }

    protected void onPause() //Deactivated
    {
        super.onPause();

        _Current = null;
    }
    public void hideActionBarTitle(boolean showTitle) {
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
    }

    private void callUserLogout() {
        CommonUtills.showProgressDialog(this, getString(R.string.str_pleasewait), false);
        final Call<CommonVo> callUserLogout = apiInterface.logout(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(this), MissUGram.getApp(this) == null ? 0 : MissUGram.getApp(this).getUserModel().getId());
        callUserLogout.enqueue(new Callback<CommonVo>() {
            @Override
            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {

                if (response != null && response.body() != null) {
                    CommonVo commonVo = response.body();
                    if (commonVo.getsStatus() == Constants.STATUS_SUCCESS) {
                        CommonUtills.setPrefIsNotificationEnabled(TestMainActivity.this, false);
                        CommonUtills.clearAllNotification(TestMainActivity.this);
                        ChatProvider.getInstance().logoutUser();
                        clearAndLogout();
                    } else {
                        CommonUtills.showAlert(TestMainActivity.this, commonVo.getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }
    public QBSystemMessagesManager getSystemMessagesManager() {
        return systemMessagesManager;
    }

    private void setIncomingMessageListener() {
        QBUser user1 = new QBUser(CommonUtills.getPrefCurrentChatUname(TestMainActivity.this), CommonUtills.getPrefCurrentChatUname(TestMainActivity.this));
        user1.setId(CommonUtills.getPrefChatId(TestMainActivity.this));
        user1.setPassword(CommonUtills.getPrefCurrentChatUname(TestMainActivity.this));
        user1.setEmail(CommonUtills.getPrefCurrentChatUname(TestMainActivity.this));
        new Thread(() -> {
            if (!QBChatService.getInstance().isLoggedIn()) {


                QBChatService.getInstance().login(user1, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        dispatchBadge(user1);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        e.getErrors();
                    }
                });


            } else {
                dispatchBadge(user1);
            }


        }).start();


    }

    private void dispatchBadge(QBUser user1) {
        if (incomingMessagesManager == null) {
            incomingMessagesManager = QBChatService.getInstance().getIncomingMessagesManager();
        }
        if (systemMessagesManager == null) {
            systemMessagesManager = QBChatService.getInstance().getSystemMessagesManager();

            // setGroupCreationListener();
        }
        incomingMessagesManager.addDialogMessageListener(new QBChatDialogMessageListener() {
            @Override
            public void processMessage(String dialogId, QBChatMessage qbChatMessage, Integer senderId) {


                if (senderId != user1.getId()) {
                    if (FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT) instanceof GrievanceChatFragment) {
                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);


                        QBRestChatService.getChatDialogById(dialogId).performAsync(new QBEntityCallback<QBChatDialog>() {
                            @Override
                            public void onSuccess(QBChatDialog mqbChatDialog, Bundle bundle) {


                                if (qbChatMessage.getSenderId() != null && qbChatMessage.getSenderId() != user1.getId()) {

                                    QBUsers.getUser(qbChatMessage.getSenderId()).performAsync(new QBEntityCallback<QBUser>() {
                                        @Override
                                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                                            ChatUserHistoryModel model = new ChatUserHistoryModel();

                                            if (mqbChatDialog.getType() == QBDialogType.PRIVATE) {
                                                int id = qbUser.getId() == null ? -1 : qbUser.getId();

                                                model.setChatuser_name(qbUser.getFullName() == null ? qbUser.getLogin() : qbUser.getFullName());
                                                model.setChatuser_id(id);
                                                model.setChatuser_fileid(qbUser.getFileId() == null ? -1 : qbUser.getFileId());
                                                model.setType(mqbChatDialog.getType() == QBDialogType.PRIVATE ? QBDialogType.PRIVATE : QBDialogType.GROUP);
                                                model.setChatuser_status(mqbChatDialog.getLastMessage());
                                                model.setUnread_messagecount(mqbChatDialog.getUnreadMessageCount());
                                            } else {
                                                // messages for a group will only be updated If group has been joined once but if you need forcefully you can join every dialog in a loop to get the updated messages from a listener which works fine but not a convenient option to do.
                                                model.setType(QBDialogType.GROUP);
                                                model.setChatDialog(mqbChatDialog);
                                                model.setUnread_messagecount(mqbChatDialog.getUnreadMessageCount());
                                                model.setChatuser_status(mqbChatDialog.getLastMessage());
                                                model.setLastMessageDateSentPrivateChat(mqbChatDialog.getLastMessageDateSent());
                                            }


                                            grievanceChatFragment.callAndUpdate(model);
                                        }

                                        @Override
                                        public void onError(QBResponseException e) {
                                            e.getLocalizedMessage();

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                e.getLocalizedMessage();
                            }
                        });

                    }
                }


            }

            @Override
            public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {

            }
        });

    }


    private void checkForNotification() {
        if (getIntent().getStringExtra(Constants.KEY_PUSH_NOTIFICATIONID) != null) {
            UserModel userModel = new UserModel();

            userModel.setId(CommonUtills.getPrefId(this));
            userModel.setFirst_name(CommonUtills.getPrefFirstName(this));
            userModel.setLast_name(CommonUtills.getPrefFirstName(this));
            userModel.setAvatar(CommonUtills.getPrefFirstName(this));
            userModel.setPush_flag(CommonUtills.getPrefisFromNotification(this) ? "1" : "0");
            userModel.setEmail(CommonUtills.getPrefUserEmail(this));
            userModel.setPassword(CommonUtills.getPrefUserPassword(this));
            userModel.setPhone(CommonUtills.getPrefUserEmail(this));
            userModel.setToken(CommonUtills.getPrefToken(this));


            MissUGram.getApp(this).setUserModel(userModel);
            String id = getIntent().getStringExtra(Constants.KEY_PUSH_NOTIFICATIONID);
            if (id.equalsIgnoreCase("1")) {
                CommonUtills.setPrefIsFromNotification(TestMainActivity.this, true);
                FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MEMORIAL_LISTING);

                FetchContacts fetchContacts = new FetchContacts(this, getSupportLoaderManager());


            } else if (id.equalsIgnoreCase("2")) {
                memid = getIntent().getStringExtra(Constants.KEY_PUSH_MEMORIAMID);
                FragmentController.addCommentsListingFragment(TestMainActivity.this, Integer.valueOf(memid), Constants.FROM_MY_MEMORIAL);
                CommonUtills.setPrefIsFromNotification(TestMainActivity.this, true);
            }
        } else {
            FragmentController.addMemorialFragment(this, Constants.FROM_MEMORIAL_LISTING);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }

        return true;
    }

    public void setData() {

        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView1.getHeaderView(0);
        tv_name = headerView.findViewById(R.id.tv_name);
        tv_email = headerView.findViewById(R.id.tv_email);
        iv_profile = headerView.findViewById(R.id.iv_profile);
        if (MissUGram.getApp(this).getUserModel() != null) {
            tv_name.setText(MissUGram.getApp(this).getUserModel().getFirst_name() + " " + MissUGram.getApp(this).getUserModel().getLast_name());
            tv_email.setText(MissUGram.getApp(this).getUserModel().getEmail());
            ImageLoader.getInstance().displayImage(MissUGram.getApp(this).getUserModel().getAvatar(), iv_profile, CommonUtills.getRoundImageOptionsForMenuProfile());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      //  getMenuInflater().inflate(R.menu.test_main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_test_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolbarIcon:
                switch (FragmentController.getFrontFragmentTag(TestMainActivity.this)) {
                    case FragmentController.TAG_MY_MEMORIAL_FRAGMENT:
                        TestMyMemorialFragment mainFragment1 = (TestMyMemorialFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_MY_MEMORIAL_FRAGMENT);
                        if (mainFragment1 != null) {
                            mainFragment1.showSearchBar();
                        }
                        break;
                    case FragmentController.TAG_MEMORIAL_FRAGMENT:
                        TestMemorialFragment mainFragment = (TestMemorialFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT);
                        if (mainFragment != null) {
                            mainFragment.showSearchBar();
                        }
                        break;
                    case FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT:
                        CreateMemorium createMemorium = (CreateMemorium) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                        if (createMemorium != null) {
                            createMemorium.doProcessCreateMemorium();
                        }

                        break;

                    case FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT:
                        FriendsAndFamilyFragment friendsAndFamilyFragment = (FriendsAndFamilyFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                        if (friendsAndFamilyFragment != null) {

                            if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                                friendsAndFamilyFragment.collectAndOpenCreateGroup();
                            } else {
                                friendsAndFamilyFragment.senInvitation();
                            }
                        }
                        break;
                    case FragmentController.TAG_DESCRIPTION_FRAGMENT:
                        DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                        if (descriptionFragment != null) {
                            descriptionFragment.openCreateMemoriumForEdit();
                        }
                        break;
                    case FragmentController.TAG_GRIEVANCE_FRAGMENT:
                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                        if (grievanceChatFragment != null) {
                            FragmentController.addFriendListFragment(TestMainActivity.this, Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS, null);
                        }
                        break;
                    case FragmentController.TAG_CREATE_GROUP_FRAGMENT:
                        CreateGroup createGroup = (CreateGroup) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CREATE_GROUP_FRAGMENT);
                        if (createGroup != null) {
                            createGroup.validateData();
                        }
                        break;
                    case FragmentController.TAG_CHAT_FRAGMENT:
                        ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                        if (chatFragment != null) {
                            chatFragment.removeUser();
                        }
                        break;
                }
                break;

            case R.id.ivToolbarIconChatUser:
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                ivToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(TestMainActivity.this, R.drawable.group));
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (drawer.isOpen()) {
            drawer.close();
        } else {
            if (isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT) instanceof TestMemorialFragment) {
                showYesNoDialog1(getString(R.string.str_suretoexit), BaseActivity.EXIT_DLG, yes -> {
                    if (yes)
                        finish();
                });
            }
            else if (isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT) instanceof TestMyMemorialFragment) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_MEMORIAL_FRAGMENT);
                if (CommonUtills.getPrefisFromNotification(TestMainActivity.this)) {
                    CommonUtills.setPrefIsFromNotification(TestMainActivity.this, false);
                    FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MEMORIAL_LISTING);
                } else {
                    new Handler().postDelayed(() -> updateToolbar(), 250);
                }

            }


            else if (isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT) instanceof FriendsAndFamilyFragment && currentFriendList == Constants.FRIEND_LIST_FROM_FROM_INVITE) {
                if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                    FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                    // if opened from grievance chat and pressing back we have to show the add group icon so calling the initUI.
                    if (FragmentController.getFrontFragmentTag(TestMainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                        GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(TestMainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                        grievanceChatFragment.initUI();
                    }
                } else {
                    FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
                }
                hideKeyboard();

                  new Handler().postDelayed(() -> updateToolbar(), 250);

            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                if (CommonUtills.getPrefisFromNotification(TestMainActivity.this)) {
                    CommonUtills.setPrefIsFromNotification(TestMainActivity.this, false);
                    FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MEMORIAL_LISTING);
                } else {
                      new Handler().postDelayed(() -> updateToolbar(), 250);
                }
            }else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                if (CommonUtills.getPrefisFromNotification(TestMainActivity.this)) {
                    CommonUtills.setPrefIsFromNotification(TestMainActivity.this, false);
                    FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MY_MEMORIAL);
                } else {
                    new Handler().postDelayed(() -> updateToolbar(), 250);
                }
            }
            else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT)) {

                if (getCurrentMemorialVal() == Constants.FROM_MYMEMORIAL_EDIT) {

                    MemorialVo memorialVo = null;
                    if (FragmentController.isThisFragmentInFront(this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT) instanceof CreateMemorium) {
                        CreateMemorium createMemorium = (CreateMemorium) FragmentController.isThisFragmentInFront(this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                        memorialVo = createMemorium.getmMemoriamVo();
                        FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                    }
                    final MemorialVo finalMemorialVo = memorialVo;
                    new Handler().postDelayed(() -> {
                        if (FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT) instanceof DescriptionFragment) {
                            DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                            descriptionFragment.setmMemoriamVo(finalMemorialVo);

                        }
                    }, 200);

                } else {

                    if (getCurrentMemorialVal() == Constants.FROM_MY_MEMORIAL) {
                        FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
                    }
                }
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_COMMENTSLISTING_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_COMMENTSLISTING_FRAGMENT);

                if (CommonUtills.getPrefisFromNotification(TestMainActivity.this)) {

                    // call the memorial detail ws and fill the vo
                    if (!memid.isEmpty() && memid != null) {
                        callDetailFromMemorium(memid);
                    }
                }


            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_EDIT_PROFILE_FRAGMENT) && getIsFromSettings() == Constants.FROM_SETTINGS) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_EDIT_PROFILE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_INFORMATION_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHAT_FRAGMENT)) {
                ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to update the chat based on the dialog in grivience screen we are getting the dialog and making a query in quickblox.

                QBChatDialog chatDialogGroup = null;
                QBChatDialog chatDialogPrivate = null;
                if (chatFragment != null) {
                    chatDialogGroup = chatFragment.getQbChatDialogFoGroupChat();
                    chatDialogPrivate = chatFragment.getQbChatDialogFoPrivaterChat();
                }
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);

                ivToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(TestMainActivity.this, R.drawable.group));

                QBChatDialog finalChatDialogGroup = chatDialogGroup;
                QBChatDialog finalChatDialogPrivate = chatDialogPrivate;
                new Handler().postDelayed(new Runnable() {
                                              @Override
                                              public void run() {
                                                  if (FragmentController.getFrontFragmentTag(TestMainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                                                      GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(TestMainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                                                      grievanceChatFragment.initUI();
                                                      grievanceChatFragment.checkIfNewMessageArrived(finalChatDialogPrivate);
                                                      grievanceChatFragment.checkIfNewMessageArrivedForGroup(finalChatDialogGroup);
                                                  }

                                              }
                                          }
                        , 250);

            } else {
                FragmentController.removeAllFragmentsFromStack(TestMainActivity.this);
                FragmentController.addMemorialFragment(TestMainActivity.this, Constants.FROM_MEMORIAL_LISTING);
            }

            if (!CommonUtills.getPrefisFromNotification(TestMainActivity.this)) {
                   new Handler().postDelayed(() -> updateToolbar(), 250);
            }

        }
    }

    private void callDetailFromMemorium(String id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        CommonUtills.showProgressDialog(this, getString(R.string.str_pleasewait), false);
        Call<MemorialDetailSingleResponseVo> mDetail = apiInterface.getDetailFromMemoriumId(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(this), ApiClient.BASE_ADDRESS + "memoriam/" + id);
        mDetail.enqueue(new Callback<MemorialDetailSingleResponseVo>() {
            @Override
            public void onResponse(Call<MemorialDetailSingleResponseVo> call, Response<MemorialDetailSingleResponseVo> response) {
                CommonUtills.dismissProgressDialog();
                if (response != null && response.body() != null) {
                    MemorialDetailSingleResponseVo memorialDetail = response.body();

                    if (memorialDetail.getsStatus() == Constants.STATUS_SUCCESS) {
                        memid = "";
                        FragmentController.addDescriptionFragment(TestMainActivity.this, memorialDetail.getMemorialinfo(), Constants.FROM_MY_MEMORIAL);
                    } else {
                        CommonUtills.showAlert(TestMainActivity.this, response.body().getsMessage(), Constants.NO_ACTION);
                    }
                }
            }

            @Override
            public void onFailure(Call<MemorialDetailSingleResponseVo> call, Throwable t) {
                CommonUtills.dismissProgressDialog();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }

    private void getPermissionWriteContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_WRITE_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }

    private void getPermissionReadContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        }
    }
        public void updateToolbar() {

        switch (FragmentController.getFrontFragmentTag(this)) {
            case FragmentController.TAG_FIRST_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.first_fragment));
                setToolbarIcon(ic_menu_white_24dp);
                ivToolbarIcon.setVisibility(View.VISIBLE);
                break;
            case FragmentController.TAG_MEMORIAL_FRAGMENT:
                setToolbarIcon(ic_menu_white_24dp);
                ivToolbarIcon.setVisibility(View.VISIBLE);
                if (currentMemorial == Constants.FROM_MEMORIAL_LISTING) {
                    setToolbarTitle(getString(R.string.memorial_fragment));
                    ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));

                }
                else {
                    setToolbarTitle(getString(R.string.my_memorial));
                    spe_search.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));

                    //spe_search.setVisibility(View.GONE);

                }

                break;
//              case FragmentController.TAG_MY_MEMORIAL_FRAGMENT:
//                setToolbarIcon(ic_menu_white_24dp);
//                ivToolbarIcon.setVisibility(View.VISIBLE);
//                if (currentMemorial == Constants.FROM_MY_MEMORIAL) {
//                    setToolbarTitle(getString(R.string.my_memorial));
//                }
//                ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));
//
//                break;


            case FragmentController.TAG_DESCRIPTION_FRAGMENT:
                if (FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT) instanceof DescriptionFragment) {
                    DescriptionFragment descriptionFragment = (DescriptionFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
                    tvToolbarTitle.setText(descriptionFragment.getmMemoriamVo().getTitle());
                }
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.VISIBLE);
                ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_draft));
                if (getCurrentMemorialVal() == Constants.FROM_MY_MEMORIAL || getCurrentMemorialVal() == Constants.FROM_MYMEMORIAL_EDIT) {
                    ivToolbarIcon.setVisibility(View.VISIBLE);
                } else {
                    ivToolbarIcon.setVisibility(View.GONE);
                }

                break;
            case FragmentController.TAG_SETTINGS_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.settings_fragment));
                setToolbarIcon(ic_menu_white_24dp);
                ivToolbarIcon.setVisibility(View.GONE);

                break;
            case FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.create_memorium_fragment));
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.VISIBLE);
                ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
                break;
            case FragmentController.TAG_COMMENTSLISTING_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.comments_fragment));
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.GONE);
                ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
                break;

            case FragmentController.TAG_EDIT_PROFILE_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.str_profile));
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.GONE);
                break;

            case FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT:


                if (getIsFromGroupChat() == Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS) {
                    tvToolbarTitle.setText("Contact List");
                    setToolbarIcon(ic_arrow_back_white_24dp);

                } else {
                    setToolbarTitle(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? getString(R.string.friends_and_family) : getString(R.string.str_header_invitecontacts));
                    setToolbarIcon(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? ic_menu_white_24dp : ic_arrow_back_white_24dp);
                    ivToolbarIcon.setVisibility(currentFriendList == Constants.FRIEND_LIST_FROM_FROM_MAIN ? View.GONE : View.VISIBLE);

                }

                ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
               break;
            case FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT:
                tvToolbarTitle.setText(getString(R.string.str_changepasswored));
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.GONE);
                break;
            case FragmentController.TAG_INFORMATION_FRAGMENT:

                if (getIsFromSlug() == Constants.FROM_SLUG) {
                    setToolbarIcon(ic_arrow_back_white_24dp);
                    ivToolbarIcon.setVisibility(View.GONE);
                } else if (getIsFromSlug() == Constants.FROM_SEARCH_DECEASED) {

                    ivToolbarIcon.setVisibility(View.GONE);
                } else if (getIsFromSlug() == Constants.FROM_SEARCH_QUOTE) {

                    ivToolbarIcon.setVisibility(View.GONE);
                } else {

                    setToolbarIcon(ic_menu_white_24dp);
                    ivToolbarIcon.setVisibility(View.GONE);
                }
                break;
            case FragmentController.TAG_GRIEVANCE_FRAGMENT:

                if (Constants.FROM_GROUP_CHAT_TAB == getIsFromGroupChat()||Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS==getIsFromGroupChat()) {
                    tvToolbarTitle.setText(getString(R.string.grievance_fragment));
                    setToolbarIcon(ic_menu_white_24dp);
                    ivToolbarIcon.setVisibility(View.VISIBLE);
                    ivToolbarIconChatUser.setVisibility(View.GONE);
                    ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_white_24dp));
                } else {
                    tvToolbarTitle.setText(getString(R.string.grievance_fragment));
                    setToolbarIcon(ic_menu_white_24dp);
                    ivToolbarIcon.setVisibility(View.GONE);
                    ivToolbarIconChatUser.setVisibility(View.GONE);
                }

                break;
            case FragmentController.TAG_CHAT_FRAGMENT:

                tvToolbarTitle.setText(getString(R.string.dr_chat));
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.GONE);
                ivToolbarIconChatUser.setVisibility(View.GONE);
                break;

            case FragmentController.TAG_CREATE_GROUP_FRAGMENT:
                tvToolbarTitle.setText("Create Group");
                setToolbarIcon(ic_arrow_back_white_24dp);
                ivToolbarIcon.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void setToolbarIcon(int ic_list_white_24dp) {
        mToolbar.setNavigationIcon(ic_list_white_24dp);

    }
    public void setCurrentMemoriamVal(int val) {
        currentMemorial = val;
    }

    public void setCurrentFriendListVal(int val) {
        currentFriendList = val;
    }

    public int getIsFromGroupChat() {
        return isFromGroupChat;
    }

    public void setIsFromGroupChat(int isFromGroupChat) {
        this.isFromGroupChat = isFromGroupChat;
    }

    public int getCurrentMemorialVal() {
        return currentMemorial;
    }

    public int getIsFromSettings() {
        return isFromSettings;
    }

    public void setIsFromSettings(int isFromSettings) {
        this.isFromSettings = isFromSettings;
    }

    public int getIsFromSlug() {
        return isFromSlug;
    }

    public void setIsFromSlug(int isFromSlug) {
        this.isFromSlug = isFromSlug;
    }

    private void clearAndLogout() {


        FragmentController.removeAllFragmentsFromStack(this);
        this.getSharedPreferences(Constants.MISSU_GRAM_PREF, Context.MODE_PRIVATE).edit().clear().apply();
        Constants.BY_PASS_SPLASH = true;
        this.startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        CommonUtills.dismissProgressDialog();
    }

    public void setToolbarTitle(String title) {
        tvToolbarTitle.setText(title);
        // to prevent title appear in a capital words. it was doen all caps for chat fragment.
        tvToolbarTitle.setAllCaps(false);
    }

        public void initChatUI(String title, String imageId, int type) {

        //  setToolbarTitle(title);
        tvToolbarTitle.setAllCaps(true);
        tvToolbarTitle.setText(title);
        if (type == Constants.FOR_GROUP_CHAT) {
            ivToolbarIcon.setVisibility(View.VISIBLE);
            ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.exit_group));
        } else {
            ivToolbarIcon.setVisibility(View.GONE);
            ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_userplus));
        }

        ivToolbarIconChatUser.setVisibility(View.VISIBLE);
// since quicklblox returns the null word in getPhoto() in string so checked as below.
        if (!imageId.equalsIgnoreCase("null") && !TextUtils.isEmpty(imageId) && !imageId.equalsIgnoreCase("-1")) {


            ChatProvider.getInstance().getImageUrlFromFileId(Integer.valueOf(imageId), new QBEntityCallback<QBFile>() {
                @Override
                public void onSuccess(QBFile qbFile, Bundle bundle) {
                    ImageLoader.getInstance().displayImage(qbFile.getPublicUrl(), ivToolbarIconChatUser,type==Constants.FOR_GROUP_CHAT? CommonUtills.getRoundImageOptionsForGroup():CommonUtills.getRoundImageOptions());
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            return true;
//        } else if (id == android.R.id.home) {
//            if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
//            } else if (currentFriendList == Constants.FRIEND_LIST_FROM_FROM_INVITE && FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_COMMENTSLISTING_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_COMMENTSLISTING_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_EDIT_PROFILE_FRAGMENT) && getIsFromSettings() == Constants.FROM_SETTINGS) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_EDIT_PROFILE_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_INFORMATION_FRAGMENT) && getIsFromSlug() == Constants.FROM_SLUG) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {
//                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHAT_FRAGMENT)) {
//                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
//                //to refresh the list of count and user in the chat re add the fragment below.
//
//                ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
//                //to update the chat based on the dialog in grievance screen we are getting the dialog and making a query in quickblox.
//
//                QBChatDialog chatDialogGroup = null;
//                QBChatDialog chatDialogPrivate = null;
//                if (chatFragment != null) {
//                    chatDialogGroup = chatFragment.getQbChatDialogFoGroupChat();
//                    chatDialogPrivate = chatFragment.getQbChatDialogFoPrivaterChat();
//                }
//                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
//
//                ivToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(TestMainActivity.this, R.drawable.group));
//
//                QBChatDialog finalChatDialogGroup = chatDialogGroup;
//                QBChatDialog finalChatDialogPrivate = chatDialogPrivate;
//                new Handler().postDelayed(new Runnable() {
//                                              @Override
//                                              public void run() {
//                                                  if (FragmentController.getFrontFragmentTag(MainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {
//
//                                                      GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(MainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
//                                                      grievanceChatFragment.initUI();
//                                                      grievanceChatFragment.checkIfNewMessageArrived(finalChatDialogPrivate);
//                                                      grievanceChatFragment.checkIfNewMessageArrivedForGroup(finalChatDialogGroup);
//                                                  }
//
//                                              }
//                                          }
//                        , 250);
//
//
//            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CREATE_GROUP_FRAGMENT)) {
//                setIsFromGroupChat(Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS);
//                setToolbarTitle("Contact List");
//                setToolbarIcon(ic_arrow_back_white_24dp);
//                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CREATE_GROUP_FRAGMENT);
//                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
//
//            } else {
//               // mDrawer.toggleMenu();
//            }
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_DESCRIPTION_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_DESCRIPTION_FRAGMENT);
            } else if (currentFriendList == Constants.FRIEND_LIST_FROM_FROM_INVITE && FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_COMMENTSLISTING_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_COMMENTSLISTING_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_CREATE_MEMORIUM_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_CHANGE_PASSWORD_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_EDIT_PROFILE_FRAGMENT) && getIsFromSettings() == Constants.FROM_SETTINGS) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_EDIT_PROFILE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_INFORMATION_FRAGMENT) && getIsFromSlug() == Constants.FROM_SLUG) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_INFORMATION_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {
                FragmentController.removeThisFragment(TestMainActivity.this, FragmentController.TAG_GRIEVANCE_FRAGMENT);
            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CHAT_FRAGMENT)) {
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to refresh the list of count and user in the chat re add the fragment below.

                ChatFragment chatFragment = (ChatFragment) FragmentController.isThisFragmentInFront(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);
                //to update the chat based on the dialog in grievance screen we are getting the dialog and making a query in quickblox.

                QBChatDialog chatDialogGroup = null;
                QBChatDialog chatDialogPrivate = null;
                if (chatFragment != null) {
                    chatDialogGroup = chatFragment.getQbChatDialogFoGroupChat();
                    chatDialogPrivate = chatFragment.getQbChatDialogFoPrivaterChat();
                }
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CHAT_FRAGMENT);

                ivToolbarIconChatUser.setImageDrawable(ContextCompat.getDrawable(TestMainActivity.this, R.drawable.group));

                QBChatDialog finalChatDialogGroup = chatDialogGroup;
                QBChatDialog finalChatDialogPrivate = chatDialogPrivate;
                new Handler().postDelayed(new Runnable() {
                                              @Override
                                              public void run() {
                                                  if (FragmentController.getFrontFragmentTag(TestMainActivity._Current).equalsIgnoreCase(FragmentController.TAG_GRIEVANCE_FRAGMENT)) {

                                                      GrievanceChatFragment grievanceChatFragment = (GrievanceChatFragment) FragmentController.getFragmentByTag(TestMainActivity._Current, FragmentController.TAG_GRIEVANCE_FRAGMENT);
                                                      grievanceChatFragment.initUI();
                                                      grievanceChatFragment.checkIfNewMessageArrived(finalChatDialogPrivate);
                                                      grievanceChatFragment.checkIfNewMessageArrivedForGroup(finalChatDialogGroup);
                                                  }

                                              }
                                          }
                        , 250);


            } else if (FragmentController.getFrontFragmentTag(TestMainActivity.this).equals(FragmentController.TAG_CREATE_GROUP_FRAGMENT)) {
                setIsFromGroupChat(Constants.FROM_GROUP_CHAT_TAB_TO_FRIENDS);
                setToolbarTitle("Contact List");
                setToolbarIcon(ic_arrow_back_white_24dp);
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_CREATE_GROUP_FRAGMENT);
                FragmentController.removeFragment(TestMainActivity.this, FragmentController.TAG_FRIEND_AND_FAMILY_FRAGMENT);

            } else {
               // mDrawer.toggleMenu();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateToolbarr() {
        setToolbarIcon(ic_menu_white_24dp);
        ivToolbarIcon.setVisibility(View.VISIBLE);
        if (currentMemorial == Constants.FROM_MEMORIAL_LISTING) {
            setToolbarTitle(getString(R.string.memorial_fragment));
            ivToolbarIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));
        }
    }

    public void updateToolbarrr() {
        setToolbarIcon(ic_menu_white_24dp);
        ivToolbarIcon.setVisibility(View.VISIBLE);
        setToolbarTitle(getString(R.string.my_memorial));
        spe_search.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_icon));

    }


//    @Override
//    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        if (id == CONTACTS_LOADER_ID) {
//            return contactsLoader();
//        }
//        return null;
//    }
//
//    private Loader<Cursor> contactsLoader() {
//        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI; // The content
//        return new CursorLoader(getApplicationContext(), contactsUri, null, null, null, null);
//    }
//
//
//    @Override
//    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//        if (data != null) {
//            contactsFromCursor(data);
//        }
//    }
//
//    private void contactsFromCursor(Cursor cursor) {
//        if (cursor.getCount() > 0) {
//            cursor.moveToFirst();
//            do {
//                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//
//
//                String conName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                Cursor emailCur = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
//                while (emailCur.moveToNext()) {
//                    ContactVo m_contactsVo = new ContactVo();
//                    m_contactsVo.setName(conName);
//
//                    String email = emailCur.getString(
//                            emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                    m_contactsVo.setContact(email);
//                    contactVos.add(m_contactsVo);
//
//                }
//                Cursor phoneCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
//                if (phoneCur == null)
//                    break;
//                while (phoneCur.moveToNext()) {
//                    ContactVo m_contactsVo = new ContactVo();
//                    m_contactsVo.setName(conName);
//                    String email = phoneCur.getString(
//                            phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                    m_contactsVo.setContact(email);
//                    contactVos.add(m_contactsVo);
//
//                }
//            } while (cursor.moveToNext());
//
//
//            sendContactToServer();
//        }
//
//    }
//
//    private void sendContactToServer() {
//        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        ContactRequestVo contactRequestVo = new ContactRequestVo();
//        if (MissUGram.getApp(this).getUserModel() == null) {
//            contactRequestVo.setUserid(CommonUtills.getPrefId(this));
//        } else {
//            contactRequestVo.setUserid(MissUGram.getApp(this).getUserModel().getId());
//        }
//        contactRequestVo.setFriend_list(contactVos);
//
//        final Call<CommonVo> contactResponse = apiInterface.submitContactDetail(Constants.KEY_BEARER + TokenKeeper.getInstance().getAccessToken(TestMainActivity.this), contactRequestVo);
//        contactResponse.enqueue(new Callback<CommonVo>() {
//            @Override
//            public void onResponse(Call<CommonVo> call, Response<CommonVo> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<CommonVo> call, Throwable t) {
//
//            }
//        });
//
//    }
//
//
//
//    @Override
//    public void onLoaderReset(Loader<Cursor> loader) {
//
//    }
//
//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
 //   }
}