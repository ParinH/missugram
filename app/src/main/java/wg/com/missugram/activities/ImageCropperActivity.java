package wg.com.missugram.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.wg.framework.log.CustomLogHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import wg.com.missugram.R;
import wg.com.missugram.utils.CommonUtills;
import wg.com.missugram.utils.MultiTouchListener;


public class ImageCropperActivity extends Activity {


    Bundle mBundle;
    String mImagePath;
    ImageView mImageView;
    TextView mDone, mCancel;
    ImageView imageView;
    FrameLayout mCroppedImageView;
    LinearLayout mLinearLayout;
    int maxWidth, maxHeight;

    public static void showDialog(final Context p_context, final String p_dialogMessage, final boolean p_isCancelButton) {
        ((Activity) p_context).runOnUiThread(new Runnable() {


            @Override
            public void run() {
                if (p_context != null) {
                    final AlertDialog.Builder m_alertDialog = new AlertDialog.Builder(p_context);
                    final AlertDialog m_dialog = m_alertDialog.create();
                    m_dialog.setTitle(p_context.getResources().getString(R.string.app_name));
                    m_dialog.setMessage(p_dialogMessage);
                    m_dialog.setCanceledOnTouchOutside(false);

                    m_dialog.setButton(AlertDialog.BUTTON_POSITIVE, p_context.getResources().getString(R.string.dialog_yes_button), new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            m_dialog.getButton(which).setEnabled(false);
                            Intent mFilePathIntent = new Intent();
                            ((Activity) p_context).setResult(Activity.RESULT_OK, mFilePathIntent);
                            ((Activity) p_context).finish();
                        }
                    });

                    m_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {


                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });

                    if (p_isCancelButton) {
                        m_dialog.setButton(AlertDialog.BUTTON_NEGATIVE, p_context.getResources().getString(R.string.dialog_no_button), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                m_dialog.dismiss();
                            }

                        });
                    }
                    m_dialog.show();

                }

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_cropper);

        mBundle = getIntent().getExtras();
        imageView = (ImageView) findViewById(R.id.imageView);
        mCroppedImageView = (FrameLayout) findViewById(R.id.ivCropView);
        mLinearLayout = (LinearLayout) findViewById(R.id.llView);
        mDone = (TextView) findViewById(R.id.done);
        mCancel = (TextView) findViewById(R.id.cancel);
        if (mBundle != null) {
            mImagePath = mBundle.getString("path");
        }


        CommonUtills.showProgressDialog(this, getString(R.string.str_pleasewait), true);


        mCroppedImageView.post(new Runnable() {


            @Override
            public void run() {
                try {
                    maxWidth = mCroppedImageView.getWidth();
                    maxHeight = mCroppedImageView.getHeight();

                   /* Glide.with(ImageCropperActivity.this)
                            .load(Uri.fromFile(new File(mImagePath)))
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                        CommonUtills.dismissProgressDialog();

                                    imageView.setImageBitmap(resource);
                                }
                            });*/


                } catch (Throwable t) {
                    CustomLogHandler.printErrorlog(t);
                }

                CommonUtills.dismissProgressDialog();
                // ImageLoader.getInstance().displayImage("file:/" + new File(mImagePath).getAbsolutePath(), imageView);
                maxWidth = mCroppedImageView.getWidth();
                maxHeight = mCroppedImageView.getHeight();

                RequestOptions mRequestOptions = new RequestOptions();
                mRequestOptions.dontTransform();
                mRequestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
                mRequestOptions.centerCrop();

                Glide.with(imageView.getContext())
                        .load(Uri.fromFile(new File(mImagePath))).apply(mRequestOptions)
                        .into(imageView);


            }
        });
        imageView.setOnTouchListener(new MultiTouchListener(false, true, true));


        mDone.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                // mImageView.setImageBitmap(mCropImageView.getCroppedImage());
                // mCropImageView.setVisibility(View.GONE);
                try {
                    mLinearLayout.setDrawingCacheEnabled(true);
                    Bitmap bitmap = Bitmap.createBitmap(mLinearLayout.getDrawingCache());
                    mLinearLayout.setDrawingCacheEnabled(false);
                    Bitmap mBitmap = Bitmap.createBitmap(bitmap, (int) mCroppedImageView.getX(), (int) mCroppedImageView.getY(), mCroppedImageView.getWidth(), mCroppedImageView.getHeight());
                    Intent mFilePathIntent = new Intent();
                    //mFilePathIntent.putExtra("path", saveImage(mBitmap));
                    mFilePathIntent.putExtra("path", saveToInternalStorage(mBitmap));
                    (ImageCropperActivity.this).setResult(Activity.RESULT_OK, mFilePathIntent);
                    finish();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialog(ImageCropperActivity.this, getString(R.string.are_sure_want_to_discard), true);
            }
        });
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private Bitmap scaleBitmap(Bitmap bm) {
        try {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float ratio = 1;

            if (width > height) {
                // landscape
                // ratio = (float) width / maxWidth;
                width = maxWidth;
                height = (int) (height / ratio);
            } else if (height > width) {
                // portrait
                // float ratio = (float) height / maxHeight;
                height = maxHeight;
                width = (int) (width / ratio);
            } else {
                // square
                height = maxHeight;
                width = maxWidth;
            }

            Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);

            bm = Bitmap.createScaledBitmap(bm, width, height, true);
            return bm;
        } catch (OutOfMemoryError oom) {
            oom.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        showDialog(ImageCropperActivity.this, getString(R.string.are_sure_want_to_discard), true);
        // super.onBackPressed();
    }
}
