package wg.com.missugram.model;


public class CommentsVo extends CommonVo {

    private String comment;
    private String user_id;
    private int memorial_id;
    private String created_at;
    private int is_image;
    private UserModel UserInfo;
    private String comment_id;

    //stores the comments id
     private String id;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getMemorial_id() {
        return memorial_id;
    }

    public void setMemorial_id(int memorial_id) {
        this.memorial_id = memorial_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getIs_image() {
        return is_image;
    }

    public void setIs_image(int is_image) {
        this.is_image = is_image;
    }

    public UserModel getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(UserModel userInfo) {
        UserInfo = userInfo;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
