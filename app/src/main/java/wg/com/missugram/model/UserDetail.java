package wg.com.missugram.model;



public class UserDetail {
    public static String mCounter = "29";
    public String id;
    public String createdAt;
    public String updatedAt;
    public String Did;
    public int frdid;
    public String isOnline;
    public String fullName;
    public String email;
    public String login;
    public String blockid;
    public String qbblockid;
    public int qbid;
    public String phone;
    public String website;
    public String lastRequestAt;
    public String externalId;
    public String facebookId;
    public String twitterId;
    public String twitterDigitsId;
    public String blobId;
    public String tags;
    public String password;
    public String oldPassword;
    public String customData;
    public int qbselectusers;
    private String imageUrl = "";

    public String getDid() {
        return Did;
    }

    public void setDid(String did) {
        Did = did;
    }

    public int getFrdid() {
        return frdid;
    }

    public void setFrdid(int frdid) {
        this.frdid = frdid;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getLastRequestAt() {
        return lastRequestAt;
    }

    public void setLastRequestAt(String lastRequestAt) {
        this.lastRequestAt = lastRequestAt;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getTwitterDigitsId() {
        return twitterDigitsId;
    }

    public void setTwitterDigitsId(String twitterDigitsId) {
        this.twitterDigitsId = twitterDigitsId;
    }

    public String getBlobId() {
        return blobId;
    }

    public void setBlobId(String blobId) {
        this.blobId = blobId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    public String getBlockid() {
        return blockid;
    }

    public void setBlockid(String blockid) {
        this.blockid = blockid;
    }

    public String getQbblockid() {
        return qbblockid;
    }

    public void setQbblockid(String qbblockid) {
        this.qbblockid = qbblockid;
    }

    public int getQbid() {
        return qbid;
    }

    public void setQbid(int qbid) {
        this.qbid = qbid;
    }

    public int getQbselectusers() {
        return qbselectusers;
    }

    public void setQbselectusers(int qbselectusers) {
        this.qbselectusers = qbselectusers;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


}


