package wg.com.missugram.model;

import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;

public class ChatUserHistoryModel {
    public int chatuser_id;
    public String chatuser_name;
    public String chatuser_status;
    public String chatuser_photo;
    public int chatuser_fileid;
    public QBChatDialog chatDialog;
    public QBDialogType type;

    public long getLastMessageDateSentPrivateChat() {
        return lastMessageDateSentPrivateChat;
    }

    public void setLastMessageDateSentPrivateChat(long lastMessageDateSentPrivateChat) {
        this.lastMessageDateSentPrivateChat = lastMessageDateSentPrivateChat;
    }

    public long lastMessageDateSentPrivateChat;

    public int getUnread_messagecount() {
        return unread_messagecount;
    }

    public void setUnread_messagecount(int unread_messagecount) {
        this.unread_messagecount = unread_messagecount;
    }

    public int unread_messagecount;

    public QBChatDialog getChatDialog() {
        return chatDialog;
    }

    public void setChatDialog(QBChatDialog chatDialog) {
        this.chatDialog = chatDialog;
    }

    public QBDialogType getType() {
        return type;
    }

    public void setType(QBDialogType type) {
        this.type = type;
    }

    public int getChatuser_fileid() {
        return chatuser_fileid;
    }

    public void setChatuser_fileid(int chatuser_fileid) {
        this.chatuser_fileid = chatuser_fileid;
    }

    public int getChatuser_id() {
        return chatuser_id;
    }

    public void setChatuser_id(int chatuser_id) {
        this.chatuser_id = chatuser_id;
    }

    public String getChatuser_name() {
        return chatuser_name;
    }

    public void setChatuser_name(String chatuser_name) {
        this.chatuser_name = chatuser_name;
    }

    public String getChatuser_status() {
        return chatuser_status;
    }

    public void setChatuser_status(String chatuser_status) {
        this.chatuser_status = chatuser_status;
    }

    public String getChatuser_photo() {
        return chatuser_photo;
    }

    public void setChatuser_photo(String chatuser_photo) {
        this.chatuser_photo = chatuser_photo;
    }
}
