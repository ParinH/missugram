package wg.com.missugram.model;


public class CommentsSingleResponseVo extends CommonVo {
    CommentsVo commentinfo;

    public CommentsVo getCommentinfo() {
        return commentinfo;
    }

    public void setCommentinfo(CommentsVo commentinfo) {
        this.commentinfo = commentinfo;
    }
}
