package wg.com.missugram.model;

import java.util.ArrayList;

public class ContactResponseVo extends CommonVo{

    private String id;
    private ArrayList<UserModel> friends_list;

    public String getUserid() {
        return id;
    }

    public void setUserid(String userid) {
        this.id = userid;
    }

    public ArrayList<UserModel> getFriends_list() {
        return friends_list;
    }

    public void setFriends_list(ArrayList<UserModel> friends_list) {
        this.friends_list = friends_list;
    }

}
