package wg.com.missugram.model;

import java.util.ArrayList;

public class InviteRequestVo {

    private int sender_user_id;
    private int memorial_id;
    private ArrayList<InviteUserVo> receiver_user_ids;

    public void setSender_user_id(int sender_user_id) {
        this.sender_user_id = sender_user_id;
    }

    public void setReceiver_user_ids(ArrayList<InviteUserVo> receiver_user_ids) {
        this.receiver_user_ids = receiver_user_ids;
    }


    public int getId() {
        return sender_user_id;
    }

    public void setId(int id) {
        this.sender_user_id = id;
    }

    public ArrayList<InviteUserVo> getReceiver_uses() {
        return receiver_user_ids;
    }

    public void setReceiver_uses(ArrayList<InviteUserVo> receiver_uses) {
        this.receiver_user_ids = receiver_uses;
    }

    public int getMemorial_id() {
        return memorial_id;
    }

    public void setMemorial_id(int memorial_id) {
        this.memorial_id = memorial_id;
    }
}
