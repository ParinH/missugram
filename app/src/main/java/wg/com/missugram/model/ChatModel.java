package wg.com.missugram.model;

import com.quickblox.chat.model.QBChatMessage;

public class ChatModel {
    public String firstname;
    public String quickbloxid;
    public int userid;
    public String photo;
    public String status;
    public String groupname;
    public int recieverId;
    public String messageBody;

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public int fileId;
    public QBChatMessage qbChatMessage;

    public int getRecieverId() {
        return recieverId;
    }

    public QBChatMessage getQbChatMessage() {
        return qbChatMessage;
    }

    public void setQbChatMessage(QBChatMessage qbChatMessage) {
        this.qbChatMessage = qbChatMessage;
    }

    public void setRecieverId(int recieverId) {
        this.recieverId = recieverId;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getname() {
        return firstname;
    }

    public void setname(String firstname) {
        this.firstname = firstname;
    }

    public String getQuickbloxid() {
        return quickbloxid;
    }

    public void setQuickbloxid(String quickbloxid) {
        this.quickbloxid = quickbloxid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
}
