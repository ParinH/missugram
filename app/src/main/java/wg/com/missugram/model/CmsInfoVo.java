package wg.com.missugram.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CmsInfoVo implements Parcelable {
    private String description;

    private String content;
    private String page_name;
    private int id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPage_name() {
        return page_name;
    }

    public void setPage_name(String page_name) {
        this.page_name = page_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(description);
        dest.writeString(content);
        dest.writeString(page_name);
        dest.writeInt(id);

    }


    protected CmsInfoVo(Parcel in) {
        this.description = in.readString();
        this.content = in.readString();
        this.page_name = in.readString();
        this.id = in.readInt();
    }


    public static final Parcelable.Creator<CmsInfoVo> CREATOR = new Parcelable.Creator<CmsInfoVo>() {
        @Override
        public CmsInfoVo createFromParcel(Parcel source) {
            return new CmsInfoVo(source);
        }

        @Override
        public CmsInfoVo[] newArray(int size) {
            return new CmsInfoVo[size];
        }
    };

}
