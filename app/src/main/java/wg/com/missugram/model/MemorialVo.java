package wg.com.missugram.model;


import android.os.Parcel;
import android.os.Parcelable;

public class MemorialVo implements Parcelable {


    private String title;
    private String description;
    private String image;
    private int user_id;
    private String category_id;
    private int id;
    private String comment;
    private UserModel UserInfo;



    public void setMemorial_id(int memorial_id) {
        this.id = memorial_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public MemorialVo() {
    }

    public UserModel getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(UserModel userInfo) {
        UserInfo = userInfo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeInt(this.user_id);
        dest.writeString(this.category_id);
        dest.writeInt(this.id);
        dest.writeString(this.comment);
        dest.writeParcelable(this.UserInfo, flags);
    }

    protected MemorialVo(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.image = in.readString();
        this.user_id = in.readInt();
        this.category_id = in.readString();
        this.id = in.readInt();
        this.comment = in.readString();
        this.UserInfo = in.readParcelable(UserModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<MemorialVo> CREATOR = new Parcelable.Creator<MemorialVo>() {
        @Override
        public MemorialVo createFromParcel(Parcel source) {
            return new MemorialVo(source);
        }

        @Override
        public MemorialVo[] newArray(int size) {
            return new MemorialVo[size];
        }
    };
}
