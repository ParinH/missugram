package wg.com.missugram.model;



public class SlugVo extends CommonVo {

    private CmsInfoVo CmsInfo;

    public CmsInfoVo getCmsInfoVo() {
        return CmsInfo;
    }

    public void setCmsInfoVo(CmsInfoVo cmsInfoVo) {
        this.CmsInfo = cmsInfoVo;
    }
}
