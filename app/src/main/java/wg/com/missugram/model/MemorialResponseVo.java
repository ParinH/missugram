package wg.com.missugram.model;

import java.util.ArrayList;


public class MemorialResponseVo extends CommonVo{

    private ArrayList<MemorialVo> memorialinfo;

    public ArrayList<MemorialVo> getMemorialinfo() {
        return memorialinfo;
    }

    public void setMemorialinfo(ArrayList<MemorialVo> memorialinfo) {
        this.memorialinfo = memorialinfo;
    }
}
