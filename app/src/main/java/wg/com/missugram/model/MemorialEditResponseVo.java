package wg.com.missugram.model;


public class MemorialEditResponseVo extends CommonVo{

    private MemorialVo memorialinfo;

    public MemorialVo getMemorialinfo() {
        return memorialinfo;
    }

    public void setMemorialinfo(MemorialVo memorialinfo) {
        this.memorialinfo = memorialinfo;
    }
}
