package wg.com.missugram.model;

import java.util.ArrayList;

public class ContactRequestVo {

    private int userid;
    private ArrayList<ContactVo> friend_list;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public ArrayList<ContactVo> getFriend_list() {
        return friend_list;
    }

    public void setFriend_list(ArrayList<ContactVo> friend_list) {
        this.friend_list = friend_list;
    }

}
