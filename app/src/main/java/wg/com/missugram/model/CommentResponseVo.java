package wg.com.missugram.model;

import java.util.ArrayList;


public class CommentResponseVo extends CommonVo {

    ArrayList<CommentsVo> commentinfo;

    public ArrayList<CommentsVo> getAlCommentListing() {
        return commentinfo;
    }

    public void setAlCommentListing(ArrayList<CommentsVo> alCommentListing) {
        commentinfo = alCommentListing;
    }

}
