package wg.com.missugram.model;


import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {


    private int id;
 //   private int userid;
    private String email;
    private String first_name;
    private String last_name;
    private String push_flag;
    private String phone;
    private String quickblox_account_id;
    private String avatar;
    private String token;


    private transient boolean isChecked;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    private String password;
    private String device_type;
    private String device_token;
    private String new_password;
    private String confirm_password;

   /* public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPush_flag() {
        return push_flag;
    }

    public void setPush_flag(String push_flag) {
        this.push_flag = push_flag;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getQuickblox_account_id() {
        return quickblox_account_id;
    }

    public void setQuickblox_account_id(String quickblox_account_id) {
        this.quickblox_account_id = quickblox_account_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
       // dest.writeInt(this.userid);
        dest.writeString(this.email);
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.push_flag);
        dest.writeString(this.phone);
        dest.writeString(this.quickblox_account_id);
        dest.writeString(this.avatar);
        dest.writeString(this.token);
        dest.writeString(this.password);
        dest.writeString(this.device_type);
        dest.writeString(this.device_token);
        dest.writeString(this.new_password);
        dest.writeString(this.confirm_password);
    }

    public UserModel() {
    }

    protected UserModel(Parcel in) {
        this.id = in.readInt();
       // this.userid = in.readInt();
        this.email = in.readString();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.push_flag = in.readString();
        this.phone = in.readString();
        this.quickblox_account_id = in.readString();
        this.avatar = in.readString();
        this.token = in.readString();
        this.password = in.readString();
        this.device_type = in.readString();
        this.device_token = in.readString();
        this.new_password = in.readString();
        this.confirm_password = in.readString();
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
