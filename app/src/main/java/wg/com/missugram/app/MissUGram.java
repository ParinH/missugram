package wg.com.missugram.app;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.wg.acralibrary.ErrorHandlerConstants;

import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import wg.com.missugram.R;
import wg.com.missugram.constants.Constants;
import wg.com.missugram.model.ChatUserHistoryModel;
import wg.com.missugram.model.UserModel;

/**
 * Entry point of the application.This is an application level class to init
 * ACRA for error reporting
 * <p/>
 * Replace crash_project_id with new application key
 */
@ReportsCrashes(formUri = ErrorHandlerConstants.CRASH_SUBMISSION_URL, formKey = "", resDialogText = R.string.crash_dialog_text, customReportContent = {ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT}, mode = ReportingInteractionMode.DIALOG, resToastText = R.string.crash_toast_text, resDialogCommentPrompt = R.string.crash_dialog_comment_prompt)
public class MissUGram extends MultiDexApplication {

    private ImageLoader mImageLoader;
    private UserModel userModel;
    private ArrayList<ChatUserHistoryModel> mChatUserHistoryModels = new ArrayList<>();
    private ArrayList<String> listOfGroupDialogIds = new ArrayList<>();


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public ArrayList<ChatUserHistoryModel> getmChatUserHistoryModelsForRecentChatList() {
        return mChatUserHistoryModelsForRecentChatList;
    }

    public void setmChatUserHistoryModelsForRecentChatList(ArrayList<ChatUserHistoryModel> mChatUserHistoryModelsForRecentChatList) {
        this.mChatUserHistoryModelsForRecentChatList = mChatUserHistoryModelsForRecentChatList;
    }

    private ArrayList<ChatUserHistoryModel> mChatUserHistoryModelsForRecentChatList=new ArrayList<>();

    public static MissUGram getApp(Context context) {
        return (MissUGram) context.getApplicationContext();
    }

    public ArrayList<ChatUserHistoryModel> getmChatUserHistoryModelsForGroup() {
        return mChatUserHistoryModels;
    }

    public void setmChatUserHistoryModelsForGroup(ArrayList<ChatUserHistoryModel> mChatUserHistoryModels) {
        this.mChatUserHistoryModels = mChatUserHistoryModels;
    }

    public ArrayList<String> getListOfGroupDialogIds() {
        Set<String> hs = new HashSet<>();
        hs.addAll(listOfGroupDialogIds);
        listOfGroupDialogIds.clear();
        listOfGroupDialogIds.addAll(hs);
        return listOfGroupDialogIds;
    }

    public void setListOfGroupDialogIds(ArrayList<String> listOfGroupDialogIds) {
        this.listOfGroupDialogIds = listOfGroupDialogIds;
    }

    @Override
    public void onCreate() {
        // ACRA.init(this);
        super.onCreate();
        preRequisitesForChat();
        initImageLoader();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onTrimMemory(int p_level) {
        super.onTrimMemory(p_level);
    }

    private void initImageLoader() {
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                //.cacheOnDisc(true).cacheInMemory(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300))
                .considerExifParams(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(config);
    }

    public ImageLoader getmImageLoader() {
        return mImageLoader;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    private void preRequisitesForChat() {
        QBSettings.getInstance().init(this.getApplicationContext(), Constants.CHAT_APP_ID, Constants.CHAT_AUTH_KEY, Constants.CHAT_AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(Constants.CHAT_APP_ACCOUNTKEY);

        QBChatService.getInstance().setReconnectionAllowed(true);
        QBSettings.getInstance().setAutoCreateSession(true);

        QBChatService.setDefaultPacketReplyTimeout(20000);//set reply timeout in milliseconds for connection's packet.
        QBChatService.getInstance().setDefaultPacketReplyTimeout(150000);
        QBChatService.getInstance().setDefaultConnectionTimeout(150000);
        QBChatService.getInstance().setUseStreamManagement(true);
        QBChatService.ConfigurationBuilder chatServiceConfigurationBuilder = new QBChatService.ConfigurationBuilder();
        chatServiceConfigurationBuilder.setSocketTimeout(0); //Sets chat socket's read timeout in seconds
        chatServiceConfigurationBuilder.setKeepAlive(true); //Sets connection socket's keepAlive option.
        chatServiceConfigurationBuilder.setUseTls(true); //Sets the TLS security mode used when making the connection. By default TLS is disabled.

        QBChatService.setConfigurationBuilder(chatServiceConfigurationBuilder);

    }
}
